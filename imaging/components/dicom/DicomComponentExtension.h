/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMCOMPONENTEXTENSION_H
#define DICOMCOMPONENTEXTENSION_H

#include <ComponentExtension.h>

namespace camitk {
class Component;
}

class DicomDialog;
class DicomDialogEntry;
class DicomSeries;


/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief
 * DICOM image Component manager.
 *
 * @note This @ref camitk::Component "Component" requires the GDCM library in order to work correctly.
 * @note This class represents the controller (in the MVP design pattern) of the DICOM Component
 */
class DicomComponentExtension : public camitk::ComponentExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ComponentExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.component.dicom")

public:
    /// Constructor
    DicomComponentExtension() : ComponentExtension() {};

    /// Method returning the component extension name
    virtual QString getName() const override {
        return "DICOM";
    };

    /// Method returning the component extension description
    virtual QString getDescription() const override {
        return "This extension adds support for DICOM single image file (.dcm) and series thanks to the gdcm library";
    };


    /** Get the list of managed extensions
     * (each file with an extension in the list can be loaded by this extension)
     */
    virtual QStringList getFileExtensions() const override;

    /** Open a single .dcm image or a directory containing files to be read
     *  This method may throw an AbortException if a problem occurs.
     */
    virtual camitk::Component* open(const QString&) override;

    /** Save a given Component (does not have to be top-level)
     *  into one of the currently managed format.
     *  @return false if the operation was not performed properly or not performed at all.
     */
    virtual bool save(camitk::Component*) const override;

    /// this method returns true as this component loads images from a directory (not a single file)
    virtual bool hasDataDirectory() const override;

protected:
    /// Destructor
    virtual ~DicomComponentExtension() = default;

private:
    /// Last opened component. When opening the directory, create 1 component per files
    /// Return at least one for the application to know it manages to open the files
    camitk::Component* lastOpenedComponent;

    /// The dialog that asks the user which dicom image files he wishes to open
    DicomDialog* dialog;

    /// The different Dicom SERIES parsed
    QList<DicomSeries*> seriesParsed;

    /// The series dialog entries representation in the VIEW
    QList<DicomDialogEntry*> seriesDialogEntries;

};

#endif // DICOMCOMPONENTEXTENSION_H

