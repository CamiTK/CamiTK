/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "DicomSeries.h"

// --------------- Constructor -------------------
DicomSeries::DicomSeries() :
    acquisitionDate{QDate::currentDate()},
    studyName{"NOT FOUND"},
    seriesName{"NOT FOUND"},
    seriesDescription{"NOT FOUND"},
    patientName{"NOT FOUND"} {
}

// --------------- Getters -------------------
QDate DicomSeries::getAcquisitionDate() const {
    return acquisitionDate;
}
QTime DicomSeries::getAcquisitionTime() const {
    return acquisitionTime;
}
QString DicomSeries::getStudyName() const {
    return studyName;
}
QString DicomSeries::getSeriesName() const {
    return seriesName;
}
QString DicomSeries::getSeriesDescription() const {
    return seriesDescription;
}
QString DicomSeries::getPatientName() const {
    return patientName;
}
QList<QString> DicomSeries::getFileNames() const {
    return fileNames;
}
std::vector<std::string> DicomSeries::getStdFileNames() const {
    return stdFileNames;
}

// --------------- Setters -------------------
void DicomSeries::setAcquisitionDate(QDate date) {
    acquisitionDate = date;
}
void DicomSeries::setAcquisitionTime(QTime time) {
    acquisitionTime = time;
}
void DicomSeries::setStudyName(QString name) {
    studyName = name;
}
void DicomSeries::setSeriesName(QString name) {
    seriesName = name;
}
void DicomSeries::setSeriesDescription(QString name) {
    seriesDescription = name;
}
void DicomSeries::setPatientName(QString name) {
    patientName = name;
}
void DicomSeries::setFileNames(QList<QString> inputFileNames) {
    fileNames = inputFileNames;
}
void DicomSeries::setStdFileNames(std::vector<std::string> inputFileNames) {
    stdFileNames = inputFileNames;
}













