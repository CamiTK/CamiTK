#Specific to SDK: control the version (major, minor, default patch and )
set(CAMITK_VERSION_MAJOR "5")
set(CAMITK_VERSION_MINOR "3")
set(CAMITK_VERSION_NICKNAME "Chartreuse") # Green
set(CAMITK_VERSION_PATCH "dev") # patch version for packaging, change when appropriate
