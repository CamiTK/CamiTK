# CMake script to configure CamiTKDir.txt at installation time (executed by top-level target "camitk-ce-copy-camitkdirtxt", see CMakeLists.txt)
message(STATUS "Updating CamiTKDir.txt in User Config Directory [${CAMITK_USER_DIR}] to [${CMAKE_INSTALL_PREFIX}]")
configure_file(${SDK_TOP_LEVEL_SOURCE_DIR}/CamiTKDir.txt.in ${SDK_TOP_LEVEL_BINARY_DIR}/CamiTKDir.txt @ONLY)
