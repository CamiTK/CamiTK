#!
#! @ingroup group_sdk_cmake
#!
#! macro get_directory_name set the variable VariableName to the name of the last directory of FullPathDirectory
#! set the variable VariableName_CMAKE to be the same but in uppercase
#!
#! Usage:
#! \code
#! directory_name(FullPathDirectory VariableName)
#! \endcode
#!
#! \param FullPathDirectory (required)    the full path directory (input)
#! \param VariableName (required)         the resulting variable name containing the base name all in uppercase
macro(get_directory_name FullPathDirectory VariableName)
  string (REGEX REPLACE ".*/([^/]*)$" "\\1" ${VariableName} ${FullPathDirectory})

  # set the variable name to uppercase
  string(TOUPPER ${${VariableName}} ${VariableName}_CMAKE)
endmacro()