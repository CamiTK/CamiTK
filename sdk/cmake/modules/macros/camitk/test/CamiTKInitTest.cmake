#!
#! @ingroup group_sdk_cmake_camitk_test
#!
#! camitk_init_test is a macro to initialize a group of test (for the same command)
#! It is used to initialize a series of tests for a given target.
#! Usually this is placed in the same CMakeLists.txt as the add_executable() cmake instruction or
#! camitk_application() macro
#!
#! It does few useful things:
#! - check if the application is properly defined
#! - initialize test id (then automatically incremented in camitk_test_declare
#! - initialize test output directory
#!
#! Usage:
#! \code
#! camitk_init_test(applicationname)
#! \endcode
#!
#! \param applicationname (required)   The name of the test application (in camitk context, name of the project
#!
#! Example invocation:
#!
#! \code
#!
#! camitk_application()
#!
#! ...
#! # Start the test series for myprogram
#! camitk_init_test(myprogram)
#! camitk_add_test(...) # will be called myprogram1
#! ...
#! camitk_add_test(...) # myprogram2
#!
#! \endcode
#
#! @sa camitk_add_test
macro(camitk_init_test)
    set(TEST_APPLICATION_ARG ${ARGN})

    set(CAMITK_TEST_ID "0")
    set(CAMITK_TEST_LIST "")

    # check for executable
    if(NOT TEST_APPLICATION_ARG)
        message(FATAL_ERROR "Initializing test cannot proceed: please specify the test application to use in brackets")
    else()
        if(TARGET ${TEST_APPLICATION_ARG})
            # get the application real name using the target properties
            get_target_property( APP_NAME ${TEST_APPLICATION_ARG} OUTPUT_NAME)
        
            # add debug postfix if needed by MSVC
            set(APP_SUFFIX "")
            if(MSVC)
                get_target_property(APP_SUFFIX ${TEST_APPLICATION_ARG} DEBUG_POSTFIX)
            endif()

            # CAMITK_INIT_TEST_EXECUTABLE is not the application name but the full path to the binary to be executed
            set(CAMITK_INIT_TEST_EXECUTABLE ${CMAKE_BINARY_DIR}/bin/${APP_NAME}${APP_SUFFIX})  
        else()            
            # This is not an application of the current CEP
            # In this case: the application name should be given as the default parameter
            set(APP_NAME ${TEST_APPLICATION_ARG})
            
            if(NOT CAMITK_COMMUNITY_EDITION_BUILD)
                # this macro is called outside CamiTK CE
                
                # find the complete path to the test programs (removing any previous attempt to find a test application
                # as this could be another application)
                unset(CAMITK_INIT_TEST_EXECUTABLE CACHE)
                find_program(CAMITK_INIT_TEST_EXECUTABLE
                            NAMES ${APP_NAME}${CAMITK_DEBUG_POSTFIX} ${APP_NAME}
                            PATH_SUFFIXES "bin"
                            PATHS ${CAMITK_BIN_DIR}
                )
                
                if (NOT CAMITK_INIT_TEST_EXECUTABLE)
                    # Test programs should be installed
                    message(FATAL_ERROR "${APP_NAME} not found.\n   This means that automatic test applications were not installed during CamiTK SDK installation.")
                endif()                                
            else()
                # in CamiTK CE build

                # add debug postfix if needed by MSVC
                set(APP_SUFFIX "")
                # determine which version of the executable to use (debug-suffixed or not)
                if(MSVC)
                    if(NOT CMAKE_BUILD_TYPE)
                        # Assume the developer that is running the test suite compiled everything in Debug
                        set(APP_SUFFIX ${CAMITK_DEBUG_POSTFIX})
                    else()
                        # support multiplaform (sometimes the "Debug" type is all uppercase, as on Win32, sometimes it is CamelCase)
                        string(TOUPPER ${CMAKE_BUILD_TYPE} CAMITK_BUILD_TYPE_UPPER)
                        if (CAMITK_BUILD_TYPE_UPPER STREQUAL "DEBUG")
                            # manage debug build only
                            set(APP_SUFFIX ${CAMITK_DEBUG_POSTFIX})
                        endif()
                        # if build type is not debug, everything is ok as
                    endif()
                endif()
                
                # In CamiTK CE build directly use the binary dir version
                set(CAMITK_INIT_TEST_EXECUTABLE ${CMAKE_BINARY_DIR}/bin/${APP_NAME}${APP_SUFFIX})  
            endif()
        endif()
    endif()
    
    set(CAMITK_TEST_BASENAME ${TEST_APPLICATION_ARG})
endmacro()
