#!
#! @ingroup group_sdk_cmake_camitk_test_level
#!
#! MACRO camitk_parse_test_init create a CMake variable to store all the automatically generated tests for action & component level information.
#!
#! usage:
#! \code
#! camitk_parse_test_init()
#! \endcode
macro(camitk_parse_test_init)
    set(CAMITK_EXTENSIONS_TESTS_LEVEL "{| class=\"wikitable\"{| style=\"background-color: #CCCCCC\" \\n" CACHE INTERNAL "")
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "|+ List of automatically generated tests per extension (action and component) \\n" CACHE INTERNAL "")
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "! Component / Action Test Name !! Test Level !! Test Description \\n\\n" CACHE INTERNAL "")
endmacro()
 
