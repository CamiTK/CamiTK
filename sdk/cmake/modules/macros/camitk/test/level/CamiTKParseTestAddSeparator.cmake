#!
#! @ingroup group_sdk_cmake_camitk_test_level
#!
#! macro camitk_parse_test_add_separator adds an extension (action or component) separator entry.
#! Typically call this macro before calling any instance of camitk_parse_test_add()
#!
#! Usage:
#! \code
#! camitk_parse_test_add(EXTENSION_TYPE
#!                       EXTENSION_NAME
#!                      )
#! \endcode
#!
#! \param EXTENSION_TYPE        The type extension (action or component)
#! \param EXTENSION_NAME        The name the extension
macro(camitk_parse_test_add_separator)
    set(options "")
    set(oneValueArgs EXTENSION_TYPE EXTENSION_NAME)
    set(multiValueArgs "")
    cmake_parse_arguments(CAMITK_PARSE_TEST_ADD_SEPARATOR "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    set( CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "|- style=\"background-color: #CCCCCC\" \\n || '''${CAMITK_PARSE_TEST_ADD_SEPARATOR_EXTENSION_TYPE} ${CAMITK_PARSE_TEST_ADD_SEPARATOR_EXTENSION_NAME}''' \\n" CACHE INTERNAL "")
endmacro() 
