#!
#! @ingroup group_sdk_cmake_camitk_test_level
#!
#! macro camitk_parse_test_validate generate the CamiTKTestsLevel.txt file which contains the CamiTK wiki entries (store in a tab) which describes for all CamiTK actions & components the different level of tests 
#!
#! Usage:
#! \code
#! camitk_parse_test_add()
#! \endcode

macro(camitk_parse_test_validate)
    # Close the wiki tab
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "|}" CACHE INTERNAL "")
        
    # Write down the CamiTKTestsLevel.txt file
    set(filename "${CMAKE_BINARY_DIR}/CamiTKTestsLevel.txt")
    string(REPLACE "\\n" "\n" CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL})
    file(WRITE ${filename} ${CAMITK_EXTENSIONS_TESTS_LEVEL})
    message(STATUS "Generated ${filename} for tests level description")

       
endmacro() 
 
