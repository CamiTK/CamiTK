#!
#! @ingroup group_sdk_cmake_camitk_test_level
#!
#! macro camitk_parse_test_add adds an extension (action or component) automatic test level information
#! Typically recursively call this function accross all actions and components to generate CamiTKTestsLevel.txt file.
#!
#! Usage:
#! \code
#! camitk_parse_test_add(NAME
#!                       LEVEL
#!                       DESCRIPTION
#!                       )
#! \endcode
#!
#! \param NAME          The name of the test for the extension
#! \param LEVEL         The level of automatic test (between 0, 1, 2, 3)
#! \param DESCRIPTION   The description of the automatic test
macro(camitk_parse_test_add)

    set(options "")
    set(oneValueArgs NAME LEVEL DESCRIPTION)
    set(multiValueArgs "")
    cmake_parse_arguments(CAMITK_PARSE_TEST_ADD "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # Test required inputs have been given to the macro
    if(NOT DEFINED CAMITK_PARSE_TEST_ADD_NAME)
        message(FATAL_ERROR "camitk_parse_test_add: the name is required")
    endif()
    if(NOT DEFINED CAMITK_PARSE_TEST_ADD_LEVEL)
        message(FATAL_ERROR "camitk_parse_test_add: the level is required")
    endif()
    if(NOT DEFINED CAMITK_PARSE_TEST_ADD_DESCRIPTION)
        message(FATAL_ERROR "camitk_parse_test_add: the description is required")
    endif()
        
    # Store these information as a new entry of test level
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "|- style=\"background-color: #EEEEEE\" \\n" CACHE INTERNAL "")
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "| style=\"width: 35%\" |${CAMITK_PARSE_TEST_ADD_NAME} \\n" CACHE INTERNAL "")
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "| style=\"width: 15%\" |'''${CAMITK_PARSE_TEST_ADD_LEVEL}''' \\n" CACHE INTERNAL "")
    set(CAMITK_EXTENSIONS_TESTS_LEVEL ${CAMITK_EXTENSIONS_TESTS_LEVEL} "|| ${CAMITK_PARSE_TEST_ADD_DESCRIPTION} \\n\\n" CACHE INTERNAL "")
       
endmacro() 
