#! This module once implemented a fork from CMakeMacroParseArguments for legacy support reason.
#! From CMake 3.0, this module was available as CMakeParseArguments module.
#! From CMake 3.5 the cmake_parse_arguments() command is implemented natively by CMake. 
#! Keep the include(CMakeParseArguments) as a placeholder, required for compatibility with projects 
#! that include it to get the command from CMake 3.4 and lower.
include(CMakeParseArguments)
