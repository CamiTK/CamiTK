# FindCamSdk.camke file using CMake's Module Mode
# The search starts by looking if the package is installed on the system.
# If not, it looks for a copy of the package somewhere on the disk.

# SET the <package>_USE_FILE
SET( CamSdk_USE_FILE UseCamSdk.cmake )

# Looks if CamSdk is installed on the system by looking in Windows's registry
GET_FILENAME_COMPONENT( camsdk_install_version "[HKEY_LOCAL_MACHINE\\Software\\STMicroelectronics\\CamSdk;latest]" NAME)

IF( camsdk_install_version MATCHES "registry" )
	SET( CAMSDK_INSTALLED FALSE CACHE BOOL "CAMSDK installed ?")
	#MESSAGE( STATUS "CamSdk version NOT found in Windows Registry")
ELSE( camsdk_install_version MATCHES "registry" )
	SET( CAMSDK_INSTALLED TRUE CACHE BOOL "CAMSDK installed ?")
	#MESSAGE( STATUS "CamSdk version found in Windows Registry : ${camsdk_install_version}")
ENDIF( camsdk_install_version MATCHES "registry" )


###############################################################################
#			FIND MODULE DIRECTORIES
###############################################################################

IF( CAMSDK_INSTALLED )
	# Get the path containing CamSdk.dll in Windows's registry
	GET_FILENAME_COMPONENT( CAMSDK_ROOT_DIR "[HKEY_LOCAL_MACHINE\\Software\\STMicroelectronics\\CamSdk;${camsdk_install_version}]" PATH)
	
	# SETting library dir
	SET( CamSdk_LATEST_DIR_TMP ${CAMSDK_ROOT_DIR}/latest )
	SET( CamSdk_LIBRARIES_LATEST_DIR_TMP ${CAMSDK_ROOT_DIR}/libraries/latest )
	
	MESSAGE( STATUS "(Register mode) CamSdk latest directory : ${CamSdk_LATEST_DIR_TMP}")
	MESSAGE( STATUS "(Register mode) CamSdk libraries latest directory : ${CamSdk_LIBRARIES_LATEST_DIR_TMP}")

ELSE( CAMSDK_INSTALLED )

	FIND_PATH( CamSdk_LATEST_DIR_TMP CamSdk.h PATHS ENV CAMSDK ENV CAMSDK_DIRS ENV CAMSDK_DIR ENV PATH )
	FIND_PATH( CamSdk_LIBRARIES_LATEST_DIR_TMP cusbcap.h PATHS ENV CAMSDK ENV CAMSDK_DIRS ENV CAMSDK_DIR ENV PATH )
	
	MESSAGE( STATUS "(FIND_PATH mode) CamSdk latest directory : ${CamSdk_LATEST_DIR_TMP}")
	MESSAGE( STATUS "(FIND_PATH mode) CamSdk libraries latest directory : ${CamSdk_LIBRARIES_LATEST_DIR_TMP}")
	
	STRING( REGEX REPLACE "/latest" "" CAMSDK_ROOT_DIR ${CamSdk_LATEST_DIR_TMP} )

ENDIF( CAMSDK_INSTALLED )

#MESSAGE( STATUS "CamSdk root directory : ${CAMSDK_ROOT_DIR}")

# Check if the "latest" directory exists
IF(EXISTS "${CamSdk_LATEST_DIR_TMP}" )
	SET(CamSdk_LATEST_DIR "${CamSdk_LATEST_DIR_TMP}" CACHE PATH "CamSdk latest dir" FORCE)
ELSE(EXISTS "${CamSdk_LATEST_DIR_TMP}" )
	MESSAGE(SEND_ERROR "Can't find CamSdk lastest directory.")
ENDIF(EXISTS "${CamSdk_LATEST_DIR_TMP}" )

# Check if the "libraries/latest" directory exists
IF(EXISTS "${CamSdk_LIBRARIES_LATEST_DIR_TMP}" )
	SET(CamSdk_LIBRARIES_LATEST_DIR "${CamSdk_LIBRARIES_LATEST_DIR_TMP}" CACHE PATH "CamSdk latest dir" FORCE)
ELSE(EXISTS "${CamSdk_LIBRARIES_LATEST_DIR_TMP}" )
	MESSAGE(SEND_ERROR "Can't find CamSdk libraries lastest directory.")
ENDIF(EXISTS "${CamSdk_LIBRARIES_LATEST_DIR_TMP}" )

###############################################################################
#			VERSION CHECKING
###############################################################################

# If version specified by user else using minimum version 0.0.3
IF (NOT CAMSDK_MIN_VERSION)
	SET(CAMSDK_MIN_VERSION "0.0.3")
ENDIF (NOT CAMSDK_MIN_VERSION)
STRING(REGEX MATCH "^[0-9]+\\.[0-9]+\\.[0-9]+" req_camsdk_major_vers "${CAMSDK_MIN_VERSION}")

# Check version string correctness
IF (NOT req_camsdk_major_vers)
	MESSAGE( FATAL_ERROR "Invalid CamSdk version STRING given: \"${CAMSDK_MIN_VERSION}\", expected e.g. \"0.0.3\"")
ENDIF (NOT req_camsdk_major_vers)

# Parsing the different version information : major minor patch
STRING(REGEX REPLACE "^([0-9]+)\\.[0-9]+\\.[0-9]+" "\\1" req_camsdk_major_vers "${CAMSDK_MIN_VERSION}")
STRING(REGEX REPLACE "^[0-9]+\\.([0-9]+)\\.[0-9]+" "\\1" req_camsdk_minor_vers "${CAMSDK_MIN_VERSION}")
STRING(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+)" "\\1" req_camsdk_patch_vers "${CAMSDK_MIN_VERSION}")
#MESSAGE( STATUS "MINIMUM VERSION : ${req_camsdk_major_vers}.${req_camsdk_minor_vers}.${req_camsdk_patch_vers}")

# If version specified in find_package command
IF( CamSdk_FIND_VERSION )
	SET( CAMSDK_MIN_VERSION ${CamSdk_FIND_VERSION} )
	SET( req_camsdk_major_vers ${CamSdk_FIND_VERSION_MAJOR} )
	SET( req_camsdk_minor_vers ${CamSdk_FIND_VERSION_MINOR} )
	SET( req_camsdk_patch_vers ${CamSdk_FIND_VERSION_PATCH} )
	#MESSAGE( STATUS "FIND VERSION : ${req_camsdk_major_vers}.${req_camsdk_minor_vers}.${req_camsdk_patch_vers}")
ENDIF( CamSdk_FIND_VERSION )

# Check if the minimum version restriction is statisfied 
IF(req_camsdk_patch_vers LESS 3)
      MESSAGE( FATAL_ERROR "Invalid Camsdk version STRING given: \"${CAMSDK_MIN_VERSION}\", patch version 3 is required, e.g. \"0.0.3\"")
ENDIF( req_camsdk_patch_vers LESS 3)

# Setting CAMSDK_VERSION_MAJOR, CAMSDK_VERSION_MINOR and CAMSDK_VERSION_PATCH variables
IF( CAMSDK_INSTALLED )
	# Make version compatible ie from v003 to 0.0.3
	STRING( REPLACE "v" "" camsdk_install_version "${camsdk_install_version}")
	STRING(REGEX REPLACE "^([0-9]+)([0-9]+)([0-9]+)" "\\1.\\2.\\3" camsdk_install_version "${camsdk_install_version}")
	
	# Parsing the different version information : major minor patch
    STRING(REGEX REPLACE "^([0-9]+)\\.[0-9]+\\.[0-9]+.*" "\\1" CAMSDK_VERSION_MAJOR "${camsdk_install_version}")
	STRING(REGEX REPLACE "^[0-9]+\\.([0-9]+)\\.[0-9]+.*" "\\1" CAMSDK_VERSION_MINOR "${camsdk_install_version}")
	STRING(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" CAMSDK_VERSION_PATCH "${camsdk_install_version}")
	#MESSAGE( STATUS "INSTALL VERSION : ${CAMSDK_VERSION_MAJOR}.${CAMSDK_VERSION_MINOR}.${CAMSDK_VERSION_PATCH}")
	
ELSE( CAMSDK_INSTALLED )

	#Reading the version file provided with package copy.
	file( READ ${CAMSDK_ROOT_DIR}/CamSdkLatestVersion.txt camsdk_copied_version )
	
	# Parsing the different version information : major minor patch
    STRING(REGEX REPLACE "^([0-9]+)\\.[0-9]+\\.[0-9]+.*" "\\1" CAMSDK_VERSION_MAJOR "${camsdk_copied_version}")
	STRING(REGEX REPLACE "^[0-9]+\\.([0-9]+)\\.[0-9]+.*" "\\1" CAMSDK_VERSION_MINOR "${camsdk_copied_version}")
	STRING(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" CAMSDK_VERSION_PATCH "${camsdk_copied_version}")
	#MESSAGE( STATUS "COPIED VERSION : ${CAMSDK_VERSION_MAJOR}.${CAMSDK_VERSION_MINOR}.${CAMSDK_VERSION_PATCH}")

ENDIF( CAMSDK_INSTALLED )

# Setting CAMSDK_VERSION variable
SET( CAMSDK_VERSION "${CAMSDK_VERSION_MAJOR}.${CAMSDK_VERSION_MINOR}.${CAMSDK_VERSION_PATCH}")

# Compute the mathematical version numbers for comparision
MATH(EXPR req_vers "${req_camsdk_major_vers}*10000 + ${req_camsdk_minor_vers}*100 + ${req_camsdk_patch_vers}")
MATH(EXPR found_vers "${CAMSDK_VERSION_MAJOR}*10000 + ${CAMSDK_VERSION_MINOR}*100 + ${CAMSDK_VERSION_PATCH}")

# Support for EXACT query.
IF( CamSdk_FIND_VERSION_EXACT )
  IF(found_vers EQUAL req_vers)
    SET( CamSdk_EXACT_FOUND TRUE )
  ELSE(found_vers EQUAL req_vers)
    SET( CamSdk_EXACT_FOUND FALSE )
    IF (found_vers LESS req_vers)
      SET(CamSdk_INSTALLED_VERSION_TOO_OLD TRUE)
    ELSE (found_vers LESS req_vers)
      SET(CamSdk_INSTALLED_VERSION_TOO_NEW TRUE)
    ENDIF (found_vers LESS req_vers)
  ENDIF(found_vers EQUAL req_vers)
ELSE( CamSdk_FIND_VERSION_EXACT )
  IF (found_vers LESS req_vers)
    SET(CamSdk_EXACT_FOUND FALSE)
    SET(CamSdk_INSTALLED_VERSION_TOO_OLD TRUE)
  ELSE (found_vers LESS req_vers)
    SET(CamSdk_EXACT_FOUND TRUE)
  ENDIF (found_vers LESS req_vers)
ENDIF( CamSdk_FIND_VERSION_EXACT )

###############################################################################
#			FIND MODULES
###############################################################################

# Assuming the the package is found
SET( CamSdk_FOUND TRUE )

# CamSdk DLLs part of the package
SET( CamSdk_DLLS CamSdk stvenum)
SET( CamSdk_LIBRARIES_DLLS 	USB2_I2C USB2_I2C0 USB2_I2C1 USB2_I2C2 USB2_I2C3 USB2_I2C4
								usbcap usbcap0 usbcap1 usbcap2 usbcap3 usbcap4
								vconvert vdisplay vimg vlist )
# Checking the existence of each DLLs
IF( CamSdk_EXACT_FOUND AND CamSdk_LATEST_DIR AND CamSdk_LIBRARIES_LATEST_DIR )

	FOREACH( CamSdk_DLL ${CamSdk_DLLS} )
		STRING(TOUPPER ${CamSdk_DLL} _upper_camsdk_module)
		FIND_FILE( CAMSDK_${_upper_camsdk_module}_DLL ${CamSdk_DLL}.dll PATHS ${CamSdk_LATEST_DIR} NO_DEFAULT_PATH )
		IF( NOT CAMSDK_${_upper_camsdk_module}_DLL )
			SET( CamSdk_FOUND FALSE )
		ENDIF( NOT CAMSDK_${_upper_camsdk_module}_DLL )
	ENDFOREACH(CamSdk_DLL)
	
	FOREACH(CamSdk_LIBRARIES_DLL ${CamSdk_LIBRARIES_DLLS})
		STRING(TOUPPER ${CamSdk_LIBRARIES_DLL} _upper_camsdk_libraries_module)
		FIND_FILE( CAMSDK_${_upper_camsdk_libraries_module}_DLL ${CamSdk_LIBRARIES_DLL}.dll PATHS ${CamSdk_LIBRARIES_LATEST_DIR} NO_DEFAULT_PATH )
		IF( NOT CAMSDK_${_upper_camsdk_libraries_module}_DLL )
			SET( CamSdk_FOUND FALSE )
		ENDIF( NOT CAMSDK_${_upper_camsdk_libraries_module}_DLL )
	ENDFOREACH(CamSdk_LIBRARIES_DLL)
	
ENDIF( CamSdk_EXACT_FOUND AND CamSdk_LATEST_DIR AND CamSdk_LIBRARIES_LATEST_DIR )

# Support for REQUIRED query
IF( CamSdk_FOUND )
	MESSAGE( STATUS "Found CamSdk-Version ${CAMSDK_VERSION} (using ${CAMSDK_ROOT_DIR})" )
ELSE( CamSdk_FOUND )
	IF(CamSdk_FIND_REQUIRED)
		MESSAGE( SEND_ERROR "Can't find CAMSDK package. Please install the package on the system OR use a HDD copy of the package" )
	ENDIF(CamSdk_FIND_REQUIRED)
ENDIF( CamSdk_FOUND )