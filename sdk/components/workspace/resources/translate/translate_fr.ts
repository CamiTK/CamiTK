<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>WorkspaceExtension</name>
    <message>
        <source>Manage CamiTK Workspace format.</source>
        <translation>Gère le format des espaces de travail CamiTK</translation>
    </message>
</context>
</TS>
