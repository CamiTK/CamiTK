/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "VtkMeshComponentExtension.h"
#include "VtkMeshComponent.h"

using namespace camitk;

// --------------- getName -------------------
QString VtkMeshComponentExtension::getName() const {
    return "VTK Mesh";
}

// --------------- getDescription -------------------
QString VtkMeshComponentExtension::getDescription() const {
    return tr("Manage VTK <em>.vtk</em> mesh files in <b>CamiTK</b>.");
}

// --------------- getFileExtensions -------------------
QStringList VtkMeshComponentExtension::getFileExtensions() const {
    QStringList ext;
    ext << "vtk";
    return ext;
}

// --------------- open -------------------
Component* VtkMeshComponentExtension::open(const QString& fileName) {
    return new VtkMeshComponent(fileName);
}

// --------------- save -------------------
bool VtkMeshComponentExtension::save(Component* component) const {
    MeshComponent* mesh = dynamic_cast<MeshComponent*>(component);
    if (mesh->getPointSet()) {
        VtkMeshUtil::savePointSetToFile(mesh->getPointSet(), mesh->getFileName().toStdString(), mesh->getName().toStdString());
        return true;
    }
    else {
        return false;
    }
}
