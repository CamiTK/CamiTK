# vtkmesh component does not require any specific things
# to be compiled/generated by default
# there are some header installation needed 
camitk_extension(COMPONENT_EXTENSION
                 DEFINES COMPILE_VTK_COMPONENT_API
                 HEADERS_TO_INSTALL VtkMeshComponent.h
                                    VtkMeshComponentExtension.h                                    
                                    VtkMeshComponentAPI.h
                                    VtkMeshUtil.h
                 CEP_NAME SDK
                 DESCRIPTION "Support for native (legacy) vtkmesh files"
                 ENABLE_AUTO_TEST
                 # Exclude binary files
                 TEST_FILES cube-with-point-and-cell-data.vtk imageBoundingBox.vtk polydata-with-data.vtk sinus_skull.vtk cube-with-tensors.vtk male.vtk structuredgrid-with-data.vtk brainImageSmooth.vtk female.vtk plate-with-data.vtk simple.vtk structured.vtk check-with-color-pointdata.vtk fieldfile.vtk pointcloud-with-data.vtk  sinus_skin.vtk
)

# Recursively update the shiboken path variable containing the CamiTK SDK tree structure
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "")

# --------------------------------
# --- Specific test management ---
# --------------------------------
camitk_tests_requirement(TESTS component-vtkmesh-level3-1
                         component-vtkmesh-level3-2
                         component-vtkmesh-level3-3
                         component-vtkmesh-level3-4
                         component-vtkmesh-level3-5
                         component-vtkmesh-level3-6
                         component-vtkmesh-level3-7
                         component-vtkmesh-level3-8
                         component-vtkmesh-level3-9
                         component-vtkmesh-level3-10
                         component-vtkmesh-level3-11
                         component-vtkmesh-level3-12
                         component-vtkmesh-level3-13
                         component-vtkmesh-level3-14
                         component-vtkmesh-level3-15
                         component-vtkmesh-level3-16
                         REQUIRES "${VTK_VERSION} VERSION_EQUAL 7.1.1"
                         REASON "VTK version is not equals than 7.1.1
   The default test files are written using the currently supported version of VTK (VTK 7.1).
   This test will therefore fail when comparing the input to the output if another version of VTK is used.
   Although everything should work fine, the output diff will make this test unconditionnaly fail."
)
