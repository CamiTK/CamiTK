<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>RawDataDialog</name>
    <message>
        <location filename="../../RawDataDialog.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="64"/>
        <source>Raw data information</source>
        <translation>Information Données Brutes</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="88"/>
        <source>File name</source>
        <translation>Nom de Fichier</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="118"/>
        <source>Volume Dimensions</source>
        <translation>Dimension du volume</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="138"/>
        <source>Dim X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="194"/>
        <source>Dim Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="244"/>
        <source>Dim Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="299"/>
        <source>Voxels Value Type</source>
        <translation>Type de Valeur des Voxels </translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="337"/>
        <source>Data Byte Order: Big Endian</source>
        <translation>Ordre des Octets de donnée : Big Endian</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="358"/>
        <source>Nb Scalar Components</source>
        <translation>Nombre de Components Scalaires</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="415"/>
        <source>Voxels Spacing</source>
        <translation>Espacement des Voxels</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="436"/>
        <source>Voxel size X</source>
        <translation>Taille X du Voxel</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="487"/>
        <source>Voxel size Y</source>
        <translation>Taille Y du Voxel</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="538"/>
        <source>Voxel size Z</source>
        <translation>Taille Z du Voxel</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="595"/>
        <source>Image Origin</source>
        <translation>Origine de l&apos;Image</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="616"/>
        <source>Ox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="667"/>
        <source>Oy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="718"/>
        <source>Oz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="769"/>
        <source>Lower Left Origin ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="796"/>
        <source>Header </source>
        <translation>En-tête</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="817"/>
        <source>Header size</source>
        <translation>Taille En-tête</translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="871"/>
        <source>Image Orientation</source>
        <translation>Orientation de l&apos;Image</translation>
    </message>
</context>
<context>
    <name>VtkImageComponentExtension</name>
    <message>
        <location filename="../../VtkImageComponentExtension.cpp" line="58"/>
        <source>Manage any file type supported by vtk in &lt;b&gt;CamiTK&lt;/b&gt;</source>
        <translation>Gérer n&apos;importe quel type de fichier supporté par vtk dans &lt;b&gt;CamiTK&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../VtkImageComponentExtension.cpp" line="165"/>
        <source>Cannot save file: unrecognized extension &quot;.</source>
        <translation>Ne peut pas sauver le fichier : extension non reconnu &quot;.</translation>
    </message>
</context>
</TS>
