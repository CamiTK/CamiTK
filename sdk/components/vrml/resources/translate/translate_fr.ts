<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>VRMLComponentExtension</name>
    <message>
        <source>Manage VRML 2 &lt;em&gt;.wrl .vrml&lt;/em&gt; files in &lt;b&gt;CamiTK&lt;/b&gt;.(very few support!)</source>
        <translation>Gérer les Fichier VRML 2 &lt;em&gt;.wrl .vrml&lt;/em&gt; dans &lt;b&gt;CamiTK&lt;/b&gt;.(trés peu de support!)</translation>
    </message>
</context>
</TS>
