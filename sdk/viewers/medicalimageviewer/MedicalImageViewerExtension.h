/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MEDICALIMAGEVIEWEREXTENSION_H
#define MEDICALIMAGEVIEWEREXTENSION_H

#include "MedicalImageViewerAPI.h"

// -- from CamiTK Core
#include <ViewerExtension.h>

/**
 * @ingroup group_sdk_libraries_core_medicalimageviewer
 *
 * @brief Manage the MedicalImageViewer viewer, instanciates and register the default "Medical Image Viewer" instance.
 *
 */
class MEDICALIMAGEVIEWER_API MedicalImageViewerExtension : public camitk::ViewerExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ViewerExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.viewer.medicalimageviewerextension")

public:
    /// Constructor
    MedicalImageViewerExtension() = default;

    /// Destructor
    virtual ~MedicalImageViewerExtension() = default;

    /// Method returning the viewer extension name
    virtual QString getName() override {
        return "Medical Image Viewer Extension";
    };

    /// Method returning the viewer extension description
    virtual QString getDescription() override {
        return "The Medical Image Viewer embeds all classical medical image views (axial, sagittal, coronal, 3D and arbitrary orientation).";
    };

    /// initialize all the viewer
    virtual void init() override;

};

#endif // MEDICALIMAGEVIEWEREXTENSION_H


