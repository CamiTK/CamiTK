/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

// ----- EXPLORER_API_H.h
#ifndef EXPLORER_API_H
#define EXPLORER_API_H

// -----------------------------------------------------------------------
//
//                           EXPLORER_API
//
// -----------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMPILE_EXPLORER_API
// flag defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// EXPLORER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#if defined(_WIN32) // MSVC and mingw

#ifdef COMPILE_EXPLORER_API
#define EXPLORER_API __declspec(dllexport)
#else
#define EXPLORER_API __declspec(dllimport)
#endif // COMPILE_EXPLORER_API

#else // for all other platforms EXPLORER_API is defined to be "nothing"

#ifndef EXPLORER_API
#define EXPLORER_API
#endif // EXPLORER_API

#endif // MSVC and mingw

#endif // EXPLORER_API_H