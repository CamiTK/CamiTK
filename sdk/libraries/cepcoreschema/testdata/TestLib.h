#ifndef TESTLIB_H
#define TESTLIB_H

// A simple test library
class TestLib {

public:
    static void test();
};

#endif // TESTLIB_H
