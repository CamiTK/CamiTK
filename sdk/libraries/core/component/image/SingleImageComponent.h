/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SINGLEIMAGEVOLUMECOMPONENT_H
#define SINGLEIMAGEVOLUMECOMPONENT_H


// -- Core stuff
#include "Component.h"
#include "Slice.h"

// -- QT stuff classes
class QMenu;

// -- VTK stuff
#include <vtkImageReslice.h>
#include <vtkWindowLevelLookupTable.h>

// -- VTK stuff classes
class vtkImageClip;

namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_component_image
 *
 * @brief
 * This Component manages sub-component of the image component seen as a single orientation only
 * (axial OR sagittal OR coronal).
 *
 * It does have a Slice representation (InterfaceBitMap), not a Geometry.
 *
 * Some frame management methods (inherited from InterfaceFrame) are overriden in order to
 * ensure nothing the frame is not modified in any way at any time.
 * The orthogonal slice (axial, coronal, or sagittal) should indeed always have a transformation to
 * the parent image component equals to Id. They are "fixed" on the ImageComponent and can not move freely.
 * A change in the frame of the parent image component is therefore automatically applied to the
 * single component.
 */
class CAMITK_API SingleImageComponent : public camitk::Component {
    Q_OBJECT

    /// Set Axial, Coronal and Sagittal Slices visible in 3D
    Q_PROPERTY(bool viewSliceIn3D READ getViewSliceIn3D WRITE setViewSliceIn3D)

public:
    /// Constructor
    SingleImageComponent(Component* parentComponent, Slice::SliceOrientation, const QString& name, vtkSmartPointer<vtkWindowLevelLookupTable> lut);

    /// Destructor
    ~SingleImageComponent() override = default;

    /// rewritten from Component so that the Component can call the ManagerComponent
    virtual void pixelPicked(double, double, double) override;

    /// rewritten to synchronize everyone
    virtual void setSelected(const bool, const bool) override;

    /// new method used to call the Component set selected
    virtual void singleImageSelected(const bool);

    ///@cond
    /**
     * TODO CAMITK_DEPRECATED. This section list all the methods marked as deprecated. They are to be removed in CamiTK 6.0
     * @deprecated
     *
     * DEPRECATED (CamiTK 6.0) -> to be removed
     * Just use set/getVisibility("3D Viewer")
     */
    /// is the slice currently viewed in 3D
    CAMITK_API_DEPRECATED("Please use getVisibility(\"3D Viewer\") instead") virtual bool getViewSliceIn3D() const;

    /** set the visibility in 3D and refresh the 3D viewers
     * @param viewSliceIn3D the boolean that tell if the visibility in 3D is on or not
     */
    CAMITK_API_DEPRECATED("Please use setVisibility(\"3D Viewer\",bool) instead") virtual void setViewSliceIn3D(bool viewSliceIn3D);
    ///@endcond

    /// @name overriden from Component to manage orthogonal orientations
    /// @{
    /// These method are overriden so that they are equivalent to no-op (empty code)
    virtual void setTransform(vtkSmartPointer<vtkTransform>) override;
    virtual void resetTransform() override;
    virtual void translate(double, double, double) override;
    virtual void rotate(double, double, double) override;
    virtual void rotateVTK(double, double, double) override;
    virtual void setTransformTranslation(double, double, double) override;
    virtual void setTransformTranslationVTK(double, double, double) override;
    virtual void setTransformRotation(double, double, double) override;
    virtual void setTransformRotationVTK(double, double, double) override;
    /// @}

    /// get the slice orientation
    Slice::SliceOrientation getSliceOrientation();

protected:
    /** The concrete building of the Service (Slice in this case, for a 2D representation). */
    virtual void initRepresentation() override final;

    /// orientation of the single image component
    Slice::SliceOrientation sliceOrientation;

    /// Look up table used for this image
    vtkSmartPointer<vtkWindowLevelLookupTable> lut;
};

}

#endif
