/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "LogSyntaxHighlighter.h"

using namespace camitk;

// -------------------- constructor --------------------
LogSyntaxHighlighter::LogSyntaxHighlighter(QTextDocument* parent): SyntaxHighlighter(parent) {
    // time stamp in bold blue
    addRule(QRegularExpression(QStringLiteral(R"(^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3})")), QColor("#2a6099"), QFont::Bold);

    // info
    addRule(QRegularExpression(QStringLiteral(R"(\[INFO\s*\])")), QColor("#158466"), QFont::Bold);
    addRule(QRegularExpression(QStringLiteral(R"(\[INFO\s*\]\s*(?:\[(?!INFO|ERROR|TRACE|WARNING)[^\]]*\])?(.*)$)")), QColor("#158466"));

    // warning
    addRule(QRegularExpression(QStringLiteral(R"(\[WARNING\s*\])")), QColor("#ea7500"), QFont::Bold);
    addRule(QRegularExpression(QStringLiteral(R"(\[WARNING\s*\]\s*(?:\[(?!INFO|ERROR|TRACE|WARNING)[^\]]*\])?(.*)$)")), QColor("#ea7500"), QFont::Bold);

    // error
    addRule(QRegularExpression(QStringLiteral(R"(\[ERROR\s*\])")), QColor("#bf0041"), QFont::Bold);
    addRule(QRegularExpression(QStringLiteral(R"(\[ERROR\s*\]\s*(?:\[(?!INFO|ERROR|TRACE|WARNING)[^\]]*\])?(.*)$)")), QColor("#bf0041"), QFont::Bold);

    // method/function and line part
    addRule(QRegularExpression(QStringLiteral(R"(\[(?!INFO|ERROR|TRACE|WARNING)[^\]]*\])")), QColor("#2a6099"), QFont::Normal, true);
}