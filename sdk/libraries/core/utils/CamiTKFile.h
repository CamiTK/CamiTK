/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CAMITKFILE_H
#define CAMITKFILE_H

#include "CamiTKAPI.h"

#include <QVariant>
class QString;
class QUrl;
class QDateTime;

namespace camitk {

/**
 * @ingroup group_sdk_libraries_core
 *
 * @brief
 * CamiTKFile provides functions to read and write in the CAMITK file format
 *
 * Schematic CamiTKFile format (JSON):
 * \code
  {
  "camitk": {
      "version": "camitk-5.2",
      "timestamp": "2023-11-20T15:45:13",
      "filename": "current absoluteFilePath or URL?",
      "components": [
      {
          "uid": "uuid2",
          "name": "...",
          "filename": "..."
      },
      {
          "uid": "uuid3",
          "name": "...",
          "filename": "...",
          "className": "...",
          "uuid3-additional" : { ... }
      }
      ],
      "framesOfReference": [
      {
          "uuid": "xxxx",
          "name": ""
      }
      ],
      "transformations": [
      {
          "uuid": "xxxx",
          "name": "",
          "from": "uuid",
          "to": "uuid"
      }
      ],
      "settings": {},
      "history": {},
      "pipeline": {},
      "cep": {}
  }
  }
  \endcode
*
*/
class CAMITK_API CamiTKFile {
public:
    /**
     * Read a CamiTKFile from a filepath
    */
    static CamiTKFile load(QString filepath);
    /**
     * Read a CamiTKFile from a url (if the protocol is supported)
    */
    static CamiTKFile load(QUrl url);

    /// Version of the camitk file format
    /// This is the CamiTK version which introduced the latest changes in the camitk format.
    /// Please update this to the current version of Core::version when introducing
    // new changes to the camitk file format
    static const char* version;

    /// maximum authorized camitk file size in bytes (to prevent memory overflow)
    static const int maxFileSize;

    /**
     * Construct an empty CamiTKFile (only header information)
    */
    CamiTKFile();

    /**
     * Save the CamitTKFile to a file
    */
    bool save(QString filepath);

    /**
     * Save the CamitTKFile to a url (if the protocol is supported)
    */
    bool save(QUrl url);

    bool isValid();
    QString getVersion();
    QDateTime getTimestamp();

    void setCurrentTimestamp();
    void setCurrentVersion();

    /**
     * Add an element to the CamiTKFile with the provided key.
     *
     * If the key already exists, this will replace the current content for this key
     *
     * The QVariant may be a value (QString, QUrl, double...), a QList<QVariant> or a QMap<QVariant>
     *
    */
    void addContent(QString key, const QVariant);

    bool hasContent(QString key);

    QVariant getContent(QString key);

private:
    QVariantMap content;

};


} // camitk namespace


#endif // CAMITKFILE_H