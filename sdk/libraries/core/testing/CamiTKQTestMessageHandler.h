/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <QMessageLogContext>
#include <QString>
#include <iostream>

// Use 
// qInstallMessageHandler(camitkQTestMessageHandler);
// in initTestCase() slot to use this specific handler of qDebug() macros

void camitkQTestMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";
    switch (type) {
    case QtDebugMsg:
        std::cerr << "Debug: " << localMsg.constData() << " (" << file << ":" << function << ")" << std::endl;
        break;
    case QtInfoMsg:
        std::cerr << "Info: " << localMsg.constData() << " (" << file << ":" << function << ")" << std::endl;
        break;
    case QtWarningMsg:
        std::cerr << "Warning: " << localMsg.constData() << " (" << file << ":" << function << ")" << std::endl;
        break;
    case QtCriticalMsg:
        std::cerr << "Critical: " << localMsg.constData() << " (" << file << ":" << function << ")" << std::endl;
        break;
    case QtFatalMsg:
        std::cerr << "Fatal: " << localMsg.constData() << " (" << file << ":" << function << ")" << std::endl;
        abort();
    }
}