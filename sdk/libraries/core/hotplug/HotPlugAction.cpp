/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "HotPlugAction.h"
#include "HotPlugActionExtension.h"

// CamiTK includes
#include <Property.h>
#include <Log.h>
#include <ActionWidget.h>
#include <Core.h>

#include <QVector3D>
#include <QPoint>
#include <QColor>
#include <QLibrary>

namespace camitk {

// ------------------- Constructor -------------------
HotPlugAction::HotPlugAction(HotPlugActionExtension* extension, VariantDataModel& data) : Action(extension) {
    hotPlugExtension = extension;
    TransformEngine transformEngine;
    QJsonObject dataObject = data.getValue().toJsonObject();

    actionLibName = transformEngine.transformToString("$joinKebabCase(name)$", dataObject);

    // Setting name, description and input component
    setName(transformEngine.transformToString("$title(name)$", dataObject));
    setDescription(data["description"]);
    setComponentClassName(data["componentClass"]);

    // Setting classification family and tags
    setFamily(data["classification"]["family"]);

    for (auto tag : data["classification"]["tags"].getValue().toList()) {
        addTag(tag.toString());
    }

    // block event management
    initializationPending = true;

    // Setting the action's parameters
    if (data["parameters"].isValid() && data["parameters"].size() > 0) {
        Property* param;

        for (const VariantDataModel& paramData : data["parameters"]) {            
            QString paramName = transformEngine.transformToString("$title(name)$", paramData.getValue().toJsonObject());

            if (paramData["type"].toString() == "enum" || paramData["type"].toString() == "int") {
                param = new Property(paramName, (int) paramData["defaultValue"].getValue().toInt(), paramData["description"], paramData["unit"]);
            }
            else {
                if (paramData["type"].toString() == "double") {
                    param = new Property(paramName, (double) paramData["defaultValue"].getValue().toDouble(), paramData["description"], paramData["unit"]);
                }
                else if (paramData["type"].toString() == "bool") {
                    param = new Property(paramName, (bool) paramData["defaultValue"].getValue().toBool(), paramData["description"], paramData["unit"]);
                }
                else if (paramData["type"].toString() == "QString") {
                    QRegularExpression regExp(R"(\"(.*)\")");
                    QRegularExpressionMatch match = regExp.match(paramData["defaultValue"]);
                    if (match.hasMatch()) {
                        param = new Property(paramName, match.captured(1), paramData["description"], paramData["unit"]);
                    }
                    else {
                        CAMITK_WARNING(tr("Parameter %1 cannot convert default value %2 to QString (default value must be escaped inside quotes: \"\\\"default value\\\"\"). Using empty string instead.").arg(paramName).arg(paramData["defaultValue"]));
                        param = new Property(paramName, "", paramData["description"], paramData["unit"]);
                    }
                }
                else if (paramData["type"].toString() == "QVector3D") {
                    QRegularExpression regExp(R"(QVector3D\((\s*[-]?[\d\.]+\s*),(\s*[-]?[\d\.]+\s*),(\s*[-]?[\d\.]+\s*)\))");
                    QRegularExpressionMatch match = regExp.match(paramData["defaultValue"]);
                    if (match.hasMatch()) {
                        param = new Property(paramName, QVector3D(match.captured(1).toDouble(), match.captured(2).toDouble(), match.captured(3).toDouble()), paramData["description"], paramData["unit"]);
                    }
                    else {
                        CAMITK_WARNING(tr("Parameter %1 cannot convert default value %2 to QVector3D (default value must look like: \"QVector3D(1.0344,42.034,-345.14)\"). Using (0,0,0) instead.").arg(paramName).arg(paramData["defaultValue"]));
                        param = new Property(paramName, QVector3D(0.0, 0.0, 0.0), paramData["description"], paramData["unit"]);
                    }
                }
                else if (paramData["type"].toString() == "QColor") {
                    QRegularExpression regExp(R"(QColor\(\s*(25[0-5]|2[0-4]\d|[01]?\d{1,2})\s*,\s*(25[0-5]|2[0-4]\d|[01]?\d{1,2})\s*,\s*(25[0-5]|2[0-4]\d|[01]?\d{1,2})\s*\))");
                    QRegularExpressionMatch match = regExp.match(paramData["defaultValue"]);
                    if (match.hasMatch()) {
                        param = new Property(paramName, QColor(match.captured(1).toInt(), match.captured(2).toInt(), match.captured(3).toInt()), paramData["description"], paramData["unit"]);
                    }
                    else {
                        CAMITK_WARNING(tr("Parameter %1 cannot convert default value %2 to QColor (default value must look like: \"QColor(124,101,45)\"). Using (0,0,0) (black) instead.").arg(paramName).arg(paramData["defaultValue"]));
                        param = new Property(paramName, QColor(0.0, 0.0, 0.0), paramData["description"], paramData["unit"]);
                    }
                }
                else if (paramData["type"].toString() == "QChar") {
                    param = new Property(paramName, paramData["defaultValue"].getValue().toChar(), paramData["description"], paramData["unit"]);
                }
                else if (paramData["type"].toString() == "QPoint") {
                    QRegularExpression regExp(R"(QPoint\(\s*([-]?[\d]+)\s*,\s*([-]?[\d]+\s*)\))");
                    QRegularExpressionMatch match = regExp.match(paramData["defaultValue"]);
                    if (match.hasMatch()) {
                        param = new Property(paramName, QPoint(match.captured(1).toInt(), match.captured(2).toInt()), paramData["description"], paramData["unit"]);
                    }
                    else {
                        CAMITK_WARNING(tr("Parameter %1 cannot convert default value %2 to QPoint (default value must look like: \"QPoint(124,101)\"). Using (0,0) instead.").arg(paramName).arg(paramData["defaultValue"]));
                        param = new Property(paramName, QPoint(0, 0), paramData["description"], paramData["unit"]);
                    }
                }
            }

            if (paramData["group"].isValid() && paramData["group"].toString() != "") {
                param->setGroupName(paramData["group"]);
            }

            QStringList numericConstraints = { "minimum", "maximum", "decimals", "singleStep"};
            for (auto& e : numericConstraints) {
                if (paramData[e].isValid() && paramData[e].toString() != "") {
                    param->setAttribute(e, paramData[e].getValue().toDouble());
                }
            }

            if (paramData["regExp"].isValid() && paramData["regExp"].toString() != "") {
                param->setAttribute("regExp", QRegularExpression(paramData["regExp"].toString()));
            }

            if (paramData["readOnly"].isValid() && paramData["readOnly"].getValue().toBool()) {
                param->setReadOnly(true);
            }

            if (paramData["type"].toString() == "enum") {
                // Set the enum type
                QString enumTypeName = transformEngine.transformToString("$upperCamelCase(p.name)$Enum", paramData.getValue().toJsonObject());
                param->setEnumTypeName(enumTypeName);
                // Populate the enum values
                param->setAttribute("enumNames", paramData["enumValues"].getValue().toStringList());
            }

            // Finally, add the new parameter
            addParameter(param);
        }
    }

    // setup UserActionLib functors
    updateSucceeded = false;
    updateLibrary();

    // call user-defined init()
    if (init != nullptr) {
        init(this);
    }

    // immediately take the property changes into account
    setAutoUpdateProperties(true);

    // initialization is finish, all events are unblocked
    initializationPending = false;

    if (data["notEmbedded"].isValid() && data["notEmbedded"].getValue().toBool()) {
        setEmbedded(false);
    }
}

// ------------------- needsUpdate -------------------
bool HotPlugAction::needsUpdate() {
    if (!updateSucceeded) {
        return true;
    }

    QDateTime lastModified = QFileInfo(actionLib.fileName()).lastModified();
    return (actionLib.fileName().isEmpty() || (lastModified > lastLoaded));
}

// ------------------- updateLibrary -------------------
bool HotPlugAction::updateLibrary() {
    // Do nothing if the library was not changed since the last update or
    // if it being rebuild
    if (!needsUpdate()) {
        updateSucceeded = true;
        return updateSucceeded;
    }

    // force reload
    if (actionLib.isLoaded()) {
        actionLib.unload();
    }

    // reset functors
    process = nullptr;
    init = nullptr;
    getUI = nullptr;
    targetDefined = nullptr;
    parameterChanged = nullptr;

    // location of the UserActionLib is relative to the CamiTK extension file
    QString sourcePath = QFileInfo(getExtension()->getCamiTKExtensionFilePath()).absolutePath();
    QDir buildExtensionDir;
    buildExtensionDir.setPath(sourcePath + "/build/lib/" + QString(Core::shortVersion) + "/actions");

    // There are no prefix for libraries on Windows
    QString libPrefix = "";
    // There might be a debug postfix on Windows
    QString debugPostfix = "";
#if defined(_WIN32) || defined(_WIN64)
    if (Core::isDebugBuild()) {
        debugPostfix = QString(Core::debugPostfix);
    }  
#else
    // but on Unix and Apple, all libraries are prefixed with "lib"
    libPrefix = "lib";
#endif  
    QString libFilePath = buildExtensionDir.filePath(libPrefix + actionLibName + debugPostfix);

    // QLibrary will look for { "*.so", "*.dll", "*.dylib"} depending on the OS
    actionLib.setFileName(libFilePath);

    // load and update functors
    if (actionLib.load()) {
        // resolve symbols
        process = (ProcessFunction) actionLib.resolve("process");

        // check
        if (process == nullptr) {
            CAMITK_WARNING(QString(R"(Error loading %1: %2\nCannot find mandatory \"process(..)\" method for action \"%3\".
            This action will therefore not be able to do anything. Please verify the CamiTKActionImplementation section and check that the method process(..) is defined with the proper signature: \"Action::ApplyStatus process(Action* self)\")").arg(actionLib.fileName()).arg(actionLib.errorString()).arg(getName()));
            updateSucceeded = false;
        }
        else {
            init = (VoidFunction) actionLib.resolve("init");
            getUI = (GetUIFunction) actionLib.resolve("getUI");
            targetDefined = (VoidFunction) actionLib.resolve("targetDefined");
            parameterChanged = (ParameterChangedFunction) actionLib.resolve("parameterChanged");
            hotPlugExtension->watchActionLibrary(actionLib.fileName(), this);
            updateSucceeded = true;
            lastLoaded = QDateTime::currentDateTime();
        }
    }
    else {
        CAMITK_INFO(QString("%1 (%2) cannot be loaded:\n%3\nTry to rebuild the extension.").arg(libFilePath).arg(actionLib.fileName()).arg(actionLib.errorString()));
        updateSucceeded = false;
    }

    return updateSucceeded;
}

// ------------------- getWidget -------------------
QWidget* HotPlugAction::getWidget() {
    // call UserAction method
    if (targetDefined  != nullptr) {
        targetDefined(this);
    }
    if (actionWidget == nullptr) {
        // first call: check the user defined widget
        if (getUI != nullptr) {
            actionWidget = getUI(this);
        }
        if (actionWidget != nullptr) {
            return actionWidget;
        }
        else {
            // no user defined UI, use default
            return Action::getWidget();
        }
    }
    else {
        ActionWidget* defaultActionWidget = dynamic_cast<ActionWidget*>(actionWidget);
        if (defaultActionWidget != nullptr) {
            // this is a default action widget, make sure the widget has updated targets
            defaultActionWidget->update();
        }
        return actionWidget;
    }
}

// ------------------- apply -------------------
Action::ApplyStatus HotPlugAction::apply() {
    // call user-defined process()
    if (process != nullptr) {
        return process(this);
    }
    else {
        return Action::ERROR;
    }
}


// ---------------------- event ----------------------------
bool HotPlugAction::event(QEvent* e) {
    if (e->type() == QEvent::DynamicPropertyChange && !initializationPending) {
        e->accept();
        QDynamicPropertyChangeEvent* changeEvent = dynamic_cast<QDynamicPropertyChangeEvent*>(e);

        if (changeEvent != nullptr) {
            // call user-defined parameterChanged()
            if (parameterChanged != nullptr) {
                parameterChanged(this, changeEvent->propertyName());
            }
            return true;
        }
        else {
            return false;
        }
    }

    // this is important to continue the process if the event is a different one
    return QObject::event(e);
}

} // namespace camitk