/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "HotPlugExtensionManager.h"

#include <Application.h>
#include <Log.h>

#include <CMakeProjectManager.h>
#include <CamiTKExtensionModel.h>

#include <QFileInfo>
#include <QSettings>

namespace camitk {

// -------------------- getLoadedExtensionFiles --------------------
const QStringList& HotPlugExtensionManager::getLoadedExtensionFiles() {
    static QStringList keys;
    keys = loadedExtensions().keys();
    return keys;
}

// -------------------- getRegisteredExtensionFiles --------------------
const QStringList HotPlugExtensionManager::getRegisteredExtensionFiles() {
    QSettings& settings = Application::getSettings();
    settings.beginGroup(Application::getName() + ".HotPlug");
    QStringList camitkExtensionFilesInSettings = settings.value("CamiTKExtensionFiles", QVariant(QStringList())).toStringList();
    settings.endGroup();
    return camitkExtensionFilesInSettings;
}

// -------------------- getLoadedExtensions --------------------
const QList<ActionExtension*>& HotPlugExtensionManager::getLoadedExtensions() {
    static QList<ActionExtension*> actionExtensions;
    actionExtensions.clear();
    for (auto ae : loadedExtensions().values()) {
        actionExtensions.append(ae);
    }
    return actionExtensions;
}

// -------------------- loadedExtensions --------------------
QMap<QString, HotPlugActionExtension*>& HotPlugExtensionManager::loadedExtensions() {
    static QMap<QString, HotPlugActionExtension*> loadedExtensionList;

    return loadedExtensionList;
}

// -------------------- registerExtension --------------------
bool HotPlugExtensionManager::registerExtension(const QString& camitkExtensionFilePath) {
    // First check if the action's type can be registered
    CamiTKExtensionModel camitkExtensionModel(camitkExtensionFilePath);
    QString generationType = camitkExtensionModel.getModel()["generationType"];
    if (generationType != "HotPlug") {
        CAMITK_WARNING_ALT(QString("Cannot register extension %1: generation type \"%2\" cannot be registered").arg(camitkExtensionFilePath).arg(generationType));
        return false;
    }

    QSettings& settings = Application::getSettings();
    settings.beginGroup(Application::getName() + ".HotPlug");
    QStringList camitkExtensionFilesInSettings = settings.value("CamiTKExtensionFiles", QVariant(QStringList())).toStringList();

    bool registered = true;
    if (!camitkExtensionFilesInSettings.contains(camitkExtensionFilePath)) {
        HotPlugActionExtension* actionExtension = load(camitkExtensionFilePath);
        if (actionExtension) {
            QStringList camitkExtensionFilesInSettings = settings.value("CamiTKExtensionFiles", QVariant(QStringList())).toStringList();
            camitkExtensionFilesInSettings.append(camitkExtensionFilePath);
            settings.setValue("CamiTKExtensionFiles", camitkExtensionFilesInSettings);
        }
        else {
            registered = false;
        }
    }

    settings.endGroup();
    return registered;
}

// -------------------- unregisterExtension --------------------
bool HotPlugExtensionManager::unregisterExtension(const QString& camitkExtensionFilePath) {
    // remove from settings (unconditionally)
    QSettings& settings = Application::getSettings();
    settings.beginGroup(Application::getName() + ".HotPlug");
    QStringList camitkExtensionFilesInSettings = settings.value("CamiTKExtensionFiles", QVariant(QStringList())).toStringList();
    if (camitkExtensionFilesInSettings.contains(camitkExtensionFilePath)) {
        camitkExtensionFilesInSettings.removeAll(camitkExtensionFilePath);
        settings.setValue("CamiTKExtensionFiles", camitkExtensionFilesInSettings);
    }
    settings.endGroup();

    // remove from memory and return true if successful
    return unload(camitkExtensionFilePath);
}

// -------------------- loadAll --------------------
bool HotPlugExtensionManager::loadAll() {
    bool success = true;

    for (auto camitkExtensionFile : getRegisteredExtensionFiles()) {
        success = success && load(camitkExtensionFile);
    }
    return success;
}

// -------------------- unloadAll --------------------
bool HotPlugExtensionManager::unloadAll() {
    bool success = true;
    for (auto& loadedExtensionFile : loadedExtensions().keys()) {
        success = success && unload(loadedExtensionFile);
    }
    loadedExtensions().clear();
    return success;
}

// -------------------- load --------------------
HotPlugActionExtension* HotPlugExtensionManager::load(const QString& camitkExtensionFilePath, bool forceRebuild) {
    HotPlugActionExtension* actionExtension = loadedExtensions().value(camitkExtensionFilePath);
    if (!actionExtension) {
        // instantiate extension
        actionExtension = HotPlugActionExtension::newHotPlugActionExtension(camitkExtensionFilePath, forceRebuild);
        if (actionExtension) {
            int expected = actionExtension->declaredActionCount();
            // initialize all actions
            actionExtension->init();
            int actual = actionExtension->getActions().size();
            if (actual != expected) {
                delete actionExtension;
                CAMITK_WARNING_ALT(QString("Extension %1 declared %2 actions but only %3 were instantiated.").arg(camitkExtensionFilePath).arg(expected).arg(actual));
                return nullptr;
            }
            else {
                //-- register all actions
                int registered = Application::registerAllActions(actionExtension);
                // check that everything went well
                if (registered == expected) {
                    loadedExtensions().insert(camitkExtensionFilePath, actionExtension);
                    return actionExtension;
                }
                else {
                    delete actionExtension;
                    return nullptr;
                }
            }
        }
    }
    return actionExtension;
}

// -------------------- unload --------------------
bool HotPlugExtensionManager::unload(const QString& camitkExtensionFilePath) {
    if (loadedExtensions().contains(camitkExtensionFilePath)) {
        HotPlugActionExtension* actionExtension = loadedExtensions().value(camitkExtensionFilePath);
        // -- unregister actions from application
        Application::unregisterAllActions(actionExtension);
        //-- delete extensions (and all its actions)
        delete actionExtension;

        // remove from the loaded list
        loadedExtensions().remove(camitkExtensionFilePath);
        return true;
    }
    else {
        return false;
    }
}

// -------------------- rebuild --------------------
void HotPlugExtensionManager::rebuild(const QString& camitkExtensionFilePath) {

    // remove build
    QString sourcePath = QFileInfo(camitkExtensionFilePath).absolutePath();
    QDir buildExtensionDir;
    buildExtensionDir.setPath(sourcePath + "/build");
    if (buildExtensionDir.exists()) {
        buildExtensionDir.removeRecursively();
    }

    // rebuild now
    CMakeProjectManager manager(camitkExtensionFilePath);
    CAMITK_INFO_ALT(QString("Building %1").arg(camitkExtensionFilePath));
    if (!manager.success()) {
        CAMITK_WARNING_ALT(QString("Failed to initialize build for \"%1\"").arg(camitkExtensionFilePath));
    }
    else {
        manager.setStages({CMakeProjectManager::Check_System, CMakeProjectManager::Generate_Source_Files, CMakeProjectManager::Configure_CMake, CMakeProjectManager::Build_Project});

        // connect signal to follow progress
        connect(&manager, &CMakeProjectManager::stageStarted, [ = ](const QString & stage) {
            CAMITK_INFO_ALT(QString("Starting %1...").arg(stage));
        });

        connect(&manager, &CMakeProjectManager::stageFinished, [ = ](const QString & stage, bool success, const QString & output) {
            CAMITK_WARNING_IF_ALT(!success, QString("%1 finished %2").arg(stage).arg((success) ? "[OK]" : "[FAILED]"));
            CAMITK_WARNING_IF_ALT(!success, QString("Output:\n%1\n").arg(output));
            CAMITK_INFO_IF_ALT(success, QString("%1 finished %2").arg(stage).arg((success) ? "[OK]" : "[FAILED]"));
        });

        connect(&manager, &CMakeProjectManager::allStagesFinished, [ = ](bool status) {
            if (!status) {
                CAMITK_WARNING_ALT(QString("Building %1 failed.").arg(camitkExtensionFilePath));
            }
        });

        manager.start();
    }
}

// -------------------- setAlwaysRebuild --------------------
void HotPlugExtensionManager::setAlwaysRebuild(bool alwaysRebuild) {
    QSettings& settings = Application::getSettings();
    settings.beginGroup(Application::getName() + ".HotPlug");
    settings.setValue("AlwaysRebuild", alwaysRebuild);
    settings.endGroup();
}

// -------------------- getAlwaysRebuild --------------------
bool HotPlugExtensionManager::getAlwaysRebuild() {
    QSettings& settings = Application::getSettings();
    settings.beginGroup(Application::getName() + ".HotPlug");
    bool alwaysRebuild = settings.value("AlwaysRebuild", false).toBool();
    settings.endGroup();
    return alwaysRebuild;
}

} // namespace camitk