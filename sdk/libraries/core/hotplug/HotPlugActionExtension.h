/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __HOTPLUG_ACTION_EXTENSION__
#define __HOTPLUG_ACTION_EXTENSION__

#include <ActionExtension.h>

#include <QTimer>
#include <QFileSystemWatcher>
#include <QMutexLocker>

namespace camitk {

class HotPlugAction;

/**
 * @brief An ActionExtension that can be created on the fly from a camitk extension file
 *
 */
class CAMITK_API HotPlugActionExtension : public ActionExtension {
Q_OBJECT

protected:
    /// constructor
    HotPlugActionExtension(const QString& camitkFilePath, bool forceRebuild = false);

public:
    static HotPlugActionExtension* newHotPlugActionExtension(const QString& camitkFilePath, bool forceRebuild);

    /// destructor
    ~HotPlugActionExtension() override;

    /// returns the action extension name (to be overriden in your ActionExtension)
    virtual QString getName() override;

    /// returns the action extension small description (to be overriden in your ActionExtension)
    virtual QString getDescription() override;

    /// get the file path of the CamiTK extension file that generated this extension
    /// @return null QString if this extension was not generated from a CamiTK extension file (legacy extension)
    virtual QString getCamiTKExtensionFilePath() const;

    /// @brief  instantiate all actions
    virtual void init() override;

    /// @return number of actions declared in the CamiTKExtensionModel
    virtual int declaredActionCount() const;

    /// add this file to the file system watcher
    /// In fact only one library is watched (the last to register)
    /// As all the library should be updated at the same time during a rebuild
    /// only one file is to be watched: the extension only needs to be
    /// informed once (this will avoid race condition in the 
    /// signal sent by fileSystemWatcher)
    void watchActionLibrary(QString libraryPath, HotPlugAction* action);

    /// @return true if all the action were instantiated and loaded successfully
    bool isSuccessfullyLoaded();

private slots:
    /// when the file watcher detect something changed
    void extensionRebuilt(const QString & path, int timerCallCount = 0);

    /// ensure all files in watchedUserActionLibraries are watched by fileSystemWatcher
    void updateWatchedFiles();
    
private:
    QString name;
    QString description;
    QString camitkFilePath;
    QMutex reloadMutex;
    /// true if the extension as well ass all the actions libraries were loaded successfully
    bool successfullyLoaded;

    /// @brief watch for the library changes and reload when needed
    /// new HotPlugAction register themselves by calling watchActionLibrary()
    QFileSystemWatcher fileSystemWatcher;

    /// key = shared library file path to watch corresponding to the HotPlugAction
    QMap<QString, HotPlugAction*> watchedUserActionLibraries;
};

} // namespace camitk

#endif // __HOTPLUG_ACTION_EXTENSION__
