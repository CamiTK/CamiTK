/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __HOTPLUG_EXTENSION_MANAGER__
#define __HOTPLUG_EXTENSION_MANAGER__

#include "HotPlugActionExtension.h"

#include <QObject>
#include <QStringList>

namespace camitk {

/**
 * @brief Manages all the HotPlug extension
 * 
 * HotPlug extension consists of 
 * - a CamiTK extension path
 * - user (developer) source code
 * 
 * HotPlug extension are registered in a CamiTK application using their CamiTK extension files.
 * Registered extensions are loaded during application starts by instantiating a HotPlugActionExtension
 * and corresponding HotPlugAction.
 * 
 */
class CAMITK_API HotPlugExtensionManager : public QObject {

Q_OBJECT

public:
    /// get the list of loaded camitk extension file (file path)
    static const QStringList& getLoadedExtensionFiles();

    /// get the list of all loaded ActionExtension* 
    static const QList<ActionExtension*>& getLoadedExtensions();

    /// get the list of registered camitk extension file (file path)
    static const QStringList getRegisteredExtensionFiles();

    /** Register an extension described by a CamiTK extension file.
     * Registering will load all the action created by this extension.
     * @return false if the path is not a valid CamiTK extension file of something went wrong
     * during registration and true if extension is already registered or if it was loaded + registered
    */
    static bool registerExtension(const QString& camitkExtensionFilePath);

    /** Unregister a camitk file.
     * Unregistering will also remove the provided actions from the application.
     * @return false if the path was not registered or something went wrong during unregistration
     */
    static bool unregisterExtension(const QString& camitkExtensionFilePath);

    /// load all the CamiTK extension files registered in the settings
    /// @return false if at least one registered extension could not be loaded
    static bool loadAll();

    /// unload all the currently loaded CamiTK extension files (delete from memory and from current loaded list)
    /// @return false if at least one registered extension could not be unloaded
    static bool unloadAll();

    /// load a CamiTK extension file
    static HotPlugActionExtension* load(const QString& camitkExtensionFilePath, bool forceRebuild = false);

    /// unload a CamiTK extension file
    static bool unload(const QString& camitkExtensionFilePath);

    /// rebuild given extension (located using its CamiTK extension file)
    static void rebuild(const QString& camitkExtensionFilePath);

    /// set the corresponding settings for persistence
    static void setAlwaysRebuild(bool alwaysRebuild);

    /// get the corresponding settings from persistence
    static bool getAlwaysRebuild();
    
private:

    /// get the map of registered camitk extension file
    /// key = CamiTK extension file
    static QMap<QString, HotPlugActionExtension*> & loadedExtensions();

};

} // namespace camitk

#endif // __HOTPLUG_EXTENSION_MANAGER__