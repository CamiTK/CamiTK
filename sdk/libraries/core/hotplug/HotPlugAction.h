/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __HOTPLUG_ACTION__
#define __HOTPLUG_ACTION__

#include <Action.h>
#include <VariantDataModel.h>
#include <TransformEngine.h>

#include "HotPlugActionExtension.h"

#include <QLibrary>

namespace camitk {

/// @brief function signatures in the UserActionLib
typedef void (*VoidFunction)(Action*);
typedef QWidget* (*GetUIFunction)(Action*);
typedef Action::ApplyStatus (*ProcessFunction)(Action* self);
typedef void (*ParameterChangedFunction)(Action* self, QString parameterName);

/**
 * @brief An Action that can be created on the fly 
 *
 */
class CAMITK_API HotPlugAction : public Action {
Q_OBJECT
    
public: 
    /// Default Constructor 
    /// calls user-defined init()
    HotPlugAction(HotPlugActionExtension *actionExtension, VariantDataModel& data);

    /// Default Destructor
    virtual ~HotPlugAction() = default;

    /// Calls user-defined targetDefined() and getUI()
    virtual QWidget* getWidget() override;

    /// manage property change immediately
    virtual bool event(QEvent* e) override;

    /// returns true if and only if the user library has changed since the last update
    /// or update was not completed successfully
    bool needsUpdate();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components, 
      * i.e., instances of "$componentClass$" (or a subclass).
      */
    virtual Action::ApplyStatus apply() override;

    /// update the library and returns true if the process method was found
    bool updateLibrary();

private:
    /// The current action class library
    QLibrary actionLib;

    /// The library basename (without prefix or suffix)
    QString actionLibName;

    /// @brief the last time the user library file was loaded
    /// (used to update the library only if it has changed since the last load)
    QDateTime lastLoaded;

    /// @brief was the last update successful or not
    bool updateSucceeded;

    /// do not manage event during initialization
    bool initializationPending;

    /// where the action is managed
    HotPlugActionExtension* hotPlugExtension;

    ///@name UserActionLib functions to call
    ///@{
    /// init() is called from the constructor for user-defined initialization
    VoidFunction init;
    
    /// process() is called from apply() for user-defined processing
    ProcessFunction process;

    /// getUI() is called from getWidget() to get the user-defined widget
    /// If getUI() returns nullptr or is not defined in the UserActionLib, use the default action widget
    GetUIFunction getUI;

    /// targetDefined() is called from getWidget to notify the UserActionLib
    /// that the action's target(s) have changed.
    /// (the user might have to update something in the action logic)
    /// Note: the UserActionLib can call self->getTargets() at any time to get the list of current targets
    VoidFunction targetDefined;

    /// parameterChanged(name:QString) is called from event(..) to notify when the user has changed the parameter
    /// of the given name
    ParameterChangedFunction parameterChanged;
    ///@}
};

} // namespace camitk

#endif // __HOTPLUG_ACTION__
