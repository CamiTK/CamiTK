/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef INTERACTIVE_VIEWER_FRAME_H
#define INTERACTIVE_VIEWER_FRAME_H

// -- Core stuff
#include "CamiTKAPI.h"

//-- QT stuff
#include <QFrame>

namespace camitk {

class InteractiveViewer;

/**
  * @ingroup group_sdk_libraries_core_viewer
  *
  * @brief
  * InteractiveViewerFrame is the basic container for the InteractiveViewer widget.
  *
  * It just differ from QFrame because:
  * - it delegates all key events to its InteractiveViewer
  * - it tells its InteractiveViewer when the visibility status changed so that the InteractiveViewer does
  *   not have to refresh when its widget is not visible.
  */
class CAMITK_API InteractiveViewerFrame : public QFrame {
public:
    /// default constructor
    InteractiveViewerFrame(QWidget*, InteractiveViewer*);

    /// default constructor
    ~InteractiveViewerFrame() = default;

    /// Handle keyboard events in the scene frame, just send everything to InteractiveViewer!
    void keyPressEvent(QKeyEvent*) override;

protected:
    void showEvent(QShowEvent*) override;

    InteractiveViewer* myInteractiveViewer;
};

}
#endif // INTERACTIVE_VIEWER_FRAME_H
