/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SCREENSHOTFORMATINFO_H
#define SCREENSHOTFORMATINFO_H

#include <QMap>

namespace camitk {

/// class containing all information concerning exporting images (screenshot) and the different supported
/// format and extension.
class ScreenshotFormatInfo {
public:
    /// \enum ScreenshotFormat list of supported screenshot export formats
    enum ScreenshotFormat {
        PNG = 0,  ///< Portable Network Graphics
        JPG,      ///< JPEG
        BMP,      ///< Bitmap
        PS,       ///< PostScript
        EPS,      ///< Encapsulated PostScript
        PDF,      ///< Portable Document Format
        TEX,      ///< LaTeX (only the text is exported)
        SVG,      ///< Scalable Vector Graphics
        OBJ,      ///< Alias Wavefront .OBJ
        RIB,      ///< RenderMan/BMRT .RIB
        VRML,     ///< VRML 2.0
        NOT_SUPPORTED
    };

    /// the corresponding type (key)
    ScreenshotFormat type{NOT_SUPPORTED};

    /// file extension (suffix)
    QString extension;

    /// file format description
    QString description;

    /// Constructor
    ScreenshotFormatInfo(ScreenshotFormat t, QString e, QString d) : type(t), extension(std::move(e)), description(std::move(d)) {}

    /// default constructor
    ScreenshotFormatInfo() :  extension(""), description("Not supported") {}

    /// return a list of supported format that can be used in a QFileDialog
    static const QString fileFilters();

    // get the default format (PNG) information
    static const ScreenshotFormatInfo* defaultFormat();

    /// return the information concerning the supporting format
    /// using an index corresponding to the enum (check index validity)
    static const ScreenshotFormatInfo* get(unsigned int);

    /// retun the information concerning the supporting format
    /// using an index corresponding to the enum (check index validity)
    static const ScreenshotFormatInfo* get(ScreenshotFormat);

    /// get the information from the extension (QString)
    static const ScreenshotFormatInfo* get(QString);

    /// get the map
    static const QMap<ScreenshotFormat, ScreenshotFormatInfo*> getMap();

private:
    /// build the map
    static QMap<ScreenshotFormat, ScreenshotFormatInfo*> initMap();
};


} // namespace

#endif // SCREENSHOTFORMATINFO_H
