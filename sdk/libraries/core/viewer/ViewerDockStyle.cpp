/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ViewerDockStyle.h"
#include "Viewer.h"

#include "Log.h"

namespace camitk {

//---------------------- constructor ------------------------
ViewerDockStyle::ViewerDockStyle(Viewer* viewer) : QProxyStyle(nullptr) {
    icon = new QIcon(viewer->getIcon());
}

//---------------------- destructor ------------------------
ViewerDockStyle::~ViewerDockStyle() {
    delete icon;
}

//---------------------- drawControl ------------------------
void ViewerDockStyle::drawControl(ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const {
    if (element == QStyle::CE_DockWidgetTitle) {
        // height/width of the icon reserved space
        int iconReservedSize = pixelMetric(QStyle::PM_ToolBarIconSize);
        // margin of title from frame
        int margin = baseStyle()->pixelMetric(QStyle::PM_DockWidgetTitleMargin);
        // generate extra margin around the icon
        int spaceFactor = 2;

        // dimension of the resized icon
        int iconActualSize = iconReservedSize - margin * 2 * spaceFactor;

        // starting position of the icon option.rect is the dock widget title rectangle
        int posX = margin * spaceFactor + option->rect.left();
        // center in height
        int posY = option->rect.center().y() - iconActualSize / 2;
        // set at bottom
        posY += margin * spaceFactor / 2;
        QPoint iconPosition(posX, posY);

        // draw the pixmap
        painter->drawPixmap(iconPosition, icon->pixmap(iconActualSize, iconActualSize));

        const_cast<QStyleOption*>(option)->rect = option->rect.adjusted(iconActualSize, 0, 0, 0);
    }

    baseStyle()->drawControl(element, option, painter, widget);
}

}

