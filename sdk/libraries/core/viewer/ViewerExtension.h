/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

#ifndef VIEWER_EXTENSION_H
#define VIEWER_EXTENSION_H

#include "CamiTKAPI.h"

// -- QT stuff
#include <QRegularExpression>
#include <QTranslator>

namespace camitk {
class Viewer;

/// register a new viewer and create a default instance using the name of the class
/// (note that the regular expression used here results in the default viewer's name of being
/// the name of the viewer class where the words are separated  space, e.g. the default
/// FooBarViewer is called "Foo Bar Viewer")
/// \note #className surrounds macro argument with double quotes converting it to string
#define registerDefaultViewer(className)                        setManagedMetaObject(&className::staticMetaObject); \
                                                                registerViewer(new className(QString(#className).replace(QRegularExpression("([^A-Z\\s])([A-Z])"),"\\1 \\2").replace(QRegularExpression("([A-Z]+)([A-Z][a-z])"),"\\1 \\2")));

/// register a new viewer, instance of X, its name is Y
#define registerNewViewer(className, instanceName)              setManagedMetaObject(&className::staticMetaObject); \
                                                                registerViewer(new className(QString(instanceName)))

/**
* @ingroup group_sdk_libraries_core_viewer
*
* @brief
* This class describes what is a generic Action extension.
* To add a ActionExtension to CamiTK core, write a new class that inherits from this class.
*
* The following methods HAVE to be redefined in your subclass:
* - getName: return the name of your extension
* - getDescription: return a small description
* - init: a simple enough method, just call registerNewAction(MyAction) for any
*   MyAction class inheriting from camitk::Action
*
* ViewerExtension implements a specific Factory pattern (see getNewInstance())
*
* Each time a viewer is registered, call setManagedMetaObject (the fist registered viewer will
* register the managed class meta object that can be reused later on to instantiate new viewer).
*
* Use registerDefaultViewer() or registerNewViewer() in your inherited viewer extension to register
* and instantiate new viewers.
*
* Using registerDefaultViewer() registers the class and instantiate a default viewer of the same name.
* \note the default viewer's name is the same as the class name with all words separated by spaces.
*
* This is important as this name will be shown in the GUI.
* Therefore:
* \code
* registerDefaultViewer(FooBarViewer);
* \endcode
* will instantiate the default FooBarViewer viewer called "Foo Bar Viewer".
*
* To get the default FooBarViewer instance, use Application::getViewer("Foo Bar Viewer").
*
* @see InteractiveSliceViewerExtension For an example of a viewer extension registering more than one viewer instance.
*/
class CAMITK_API ViewerExtension : public QObject {

protected:
    /// constructor
    ViewerExtension();

public:
    /// destructor
    ~ViewerExtension() override;

    /// returns the viewer extension name (to be overriden in the derived class)
    virtual QString getName() = 0;

    /// returns the viewer extension small description (to be overriden in the derived class)
    virtual QString getDescription() = 0;

    /// this method should just call registerNewViewer(MyViewer) for any MyViewer class you need to register by this extension
    virtual void init() = 0;

    /// get the list of viewer instances registered by this extension
    const ViewerList& getViewers();

    /// set the file path (once loaded as a dynamic library) = where this extension was loaded
    void setLocation(const QString loc);

    /// get the file path (location of the .dll/.so/.dylib) of this plugin
    QString getLocation() const;

    /// Load, for the selected langage (asked to the Application), the associated .qm file
    void initResources();

    /// get the class name of the viewer managed by this extension
    QString getViewerClassName();

    /// factory: create a new viewer instance and register it
    /// @return a new instance of the given name, or the previously registered instance if it already exist
    Viewer* getNewInstance(QString name);

protected:
    /// register an viewer instance
    /// The first call will also register the managed viewer class name in viewerClassName
    /// and its static meta object in
    bool registerViewer(Viewer*);

    /// the list of viewers
    ViewerList viewers;

    /// The first time this method is called, it sets the static meta object
    void setManagedMetaObject(const QMetaObject*);

private:
    /// the shared lib (.so, .dll or .dylib) used to instantiate the ComponentExtension subclass instance
    QString dynamicLibraryFileName;

    /// Provide internationalization support for text output.
    QTranslator* translator;

    /// static meta object of the viewer class managed by this extension
    const QMetaObject* managedMetaObject;
};

}
// -------------------- declare the interface for QPluginLoader --------------------
Q_DECLARE_INTERFACE(camitk::ViewerExtension, "TIMC-IMAG. Viewer Extension/2.1")    //TODO use variable from CMake?

#endif //VIEWER_EXTENSION_H

