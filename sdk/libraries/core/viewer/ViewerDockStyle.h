/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VIEWERDOCKSTYLE_H
#define VIEWERDOCKSTYLE_H

// -- Core stuff
#include "CamiTKAPI.h"

#include <QProxyStyle>
#include <QStyleOption>
#include <QPainter>

namespace camitk {
class Viewer;

/**
  * @ingroup group_sdk_libraries_core_viewer
  *
  * @brief a specific style made for QDockWidget that adds an icon to the dock widget title
  *
  * Greatly inspired by https://stackoverflow.com/a/3482795
  */
class CAMITK_API ViewerDockStyle : public QProxyStyle {
    Q_OBJECT
public:
    /// constructor just set the viewer that is using this style
    ViewerDockStyle(Viewer*);

    /// destructor
    virtual ~ViewerDockStyle();

    /// override method from QProxyStyle
    virtual void drawControl(ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget = 0) const override;

private:
    /// icon generated from the viewer pixmap
    QIcon* icon;
};

}

#endif // VIEWERDOCKSTYLE_H
