/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef __CAMITK_EXTENSION_MODEL__
#define __CAMITK_EXTENSION_MODEL__

#include <QJsonArray>
#include <QJsonObject>
#include <QFile>

#include <VariantDataModel.h>

/**
 * \class CamiTK Extension Model
 * This class manages a CamiTK Extension model (stored as a VariantDataModel).
 * It can load/save the model to CamiTK extension file (JSON)
 * 
 * Role:
 * - Reads the given CamiTK file (.json)
 * - Provides the corresponding VariantDataModel (as a reference)
 * - Saves the current state of the model to a given file
 * - The managed model can be set using setModel(..)
 * 
 * \note the static newExtension(..) method create a QVariantMap 
 * that can be used to add an new extension of the given type to a model.
 * 
 * The expected JSON file must follow the following format:
```json
{
    "camitk": {
        "extensions": {
            "actions": [ ... ],
            "name":"..."
        },
        "timestamp": "2024-05-17T16:27:05",
        "version": "camitk-5.2"
    }
}
```

 * Instead the `actions` array (or as well as, at the same level), you may add a `components`,
 * `viewers`, `libraries` and `applications` arrays.
 *
 *
 */
class CamiTKExtensionModel {

public:
    /// constructor
    CamiTKExtensionModel(const QString& filePath = QString());
    
    /// reset the model from scratch (the model will then be empty)
    void clear();

    /// read the given file and creates the corresponding configuration
    bool load(const QString& filePath);

    /// save the current config to the current filePath or given filePath
    /// and update camitkFile if needed
    /// **Warning** it does overwrite the previous content of filePath
    /// @return false if the file could not be reopened for writing
    bool save(const QString& filePath = QString());

    /// was the data model modified since last read
    bool isModified();

    /// the camitk extension data model as a JSON string
    QString toJSON();

    /// get access to the VariantDataModel
    VariantDataModel& getModel();

    /// modify the model
    void setModel(VariantDataModel&);

    /// reset the model with empty extension arrays.
    ///
    /// After calling this method, all the extension type arrays are empty
    /// and the licence is set to LGPL-3 CamiTK:
    ///
    ///```json
    /// {
    ///    "name": "...",
    ///    "license": "LGPL-3 CamiTK",
    ///    "actions": [],
    ///    "components": [],
    ///    "viewers": [],
    ///    "libraries": [],
    ///    "applications": [] 
    /// }
    ///```
    void resetModel(QString name);
    
private:
    /// the "extensions" map object
    VariantDataModel extensions;
};

#endif // __CAMITK_EXTENSION_MODEL__