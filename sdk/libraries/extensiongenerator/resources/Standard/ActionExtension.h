$licenseComment$

#ifndef __$upperSnakeCase(name)$_ACTIONEXTENSION__
#define __$upperSnakeCase(name)$_ACTIONEXTENSION__

#include <ActionExtension.h>

class $upperCamelCase(name)$ActionExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.$upperCamelCase(name)$.action.$upperCamelCase(name)$ActionExtension")

public:
    /// Constructor
    $upperCamelCase(name)$ActionExtension() : ActionExtension() {};

    /// Destructor
    virtual ~$upperCamelCase(name)$ActionExtension() = default;

    /// Method returning the action extension name
    virtual QString getName() override {
        return "$title(name)$";
    };

    /// Method returning the action extension description
    virtual QString getDescription() override {
        return R"($description$)";
    };

    /// initialize all the actions
    virtual void init() override;

    /// get the file path of the CamiTK extension file that generated this extension
    virtual QString getCamiTKExtensionFilePath() const;
};

#endif // __$upperSnakeCase(name)$_ACTIONEXTENSION__

