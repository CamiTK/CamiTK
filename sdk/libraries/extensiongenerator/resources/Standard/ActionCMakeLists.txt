#--------------------------------------------
#         CamiTK Action Extension
#--------------------------------------------
camitk_extension(
    @if@ $actions$
    ACTION_EXTENSION
    @endif@
    @if@ $components$
    COMPONENT_EXTENSION
    @endif@
    @if@ $viewers$
    VIEWER_EXTENSION
    @endif@
    TARGET_NAME "$kebabCase(name)$"
    INSTALL_ALL_HEADERS
    DESCRIPTION "$description$"
    CAMITK_FILE "$camitkFile$"
    $extensionDependencies$

    #
    # Other supported options:
    #
    # [NEEDS_ITK]
    # [NEEDS_LIBXML2]
    # [NEEDS_OPENCV]
    # [NEEDS_IGSTK]
    # [NEEDS_XSD]
    # [NEEDS_GDCM]
    # [NEEDS_ACTION_EXTENSION action1 action2 ...]
    # [NEEDS_VIEWER_EXTENSION viewer1 viewer2 ...]
    # [NEEDS_COMPONENT_EXTENSION component1 component2 ...]
    # [NEEDS_CEP_LIBRARIES CEPLib1 CEPLib2 ...]
    # [INCLUDE_DIRECTORIES dir1 dir2 ...]
    # [DEFINES flag1 flag2 ...]
    # [CXX_FLAGS flag1 flag2 ...]
    # [EXTERNAL_SOURCES file1 file2 ...]
    # [EXTERNAL_LIBRARIES lib1 lib2 ...]
    # [HEADERS_TO_INSTALL header1.h header2.h ...]
    # [INSTALL_ALL_HEADERS]
    # Activate test:
    #ENABLE_AUTO_TEST
    # Limit tested files to a list
    # By default tests use all files in subdir testdata/ for component or all installed testdata files for actions
    #TEST_FILES file1 file2 ...
    # for component extension, test can be relaxed to lower levels using the AUTO_TEST_LEVEL option
    # for action extension, you can add an integration test using the ENABLE_INTEGRATION_TEST option
)

# For component extension, additional action autotest can be added.
# If this extension provides a new component extension, it is possible to check its integration 
# with previously defined actions (in CamiTK SDK or other CEPs) by using the
# camitk_additional_action_test macro.
# Example:
# camitk_additional_action_test(ACTION_EXTENSIONS action1 action2
#                               TEST_FILES file1 file2
# )
# will add automatic test for action1 and action2 using file1 and file2 (provided by this extension in testdata subdir)

# Use the camitk_disable_tests to disable a test permanently
# camitk_disable_tests(TESTS testname1 [testname2 ...]
#                      REASON "explanation")

# Use camitk_tests_requirement to specify a requirement for a test
# camitk_tests_requirement(TESTS testname1 [testname2 ...]
#                          REQUIRES requirement
#                          REASON "explanantion")
