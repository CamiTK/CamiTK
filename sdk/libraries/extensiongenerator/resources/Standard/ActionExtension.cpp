$licenseComment$

#include "$upperCamelCase(name)$ActionExtension.h"

// include generated actions headers
@for@ $a$ in $actions$
#include "$upperCamelCase(a.name)$.h"
@endfor@

// --------------- getActions -------------------
void $upperCamelCase(name)$ActionExtension::init() {
    @for@ $a$ in $actions$
    // Creating and registering the instance of $upperCamelCase(a.name)$
    registerNewAction($upperCamelCase(a.name)$);
    @endfor@
}

// -------------------- getCamiTKExtensionFilePath --------------------
QString $upperCamelCase(name)$ActionExtension::getCamiTKExtensionFilePath() const {
    return "$camitkFileAbsolutePath$";
}
