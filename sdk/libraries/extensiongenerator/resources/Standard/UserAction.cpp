$licenseComment$
#include "$upperCamelCase(name)$.h"

//
// Generated source for action "$title(name)$"
// $date()$ - $time()$
//

// CamiTK includes
#include <Property.h>
#include <Log.h>

@if@ $userIncludesInImplementation$
// Additional includes
$userIncludesInImplementation$
@endif@

@if@ $itkFilters$
// Itk includes
#include <itkVTKImageToImageFilter.h>
#include <itkImageToVTKImageFilter.h>
#include <ItkProgressObserver.h>
@endif@

@if@ $implementationIncludes$
// includes for specific types
$implementationIncludes$
@endif@

@if@ $componentClass$
// This action process $componentClass$, include is required
#include <$componentClass$.h>
@endif@

using namespace camitk;

// -------------------- init --------------------
void $upperCamelCase(name)$::init() {
    // Add initialization if required
    //
    // Note: use the "classMembers" field of the $name$ action in
    // the .camitk JSON file to declare additional private,
    // public, or signal/slot members.
}

// -------------------- process --------------------
Action::ApplyStatus $upperCamelCase(name)$::process() {
    QString logMessage;
    logMessage = tr("Action \"$title(name)$\" called");
    @if@ $parameters$
    logMessage += tr(" with parameters:");
    @endif@
    logMessage += "\n";

    // example of value retrieval
    @for@ $p$ in $parameters$
    @if@ $p.enumValues$ 
    int param$index(p)$ = getParameterValue("$title(p.name)$").value<int>(); // enum as an int
    QStringList $lowerCamelCase(p.name)$EnumValues;    
    @subfor@ $enumVal$ in $p.enumValues$
    $lowerCamelCase(p.name)$EnumValues.append("$enumVal$"); 
    @endsubfor@
    QString param$index(p)$String = $lowerCamelCase(p.name)$EnumValues.at(param$index(p)$);
    logMessage += QString(" - $title(p.name)$: ") + param$index(p)$String + " (value: " + QString::number(param$index(p)$) + ")\n";
    @else@ 
    $p.type$ param$index(p)$ = getParameterValue("$title(p.name)$").value<$p.type$>(); 
    logMessage += QString(" - $title(p.name)$: ") + getParameterValueAsString("$title(p.name)$") + "\n";
    @endif@ 
    @endfor@
    // Create a warning log message for this demo (although CAMITK_TRACE
    // and CAMITK_INFO are usually recommended for log messages)
    CAMITK_WARNING(logMessage);

    // Process each targets
    @if@ $componentClass$
    // example of processing
    for(Component* selected: getTargets()) {
        $componentClass$* input = dynamic_cast <$componentClass$*>(selected);
        // Here do something here input
        CAMITK_INFO(QString("Processing component %1...").arg(input->getName()));
    }
    @endif@
    
    return SUCCESS;
}

// -------------------- targetDefined --------------------
void $upperCamelCase(name)$::targetDefined() {
    @if@ $componentClass$
    // example of processing
    QString logMessage = "Current targets:\n";
    for(Component* selected: getTargets()) {
        $componentClass$* input = dynamic_cast <$componentClass$*>(selected);
        logMessage += "- Component \"" + input->getName() + "\" is a target\n";
    }
    CAMITK_WARNING(logMessage);
    @endif@
}

// -------------------- parameterChanged --------------------
void $upperCamelCase(name)$::parameterChanged(QString parameterName) {
    // Here do something when the given parameter has changed
    CAMITK_WARNING(QString("Parameter %1 value changed to %2").arg(parameterName).arg(getParameterValueAsString(parameterName)))
}

@if@ equals($gui$,"Custom GUI")
// -------------------- getUI --------------------
QWidget* $upperCamelCase(name)$::getUI() {
    // This is where you need to instantiate and manage
    // your custom GUI
    //
    // Note: this method will only be called once, 
    // at the first triggering of the action.
    // You need to use signal/slot to update your widget
    // in another part of your code.
    //
    return nullptr;
}
@endif@
