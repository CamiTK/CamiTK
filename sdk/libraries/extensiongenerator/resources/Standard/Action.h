$licenseComment$

#ifndef __$upperSnakeCase(name)$_ACTION__
#define __$upperSnakeCase(name)$_ACTION__

#include <Action.h>

// Additional includes
$headerIncludes$

//
// Generated source for action "$title(name)$"
// $date()$ - $time()$
//

class $upperCamelCase(name)$ : public camitk::Action {
    Q_OBJECT
    
public: 
    /// Default Constructor 
    /// Calls user-defined init()
    $upperCamelCase(name)$(camitk::ActionExtension *);

    /// Default Destructor
    virtual ~$upperCamelCase(name)$() = default;

    /// Call user-defined targetDefined() and getUI()
    virtual QWidget* getWidget() override;

    /// manage property change immediately
    virtual bool event(QEvent* e) override;

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components, 
      * i.e., instances of "$componentClass$" (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply() override;

private:

    /// Called from the constructor for user-defined initialization
    void init();

    /// Called from apply() for user-defined processing
    camitk::Action::ApplyStatus process();

@if@ equals($gui$,"Custom GUI")
    /// Called from getWidget() to get the user-defined widget
    /// If getUI() returns nullptr, use the default action widget
    QWidget* getUI();
@endif@

    /// Called from getWidget to notify the user
    /// (the user might have to update something in the action logic)
    /// Note: call getTargets() at any time to get the list of current targets
    void targetDefined();

    /// Called from event to notify when the user has changed the parameter
    /// of the given name
    void parameterChanged(QString parameterName);

    /// do not manage event during initialization
    bool initializationPending;

///@name user-defined members
/// add the  public:  / private: / public slots: /...
/// to define the targeted member section 
///@{
$classMembers$
///@}
}; 

#endif // __$upperSnakeCase(name)$_ACTION__
