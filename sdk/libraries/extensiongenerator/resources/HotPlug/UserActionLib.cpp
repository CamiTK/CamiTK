$licenseComment$

//
// Generated source for action "$title(name)$"
// $date()$ - $time()$
//
// CamiTK includes
#include <Log.h>
#include <Action.h>

@if@ $userIncludesInImplementation$
// Additional includes
$userIncludesInImplementation$
@endif@

@if@ $itkFilters$
// Itk includes
#include <itkVTKImageToImageFilter.h>
#include <itkImageToVTKImageFilter.h>
#include <ItkProgressObserver.h>
@endif@

// You may remove this header if they are not needed
#include <QVector3D>
#include <QPoint>
#include <QColor>

@if@ $componentClass$
// This action process $componentClass$, include is required
#include <$componentClass$.h>
@endif@

// You can also move CamiTKActionDeclaration($upperCamelCase(name)$) to a separate
// header file if you prefer (do not forget to add the header guards __$upperSnakeCase(name)$_ACTION__)
CamiTKActionDeclaration($upperCamelCase(name)$) {
    // here add all the extra declaration required for the action processing (data, methods...)
    // Do not use qualified class name
    // E.g., use "void myMethod(...);" instead of "void $upperCamelCase(name)$::myMethod(..)"
}

using namespace camitk;


CamiTKActionImplementation($upperCamelCase(name)$) {

    // Do not use qualified class name
    // E.g., use "void myMethod(...) {..}" instead of "void $upperCamelCase(name)$::myMethod(..) {..}"

    // -------------------- init --------------------
    void init(Action* self) {
        // Add initialization if required 
        // You may comment or remove this method if your action does not required
        // any initialization
    }

    // -------------------- process --------------------
    Action::ApplyStatus process(Action* self) {
        QString logMessage;
        logMessage = QString("Action \"%1\" called").arg(self->getName());
        @if@ $parameters$
        logMessage += QString(" with parameters:");
        @endif@
        logMessage += "\n";

        // example of value retrieval
        @for@ $p$ in $parameters$
        @if@ $p.enumValues$
        int param$index(p)$ = self->getParameterValue("$title(p.name)$").value<int>();    // enum as an int        
        QStringList $lowerCamelCase(p.name)$EnumValues;
        @subfor@ $enumVal$ in $p.enumValues$
        $lowerCamelCase(p.name)$EnumValues.append("$enumVal$");
        @endsubfor@
        QString param$index(p)$String = $lowerCamelCase(p.name)$EnumValues.at(param$index(p)$);
        logMessage += QString(" - $title(p.name)$: ") + param$index(p)$String + " (value: " + QString::number(param$index(p)$) + ")\n";
        @else@
        $p.type$ param$index(p)$ = self->getParameterValue("$title(p.name)$").value < $p.type$ > ();
        logMessage += QString(" - $title(p.name)$: ") + self->getParameterValueAsString("$title(p.name)$") + "\n";
        @endif@
        @endfor@
        // Create a warning log message for this demo (although CAMITK_TRACE_ALT
        // and CAMITK_INFO_ALT are usually recommended for log messages)
        CAMITK_WARNING_ALT(logMessage);

        // Process each targets
        @if@ $componentClass$
        // example of processing
        for (Component* selected : self->getTargets()) {
            $componentClass$* input = dynamic_cast<$componentClass$*>(selected);
            // Here do something here input
            CAMITK_INFO_ALT(QString("Processing component %1...").arg(input->getName()));
        }
        @endif@

        return Action::SUCCESS;
    }

    @if@ $componentClass$
    // -------------------- targetDefined --------------------
    void targetDefined(Action* self) {
        // You may comment or remove this method if your action does not required
        // to do something when the target is defined

        // example:
        QString logMessage = "Current targets:\n";
        for(Component* selected: self->getTargets()) {
            $componentClass$* input = dynamic_cast <$componentClass$*>(selected);
            logMessage += "- Component \"" + input->getName() + "\" is a target\n";
        }
        CAMITK_WARNING_ALT(logMessage);
    }
    @endif@

    // -------------------- parameterChanged --------------------
    void parameterChanged(Action* self, QString parameterName) {
        // You may comment or remove this method if your action does not required
        // to do something when one of the parameter's value has changed

        // example:
        CAMITK_WARNING_ALT(QString("Parameter %1 value changed to %2").arg(parameterName).arg(self->getParameterValueAsString(parameterName)))
    }

    // -------------------- getUI --------------------
    QWidget* getUI(Action* self) {
        // You may comment or remove this method if your action
        // uses the default action widget

        // If you don't want to use the default action widget
        // but want to create your own action widget,
        // then write you UI instantiation here.
        //
        // Note: CamiTK uses a lazy instantiation pattern for
        // UI. This method will only be called once, when the
        // action is triggered.
        // Add other signal/slot methods to update your widget
        // if needed.
        //
        return nullptr;
    }

}