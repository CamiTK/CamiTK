# Build library for action $upperCamelCase(name)$
camitk_library(SHARED
    LIBNAME $joinKebabCase(name)$
    OUTPUT_SUBDIRECTORY actions
    SOURCES $upperCamelCase(name)$.cpp
    DESCRIPTION "Create extension modern library for action $name$: $description$"
    NEEDS_CAMITKCORE
    INSTALL_NO_HEADERS
    $extensionDependencies$
)
# export all symbols on Windows MSVC for action $upperCamelCase(name)$
set_property(TARGET ${LIBRARY_TARGET_NAME} PROPERTY WINDOWS_EXPORT_ALL_SYMBOLS true)

