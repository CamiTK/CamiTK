/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ExtensionGenerator.h"

#include <QCoreApplication>
#include <QJsonParseError>
#include <QJsonArray>

#include "TransformEngine.h"
#include <iostream>

// -------------------- constructor --------------------
ExtensionGenerator::ExtensionGenerator(const QString& camitkFilePath, const QString& outputDirectoryName) {
    success = true;
    this->camitkFilePath = camitkFilePath;
    success = camitkExtensionModel.load(camitkFilePath);

    success = success && createOutputDirectoryIfNeeded(outputDirectoryName + QDir::separator());
}

ExtensionGenerator::~ExtensionGenerator() {
}

// -------------------- createOutputDirectoryIfNeeded --------------------
bool ExtensionGenerator::createOutputDirectoryIfNeeded(const QString& dirPath) {
    QDir dir(dirPath);
    if (!dir.isAbsolute()) {
        QString pwd = QDir::currentPath();
        dir.setPath(pwd + QDir::separator() + dirPath);
    }

    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            std::cerr << "ExtensionGenerator warning: failed to create directory " << dir.absolutePath().toStdString();
            return false;
        }
    }

    outputDir.setPath(dir.absolutePath());
    return true;
}

// -------------------- fileToString --------------------
QString ExtensionGenerator::fileToString(const QString& filename) {
    QFile file(filename);
    QString fileContent;
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        std::cerr << "ExtensionGenerator warning: cannot open template engine file \"" << filename.toStdString() << "\"\nerror: " << file.errorString().toStdString();
    }

    QTextStream in(&file);
    fileContent = in.readAll();

    file.close();

    return fileContent;
}

// -------------------- inheritFromExtension --------------------
void ExtensionGenerator::inheritFromExtension(const QString type, VariantDataModel& extensionModel) {
    VariantDataModel& extensionArray = extensionModel[type];
    if (extensionArray.isValid()) {
        for(auto & ext : extensionArray) {
            ext["licenseComment"] = extensionModel["licenseComment"];
            ext["extensionDependencies"] = extensionModel["extensionDependencies"];
        }
    }
}

// -------------------- generateActionExtension --------------------
void ExtensionGenerator::generateActionExtension(VariantDataModel& extensionModel, QStringList& statusMessage, QStringList& warningMessage) {
    // model → JSON (needed by the transform engine)
    QJsonObject extensionJSON = QJsonObject::fromVariantMap(extensionModel.getValue().toMap());
    QJsonArray actionsArrayJSON = QJsonArray::fromVariantList(extensionModel["actions"].getValue().toList());

    if (actionsArrayJSON.size() == 0) {
        return;
    }
    
    // create the actions directory
    QFileInfo camitkFileInfo(camitkFilePath);
    QDir actionDirectory(camitkFileInfo.absolutePath());
    actionDirectory.mkdir("actions");
    actionDirectory.cd("actions");

    // There are two types of generation:
    // - Standard (C++)
    // - HotPlug (C++ and Python, now default)
    QString generationType = extensionModel["generationType"].toString(); // either Standard or HotPlug
    if (generationType.isEmpty() || generationType.isNull()) {
        // default
        generationType = "HotPlug";
    }

    if (generationType == "HotPlug") {
        //-- HotPlug generation
        // append the directory to the action CMakeLists.txt
        QFile actionLibrariesCMakeFile(actionDirectory.filePath("CMakeLists.txt"));
        if (!actionLibrariesCMakeFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            warningMessage.append(QString("Cannot open actions CMakeLists.txt for writing: %1").arg(actionLibrariesCMakeFile.errorString()));
        }
        else {
            statusMessage.append("- Generating actions CMakeLists.txt");
            QTextStream out(&actionLibrariesCMakeFile);
            out << "# Create targets for all actions\n\n";
            for (const auto& actionJSONValue : actionsArrayJSON) {
                // create required information from action name
                QJsonObject actionJSON = actionJSONValue.toObject();
                QString libName = transformEngine.transformToString("$joinKebabCase(name)$", actionJSON).toLower();
                QString className = transformEngine.transformToString("$upperCamelCase(name)$", actionJSON);

                // add the camitk_library macro to the ActionLibraries.cmake
                out << transformEngine.transformToString(fileToString(":/" + generationType + "/ActionCMakeLists.txt"), actionJSON) << "\n\n";

                // generate user-defined code from the template if it does not exist
                transformEngine.setTemplateString(fileToString(":/" + generationType + "/UserActionLib.cpp"));
                if (transformEngine.transformToFile(actionJSON, actionDirectory.filePath(className + ".cpp"), false)) {
                    statusMessage.append("- Generating initial user source code for action \"" + actionJSON["name"].toString() + "\" (" + className + ".cpp)...");
                }
                else {
                    statusMessage.append("- User source code for action \"" + actionJSON["name"].toString() + "\" already exists (" + className + ".cpp): not overwritten");
                }

            }
            // all camitk_library are now defined in ActionLibraries.cmake
            actionLibrariesCMakeFile.close();
        }
    }
    else {
        //-- Standard generation

        //-- generate action CMakeLists.txt
        transformEngine.setTemplateString(fileToString(":/" + generationType + "/ActionCMakeLists.txt"));
        if (transformEngine.transformToFile(extensionJSON, actionDirectory.filePath("CMakeLists.txt"), false)) {
            statusMessage.append("- Generating CMakeLists.txt for actions...");
        }
        else {
            statusMessage.append("- CMakeLists.txt for action already exists: not overwritten");
        }

        //-- generate user action implementation
        transformEngine.setTemplateString(fileToString(":/" + generationType + "/UserAction.cpp"));
        for (const auto& actionJSONValue : actionsArrayJSON) {
            QJsonObject actionJSON = actionJSONValue.toObject();
            QString className = transformEngine.transformToString("$upperCamelCase(name)$", actionJSON);
            
            // generate user-defined code from the template if it does not exist
            if (transformEngine.transformToFile(actionJSON, actionDirectory.filePath(className + ".cpp"), false)) {
                statusMessage.append("- Generating initial user source code for action \"" + actionJSON["name"].toString() + "\" (" + className + ".cpp)...");
            }
            else {
                statusMessage.append("- User source code for action \"" + actionJSON["name"].toString() + "\" already exists (" + className + ".cpp): not overwritten");
            }
        }

        // -- generation in build directory
        if (!isUserSourceCodeGeneration()) {
            // -- if the output directory is different that the camitkFile directory,
            //    the extension and non-user action source files have to be generated
            // - generate the ActionExtension class header and implementation files
            // - for all action generate the action implementation files
            // - for all action generate the action header files    

            // outputDir should be build/generated/actions
            statusMessage.append("- Generating generic source code for action extension \"" + extensionModel["name"].toString() + "\" source code (overwriting)...");
            transformEngine.setTemplateString(fileToString(":/" + generationType + "/ActionExtension.h"));
            transformEngine.transformToFile(extensionJSON, outputDir.filePath("$upperCamelCase(name)$ActionExtension.h"));
            transformEngine.setTemplateString(fileToString(":/" + generationType + "/ActionExtension.cpp"));
            transformEngine.transformToFile(extensionJSON, outputDir.filePath("$upperCamelCase(name)$ActionExtension.cpp"));

            transformEngine.setTemplateString(fileToString(":/" + generationType + "/Action.cpp"));
            for (const auto& actionJSON : actionsArrayJSON) {
                statusMessage.append("- Generating generic source code for action \"" + actionJSON.toObject()["name"].toString() + "\" (overwriting)...");
                transformEngine.transformToFile(actionJSON.toObject(), outputDir.filePath("$upperCamelCase(name)$.cpp"));
            }

            transformEngine.setTemplateString(fileToString(":/" + generationType + "/Action.h"));
            for (const auto& actionJSON : actionsArrayJSON) {
                transformEngine.transformToFile(actionJSON.toObject(), outputDir.filePath("$upperCamelCase(name)$.h"));
            }
        }
    }
}

// -------------------- isUserSourceCodeGeneration --------------------
bool ExtensionGenerator::isUserSourceCodeGeneration() {
    QFileInfo camitkFileInfo(camitkFilePath);
    QDir camitkFileDirectory(camitkFileInfo.absolutePath());
    return (camitkFileDirectory == outputDir);
}

// -------------------- process --------------------
bool ExtensionGenerator::generate() {
    if (!success) {
        // don't start as something wrong happened before
        std::cerr << "ExtensionGenerator: setup incomplete, cannot start template transformation.";
        return false;
    }

    // accumulate messages in these list, output only at the end
    QStringList statusMessage;
    QStringList warningMessage;

    //-- get the path to the .camitk file
    QFileInfo camitkFileInfo(camitkFilePath);
    QDir camitkFileDirectory(camitkFileInfo.absolutePath());
    
    //-- check if this extension is already part of a CEP, i.e., the root directory contains a CMakeLists.txt
    QDir camitkFileDirectoryRoot = camitkFileDirectory;    
    camitkFileDirectoryRoot.cdUp();
    bool isInCEP = QFile(camitkFileDirectoryRoot.filePath("CMakeLists.txt")).exists();

    //-- get the model containing all the extensions
    VariantDataModel& extensionModel = camitkExtensionModel.getModel();
    // create JSON object from the model
    QJsonObject extensionJSON = QJsonObject::fromVariantMap(extensionModel.getValue().toMap());

    //-- add camitk file information
    extensionModel["camitkFile"] = camitkFileInfo.fileName();
    extensionModel["camitkFileAbsolutePath"] = camitkFileInfo.absoluteFilePath();

    //-- add camitk installation information
    // precondition: this is ran by either camitk-imp or camitk-extensiongenerator but both
    // are installed in the same directory → this determines CamiTK DIR bin directory
    // current executable bin dir (this should be a camitk application from inside the installed/build directory)
    // This will set EXTENSION_GENERATOR_CAMITK_DIR in the extension top-level CMakeLists.txt
    // which will be used to find camitk-config
    QDir camitkBinDir;
    camitkBinDir.setPath(QCoreApplication::applicationDirPath());
    QDir camiTKDir = camitkBinDir;
    camiTKDir.cdUp();
    extensionModel["camitkDir"] = camiTKDir.absolutePath();

    //-- add cmake module path relatively to current executable path
    // This will set EXTENSION_GENERATOR_CMAKE_MODULE_PATH in the extension top-level CMakeLists.txt
    // which should work in most cases (i.e., if CamitK is either installed locally or globally or
    // even if this is used during the SDK build, during local build or CI build) and will locate
    // the CamiTK CMake modules and macros required to build the extension.
    QDir camiTKCMakeModuleDir = camiTKDir;
    camiTKCMakeModuleDir.cd("share");
    camiTKCMakeModuleDir.cd(CAMITK_SHORT_VERSION); // CAMITK_SHORT_VERSION is defined in CMakeLists.txt
    camiTKCMakeModuleDir.cd("cmake");
    extensionModel["camitkCMakeModulePath"] = QString("%1;%2").arg(camiTKCMakeModuleDir.absolutePath()).arg(camiTKCMakeModuleDir.filePath("macros"));

    //-- add license text information
    QString licenseString = extensionModel["license"].toString();
    if (licenseString == "BSD") {
        extensionModel["licenseComment"] = transformEngine.transformToString(fileToString(":/licenses/BSD"), extensionJSON);
    }
    else if (licenseString == "GPL" || licenseString == "GPL-3") {
        extensionModel["licenseComment"] = transformEngine.transformToString(fileToString(":/licenses/GPL-3"), extensionJSON);
    }
    else if (licenseString == "LGPL" || licenseString == "LGPL-3") {
        extensionModel["licenseComment"] = transformEngine.transformToString(fileToString(":/licenses/LGPL-3"), extensionJSON);
    }
    else if (licenseString == "LGPL CamiTK" || licenseString == "LGPL-3 CamiTK") {
        // replace license markers with themselves
        extensionModel["CAMITK_LICENCE_BEGIN"] = "$CAMITK_LICENCE_BEGIN$";
        extensionModel["CAMITK_LICENCE_END"] = "$CAMITK_LICENCE_END$";
        extensionModel["licenseComment"] = transformEngine.transformToString(fileToString(":/licenses/LGPL-3 CamiTK"), extensionJSON);
    }
    else {
        // add custom license into a multiline comment if it is not a comment itself.
        QRegularExpression multiLineCommentRegEx(R"(^\/\*\s*((?:.*?\r?\n?)*)\*\/)");
        if (multiLineCommentRegEx.match(licenseString).hasMatch()) {
            extensionModel["licenseComment"] = licenseString;
        }
        else {
            extensionModel["licenseComment"] = "/*\n" + licenseString + "\n*/\n";
        }
    }

    //-- update information license and dependencies for all action/component/viewer/library  
    inheritFromExtension("actions", extensionModel);
    inheritFromExtension("components", extensionModel);
    inheritFromExtension("viewers", extensionModel);
    inheritFromExtension("libraries", extensionModel);

    // update JSON object from updated model
    extensionJSON = QJsonObject::fromVariantMap(extensionModel.getValue().toMap());

    //-- generation in source directory (systematic)
    // - generate CMakeLists.txt if it does not exist
    // - copy FindCamiTK.cmake, .gitignore and .vscode/launch.json if they don't already exist
    // - generate user defined action source code for each action if they do not exist
    statusMessage.append("Generating extension source code...");
    statusMessage.append("- source CamiTK file: " + camitkFileInfo.absoluteFilePath());
    statusMessage.append("- output directory: " + outputDir.absolutePath());

    //-- if standalone CEP, add FindCamiTK.txt, .gitignore, and .vscode
    if (!isInCEP) {
        statusMessage.append("- Checking CamiTK Extension Project...");
        if (!QFile(camitkFileDirectory.filePath("FindCamiTK.cmake")).exists()) {
            QFile::copy(":/FindCamiTK.cmake", camitkFileDirectory.filePath("FindCamiTK.cmake"));
            statusMessage.append("- Copying FindCamiTK.cmake...");
        }
        else {
            statusMessage.append("- FindCamiTK.cmake already exists: not overwritten");
        }

        //-- add other useful files (gitignore and vscode subdir)
        if (!QFile(camitkFileDirectory.filePath(".gitignore")).exists()) {
            QFile::copy(":/gitignore", camitkFileDirectory.filePath(".gitignore"));
            statusMessage.append("- Copying .gitignore...");
        }
        else {
            statusMessage.append("- .gitignore already exists: not overwritten");
        }
        QDir vsCodeDir = camitkFileDirectory;
        vsCodeDir.mkdir(".vscode");
        vsCodeDir.cd(".vscode");
        transformEngine.setTemplateString(fileToString(":/vscode-launch.json"));
        if (transformEngine.transformToFile(extensionJSON, vsCodeDir.filePath("launch.json"), false)) {
            statusMessage.append("- Generating .vscode/launch.json...");
        }
        else {
            statusMessage.append("- .vscode/launch.json already exists: not overwritten");
        }
    }
    else {
        statusMessage.append("- Checking extension inside another CEP...");
    }
    
    //-- Top level CMakeLists
    transformEngine.setTemplateString(fileToString(":/CMakeLists.txt"));
    if (transformEngine.transformToFile(extensionJSON, camitkFileDirectory.filePath("CMakeLists.txt"), false)) {
        statusMessage.append("- Generating top-level CMakeLists.txt...");
    }
    else {
        statusMessage.append("- CMakeLists.txt already exists: not overwritten");
    }

    generateActionExtension(extensionModel, statusMessage, warningMessage);

    std::cout << statusMessage.join("\n").toStdString() << std::endl;
    std::cout << warningMessage.join("\n").toStdString() << std::endl;


    return true;
}
