/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef __EXTENSION_GENERATOR__
#define __EXTENSION_GENERATOR__

#include <QString>
#include <QJsonArray>
#include <QDir>
#include "CamiTKExtensionModel.h"
#include "TransformEngine.h"

/**
 * Extension generator
 * 
 * Generate extension source code from a .camitk (JSON) that contains a "camitk"/"extensions" objects.
 * 
 * Currently supported:
 * - action extensions ("actions" JSON object inside "extensions"): generates the source code for each "action" inside the array "actions"
 * 
 * \note 
 * In the template the extension $license$ is used for each generated source file to generate a license 
 * comment at the top. $license$ can either be:
 * - a common license string (same as the one used in the debian /usr/share/common-licenses directory). 
 *   Supported license strings: BSD, GPL (same as GPL-3) and LGPL (same as LGPL-3).
 * - a specific license text written in the "license" JSON string
 *
 */
class ExtensionGenerator {

public:
    ExtensionGenerator(const QString& camitkFilePath, const QString& outputDirectoryName);
    virtual ~ExtensionGenerator();

    /// call all methods to generate the source code
    /// Also generate CMake files and user action classes if they don't exist 
    /// and the output directory is the directory of camitkFilePath (i.e. if it is the first
    /// generation)
    bool generate();

private:
    /// Where to store the generated files
    QDir outputDir;

    /// The camitk file model
    CamiTKExtensionModel camitkExtensionModel;

    /// @brief the path from where the camitk file model should be loaded
    QString camitkFilePath;

    /// Current transform engine
    TransformEngine transformEngine;

    /// current status
    bool success;

    /// @name utility
    /// @{
    /// Returns the text content of filename
    QString fileToString(const QString& filename);

    /// create the output directory
    bool createOutputDirectoryIfNeeded(const QString& dirPath);

    /// update the given type (action/component/viewer/library) using the top-level extensions model information
    /// This will add "licenseComment" and "extensionDependencies" fields to each action/component/viewer/library
    void inheritFromExtension(const QString type, VariantDataModel& extensionModel);

    /// generate action extension code
    void generateActionExtension(VariantDataModel& extensionModel, QStringList& statusMessage, QStringList& warningMessage);

    /// Check if the extension generator has to generate user source code or (hidden) source code in build
    /// @return true if and only if the current output directory is the same as the directory of the camitk file
    bool isUserSourceCodeGeneration();
    ///@}

};

#endif // __EXTENSION_GENERATOR__