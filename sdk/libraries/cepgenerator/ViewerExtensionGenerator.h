/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWEREXTENSIONGENERATOR_H
#define VIEWEREXTENSIONGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only

// local includes
#include "ExtensionGenerator.h"

// Qt
#include <QStringList>

namespace cepcoreschema {
class ViewerExtension;
}

class ViewerGenerator;

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate an viewer extension.
 *
 **/
class ViewerExtensionGenerator : public ExtensionGenerator {

public:
    ViewerExtensionGenerator(QString xmlFileName, QString viewerExtensionsDirectory, QString licence = "");
    ViewerExtensionGenerator(cepcoreschema::ViewerExtension& domViewerExtension, QString viewerExtensionsDirectory, QString licence = "");

    ~ViewerExtensionGenerator();


protected:
    /// @{ Helpers methods
    void generateExtensionClass(QString directory) override;
    void writeCFile(QString directory) override;
    void writeHFile(QString directory) override;

    void generateTestDataFiles(QString directory, QString testDataDirName) override;

    ///@}

private:
    /// Helper method for constructors
    void createFromDom(cepcoreschema::ViewerExtension& dom);

    bool registerDefaultViewer;

    QStringList namedViewers;

    ViewerGenerator* viewerGenerator;
};

#endif // VIEWEREXTENSIONGENERATOR_H
