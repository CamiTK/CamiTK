/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ViewerExtensionGenerator.h"

// includes from cepcoreschema
#include <ViewerExtension.hxx>
#include <Viewer.hxx>
#include <Dependency.hxx>

// includes from STL
#include <iostream>
#include <memory>

// includes from Qt
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QTextStream>

// local includes
#include "ClassNameHandler.h"
#include "ViewerGenerator.h"
#include "DependencyGenerator.h"


using namespace cepcoreschema;

ViewerExtensionGenerator::ViewerExtensionGenerator(QString xmlFileName, QString viewerExtensionDirectory, QString licence)
    : ExtensionGenerator(viewerExtensionDirectory, licence, "Viewer") {
    QFileInfo xmlFile(xmlFileName);

    if ((! xmlFile.exists()) || (! xmlFile.isFile())) {
        throw std::invalid_argument("I/O exception during viewer extension file generation:\nFile " + xmlFileName.toStdString() + " does not exist or is not a file...\n");
    }

    try {
        std::string xmlFileStr = xmlFileName.toStdString();
        std::unique_ptr<ViewerExtension>domViewerExtension = viewerExtension(xmlFileStr, xml_schema::flags::dont_validate);
        createFromDom(*domViewerExtension);
    }
    catch (...) {
        throw std::invalid_argument("I/O exception during viewer extension file generation:\nFile " + xmlFileName.toStdString() + " is not valid...\n");
    }
}

ViewerExtensionGenerator::ViewerExtensionGenerator(ViewerExtension& domViewerExtension, QString viewerExtensionDirectory, QString licence)
    : ExtensionGenerator(viewerExtensionDirectory, licence, "Viewer") {
    createFromDom(domViewerExtension);
}

void ViewerExtensionGenerator::createFromDom(ViewerExtension& dom) {
    name = QString(dom.name().c_str());
    description = QString(dom.description().c_str()).simplified();
    registerDefaultViewer = dom.registerDefaultViewer().present();

    // named viewers
    for (auto it = dom.registerNewViewer().begin(); it != dom.registerNewViewer().end(); it++) {
        namedViewers << QString((*it).c_str());
    }

    if (dom.dependencies().present()) {
        Dependencies deps = dom.dependencies().get();

        for (Dependencies::dependency_iterator it = deps.dependency().begin(); it != deps.dependency().end(); it++) {
            Dependency& dep = (*it);
            DependencyGenerator* depGen = new DependencyGenerator(dep);
            dependencyGenerators.append(depGen);
        }
    }

    // Create the viewer class...
    viewerGenerator = new ViewerGenerator(dom.viewer(), licence);

}

ViewerExtensionGenerator::~ViewerExtensionGenerator() {
//    for (QVector<DependencyGenerator *>::iterator dep = dependencyGenerators.begin(); dep != dependencyGenerators.end(); dep++)
//        delete dep;

}


void ViewerExtensionGenerator::generateExtensionClass(QString directory) {
    // C generate viewers
    viewerGenerator->generateFiles(directory);
}

void ViewerExtensionGenerator::writeHFile(QString directory) {
    QString className = ClassNameHandler::getClassName(name) + "Extension";
    QFile initHFile(":/resources/ViewerExtension.h.in");
    initHFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream inh(&initHFile);

    QFileInfo extFileHPath;
    extFileHPath.setFile(directory, className + ".h");
    QFile extFileH(extFileHPath.absoluteFilePath());

    if (! extFileH.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QString msg = "Exception from extension generation: \n    Cannot write on file " + extFileHPath.fileName() + "\n";
        throw (msg);
    }

    QTextStream outh(&extFileH);

    QString headdef = className.toUpper();
    QString text;

    do {
        text = inh.readLine();
        text.replace(QRegExp("@LICENCE@"), licence);
        text.replace(QRegExp("@HEADDEF@"), headdef);
        text.replace(QRegExp("@EXTENSIONCLASSNAME@"), className);
        text.replace(QRegExp("@EXTENSIONNAME@"), className);
        text.replace(QRegExp("@EXTENSIONDESCRIPTION@"), description);
        outh << text << Qt::endl;
    }
    while (! text.isNull());

    extFileH.close();
    initHFile.close();
}

void ViewerExtensionGenerator::writeCFile(QString directory) {
    QString className = ClassNameHandler::getClassName(name) + "Extension";
    std::cout << "Generating ViewerExtension \"" << className.toStdString() << "\"..." << std::endl;

    QFile initCFile(":/resources/ViewerExtension.cpp.in");
    initCFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream inc(&initCFile);

    QFileInfo extFileCPath;
    extFileCPath.setFile(directory, className + ".cpp");
    QFile extFileC(extFileCPath.absoluteFilePath());

    if (! extFileC.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QString msg = "Exception from extension generation: \n    Cannot write on file " + extFileCPath.fileName() + "\n";
        throw (msg);
    }

    QTextStream outc(&extFileC);

    QString text;

    do {
        text = inc.readLine();
        text.replace(QRegExp("@LICENCE@"), licence);
        text.replace(QRegExp("@EXTENSIONCLASSNAME@"), className);
        text.replace(QRegExp("@VIEWERCLASSNAME@"), viewerGenerator->getClassName());

        if (text.contains(QRegExp("@IF_DEFAULTVIEWER@"))) {
            text = inc.readLine();

            while (! text.contains(QRegExp("@ENDIF_DEFAULTVIEWER@"))) {
                text.replace(QRegExp("@VIEWERCLASSNAME@"), viewerGenerator->getClassName());

                if (registerDefaultViewer) {
                    outc << text << Qt::endl;
                }

                text = inc.readLine();
            }
        }

        else {
            if (text.contains(QRegExp("@IF_NAMEDVIEWER@"))) {
                text = inc.readLine();

                if (namedViewers.size() > 0) {
                    while (!text.contains(QRegExp("@BEGIN_REGISTERVIEWERS@"))) {
                        text.replace(QRegExp("@VIEWERNAME@"), viewerGenerator->getClassName());
                        outc << text << Qt::endl;
                        text = inc.readLine();
                    }

                    text = inc.readLine();
                    QStringList registerNewViewer;

                    for (auto& viewerName : qAsConst(namedViewers)) {
                        QString textTmp = text;
                        textTmp.replace(QRegExp("@VIEWERNAME@"), viewerName);
                        textTmp.replace(QRegExp("@VIEWERCLASSNAME@"), viewerGenerator->getClassName());
                        registerNewViewer << textTmp;
                    }

                    for (auto& registerViewer : qAsConst(registerNewViewer)) {
                        outc << registerViewer << Qt::endl;
                    }

                    text = inc.readLine();

                    while (!text.contains(QRegExp("@END_REGISTERVIEWERS@"))) {
                        text = inc.readLine();
                    }

                    text = inc.readLine();
                }

                while (!text.contains(QRegExp("@ENDIF_NAMEDVIEWER@"))) {
                    text = inc.readLine();
                }
            }
            else {
                outc << text << Qt::endl;
            }
        }

    }
    while (! text.isNull());

    extFileC.close();
    initCFile.close();
}

void ViewerExtensionGenerator::generateTestDataFiles(QString directory, QString testDataDirName) {
    // Do not do anything yet
    // TODO think of the kind of test file we could put here by default...

}
