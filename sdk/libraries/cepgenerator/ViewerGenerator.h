/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERGENERATOR_H
#define VIEWERGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// includes from Qt
#include <QString>
#include <QVector>
#include <QStringList>

namespace cepcoreschema {
class Viewer;
}

class ParameterGenerator;

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate a viewer
 *
 **/
class ViewerGenerator {

public:

    ViewerGenerator(QString xmlFileName, QString licence);
    ViewerGenerator(cepcoreschema::Viewer& domViewer, QString licence);

    ~ViewerGenerator() = default;

    void generateFiles(QString directoryName);
    QString getClassName() const;
private:
    /// Helper methods for constructors
    void createFromDom(cepcoreschema::Viewer& dom);

    void writeHFile(QString directoryName);
    void writeCFile(QString directoryName);
    void writeSpecialItkFile(QString directoryName);

    /// Name of the viewer
    QString name;

    QString className;

    QString description;

    QString licence;

    QString type;

    QStringList components;
};

#endif // VIEWERGENERATOR_H
