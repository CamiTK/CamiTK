<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>SphereTopology</name>
    <message>
        <location filename="../../SphereTopology.cpp" line="56"/>
        <source>Center</source>
        <translation>Centre</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="56"/>
        <source>This coordinate represents the center of the sphere. &lt;br /&gt;By default this value is (0,0,0).</source>
        <translation>Ces coordonnées représentent le centre de la sphere. &lt;br /&gt;Par défaut cette valeur est (0,0,0).</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="59"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="59"/>
        <source> This is the radius of the sphere. &lt;br /&gt;The default value is 0.5.</source>
        <translation>C&apos;est le rayon de la sphère. &lt;br /&gt;La valeur par défaut est 0.5.</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="64"/>
        <source>Phi resolution</source>
        <translation>Résolution de phi</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="64"/>
        <source>This number represents the number of division between start phi and end phi on the sphere. &lt;br /&gt;The phi divisions are similar to latitude lines on the earth. &lt;br /&gt;&lt;br /&gt;The default phi resolution is 8</source>
        <translation>Ce nombre représente le nombre de division entre le début de phi et la fin  de phi sur la sphére. &lt;br /&gt;Les divisions de phi sont similaires aux lignes de latitude sur la terre &lt;br /&gt; &lt;br /&gt;La résolution de phi par défaut est 8</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="69"/>
        <source>Theta resolution</source>
        <translation>Résolution de Theta</translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="69"/>
        <source>This number represents the number of division between start theta and end theta around the sphere. &lt;br /&gt;The theta divisions are similar to longitude lines on the earth. &lt;br /&gt;The higher the resolution the closer the approximation will come to a sphere and the more polygons there will be. &lt;br /&gt;&lt;br /&gt;The default theta resolution is 8.</source>
        <translation>Ce nombre représente le nombre de division entre le début de theta et la fin de theta autour de la sphére. &lt;br /&gt;Les divisions de theta sont similaires aux lignes de longitude sur la terre &lt;br /&gt;Plus grande est la resolution plus l&apos;approximation s&apos;approchera d&apos;une sphere constituée d&apos;un grand nombre de polygones.&lt;br /&gt; &lt;br /&gt;La résolution de phi par défaut est 8.</translation>
    </message>
</context>
</TS>
