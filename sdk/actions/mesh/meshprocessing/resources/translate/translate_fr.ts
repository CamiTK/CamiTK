<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AppendMeshes</name>
    <message>
        <location filename="../../AppendMeshes.cpp" line="44"/>
        <source>Append several meshes in one mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AppendMeshes.cpp" line="48"/>
        <source>Merge Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AppendMeshes.cpp" line="48"/>
        <source>Define if close points are merged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CleanPolyData</name>
    <message>
        <location filename="../../CleanPolyData.cpp" line="43"/>
        <source>Merge duplicate points, and/or remove unused points and/or remove degenerate cells.</source>
        <translation>Fusionner les points dupliqués, et/ou supprimer les points inutilisés et/ou supprimer les cellules dégénérées.</translation>
    </message>
    <message>
        <location filename="../../CleanPolyData.cpp" line="48"/>
        <source>vtkCleanPolyData</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CleanPolyData.cpp" line="49"/>
        <source>Clean vtkPolyData</source>
        <translation>Nettoyer vtkPolyData</translation>
    </message>
</context>
<context>
    <name>ComputeCurvatures</name>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="17"/>
        <source>Computes curvatures of a surface.&lt;br/&gt;</source>
        <translation>Calcule les courbures d&apos;une surface.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="20"/>
        <source>curvatures</source>
        <translation>courbures</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="22"/>
        <source>Curvature type</source>
        <translation>type de courbure</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="22"/>
        <source>The type of curvature to compute on the surfacic mesh.</source>
        <translation>le type de courbure à calculer sur le maillage de surface.</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Gaussian</source>
        <translation>Gaussienne</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Mean</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="37"/>
        <source>Compute Curvatures...</source>
        <translation>Calculer les Courbures...</translation>
    </message>
</context>
<context>
    <name>Decimation</name>
    <message>
        <location filename="../../Decimation.cpp" line="43"/>
        <source>This action reduce the number of triangles in a triangle mesh, forming a good approximation to the original geometry. &lt;br/&gt;&lt;br /&gt; The algorithm proceeds as follows. Each vertex in the mesh is classified and inserted into a priority queue. The priority is based on the error to delete the vertex and retriangulate the hole. Vertices that cannot be deleted or triangulated (at this point in the algorithm) are skipped. Then, each vertex in the priority queue is processed (i.e., deleted followed by hole triangulation using edge collapse). This continues until the priority queue is empty. Next, all remaining vertices are processed, and the mesh is split into separate pieces along sharp edges or at non-manifold attachment points and reinserted into the priority queue. Again, the priority queue is processed until empty. If the desired reduction is still not achieved, the remaining vertices are split as necessary (in a recursive fashion) so that it is possible to eliminate every triangle as necessary. &lt;br/&gt;&lt;br /&gt;To use this object, at a minimum you need to specify the parameter &lt;b&gt;Decimation percentage&lt;/b&gt;. The algorithm is guaranteed to generate a reduced mesh at this level as long as the following four conditions are met:&lt;ul&gt;                    &lt;li&gt;topology modification is allowed (i.e., the parameter &lt;b&gt;Preserve topology&lt;/b&gt; is false); &lt;/li&gt;                   &lt;li&gt;mesh splitting is enabled (i.e., the parameter &lt;b&gt;Splitting&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the algorithm is allowed to modify the boundaries of the mesh (i.e., the parameter &lt;b&gt;Boundary vertex deletion?&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the maximum allowable error (i.e., the parameter &lt;b&gt;Maximum error&lt;/b&gt;) is set to &lt;i&gt;1.0e+38f&lt;/i&gt; (default value). &lt;/ul&gt;             Other important parameters to adjust are the &lt;b&gt;Feature angle&lt;/b&gt; and &lt;b&gt;Split angle&lt;/b&gt; parameters, since these can impact the quality of the final mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="52"/>
        <source>Decimation</source>
        <translation>Décimation</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="53"/>
        <source>Simplify</source>
        <translation>Simplifier</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="59"/>
        <source>Decimation percentage</source>
        <translation>Pourcentage de décimation</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="59"/>
        <source>Specify the desired reduction in the total number of polygons. &lt;br/&gt;Because of various constraints, this level of reduction may not be realized. If you want to guarantee a particular reduction, you must turn off &lt;b&gt;Preserve topology?&lt;/b&gt; and &lt;b&gt;Boundary vertex deletion?&lt;/b&gt;, turn on &lt;b&gt;Split mesh?&lt;/b&gt;, and set the &lt;b&gt;Maximum error&lt;/b&gt; to its maximum value (these parameters are initialized this way by default). </source>
        <translation>Spécifier la réduction souhaitée dans le nombre total de polygones. &lt;br/&gt;A cause de plusieurs contraintes, ce niveau de réduction ne peut pas être réalisé. Si vous voulez garantir une réduction spécifique, vous devez arrêter  &lt;br/&gt;Préserver la topologie? &lt;/b&gt; et  &lt;br/&gt;Suppression des limites du vertex? &lt;/b&gt;, démarrer  &lt;br/&gt;Couper le maillage? &lt;/b&gt;, et initialiser L&apos; &lt;br/&gt;erreur maximum &lt;/b&gt; à sa valeur maximum (ces paramêtres sont initialisés de cette manière par défaut).</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="65"/>
        <source>Preserve topology?</source>
        <translation>Preserver la topologie?</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="65"/>
        <source>Turn on/off whether to preserve the topology of the original mesh. &lt;br/&gt;If on, mesh splitting and hole elimination will not occur. This may limit the maximum reduction that may be achieved. </source>
        <translation>Mettre ON/OFF pour preserver la topologie du maillage initial. &lt;br/&gt;si ON, la decoupe de maillage et l&apos;élimination de trou ne se produira pas. il se peut que cela limite la réduction maximale qui peut être achevée.  </translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="68"/>
        <source>Maximum error</source>
        <translation>Erreur maximum</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="68"/>
        <source>Set the largest decimation error that is allowed during the decimation process.&lt;br/&gt; This may limit the maximum reduction that may be achieved. The maximum error is specified as a fraction of the maximum length of the input data bounding box.</source>
        <translation>Initialise la plus grande erreur de décimation qui est autorisée lors du processus de décimation. &lt;br/&gt; Il se peut que cela limite la réduction maximum qui peut être atteinte. L&apos;erreur maximum est définie comme une fraction de la longueur maximum de la boite rattaché au donnée en entrée. </translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="74"/>
        <source>Feature angle</source>
        <translation>caractéristique de l&apos;angle</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="74"/>
        <source>Specify the mesh feature angle. &lt;br/&gt;This angle is used to define what an edge is (i.e., if the surface normal between two adjacent triangles is &gt;= FeatureAngle, an edge exists). </source>
        <translation>Spécifie l&apos;angle caractéristique du maillage. &lt;br/&gt;Cette angle est utilisé pour définir ce qu&apos;est un côté (i.e., si la normale à la surface entre deux triangles adjacents est &gt;= Caractéristique de l&apos;angle, un côté existe).  </translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="80"/>
        <source>Split edge?</source>
        <translation>couper le front?</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="80"/>
        <source>Turn on/off the splitting of the mesh at corners, along edges, at non-manifold points, or anywhere else a split is required. &lt;br/&gt;Turning splitting off will better preserve the original topology of the mesh, but you may not obtain the requested reduction. </source>
        <translation>Mettre ON/OFF la découpe du maillage aux coins, le long des coins, at non_manifold points, ou n&apos;importe où une découpe est désirée. &lt;br/&gt; Mettre la découpe OFF preservera mieux la topologie du maillage, mais il se peut que vous n&apos;obteniez pas la réduction recherchée.  </translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="83"/>
        <source>Split angle</source>
        <translation>Couper l&apos;angle</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="83"/>
        <source>Specify the mesh split angle. This angle is used to control the splitting of the mesh. &lt;br/&gt;A split line exists when the surface normals between two edge connected triangles are &gt;= SplitAngle. </source>
        <translation>Spécifie l&apos;angle de découpe du maillage. Cet angle est utilisé pour contrôler le découpage du maillage. &lt;br/&gt;Une ligne de découpe existe quand la normale à la surface entre deux cotés de triangles connectés sont &gt;= Couper angle.</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="89"/>
        <source>Boundary vertex deletion?</source>
        <translation>Destruction de la limite d&apos;un sommet?</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="89"/>
        <source>Turn on/off the deletion of vertices on the boundary of a mesh. &lt;br/&gt;This may limit the maximum reduction that may be achieved.</source>
        <translation>Mettre ON/OFF la destruction des sommets sur la limite d&apos;un maillage.. &lt;br/&gt; Il se peut cela limite la réduction maximum qui peut être atteinte.  </translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="92"/>
        <source>Max triangles for 1 vertex</source>
        <translation>Triangles Max pour un sommet</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="92"/>
        <source>If the number of triangles connected to a vertex exceeds &lt;b&gt;Max triangles for 1 vertex&lt;/b&gt;, then the vertex will be split. &lt;br/&gt;&lt;br/&gt;NOTE: the complexity of the triangulation algorithm is proportional to Degree^2. &lt;br/&gt;Setting degree small can improve the performance of the algorithm.</source>
        <translation>Si le nombre de triangles  connectés à une sommet dépasse . &lt;b&gt;Triangles Max pour 1 sommet . &lt;/b&gt;, alors le sommet sera découpée.. &lt;br/&gt;. &lt;br/&gt;NOTE: la complexité de l&apos;algorithme de triangulation est proportionnelle à Degré^2.. &lt;br/&gt;Paramêtrer un petit degré peut améliorer la performance de l&apos;algorithme.</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="97"/>
        <source>Inflection ratio</source>
        <translation>Ratio d&apos;inflexion</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="97"/>
        <source>Specify the inflection point ratio. &lt;br/&gt;An inflection point occurs when the ratio of reduction error between two iterations is greater than or equal to the InflectionPointRatio. </source>
        <translation>Spécifier le ration du point d&apos;inflexion.. &lt;br/&gt;Un point d&apos;inflexion apparaît quand le ratio de l&apos;erreur de réduction entre deux itérations est plus grand ou égal au Ration d&apos;inflexion.</translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="111"/>
        <source>Performing Decimation</source>
        <translation>Effectuer la Décimation</translation>
    </message>
    <message>
        <source>This action reduce the number of triangles in a triangle mesh, forming a good approximation to the original geometry. &lt;br/&gt;&lt;br /&gt; The algorithm proceeds as follows. Each vertex in the mesh is classified and inserted into a priority queue. The priority is based on the error to delete the vertex and retriangulate the hole. Vertices that cannot be deleted or triangulated (at this point in the algorithm) are skipped. Then, each vertex in the priority queue is processed (i.e., deleted followed by hole triangulation using edge collapse). This continues until the priority queue is empty. Next, all remaining vertices are processed, and the mesh is split into separate pieces along sharp edges or at non-manifold attachment points and reinserted into the priority queue. Again, the priority queue is processed until empty. If the desired reduction is still not achieved, the remaining vertices are split as necessary (in a recursive fashion) so that it is possible to eliminate every triangle as necessary. &lt;br/&gt;&lt;br /&gt;To use this object, at a minimum you need to specify the parameter &lt;b&gt;Decimation percentage&lt;/b&gt;. The algorithm is guaranteed to generate a reduced mesh at this level as long as the following four conditions are met:&lt;ul&gt;                    &lt;li&gt;topology modification is allowed (i.e., the parameter &lt;b&gt;Preserve topology&lt;/b&gt; is false); &lt;/li&gt;                   &lt;li&gt;mesh splitting is enabled (i.e., the parameter &lt;b&gt;Splitting&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the algorithm is allowed to modify the boundaries of the mesh (i.e., the parameter &lt;b&gt;Boundary vertex deletion?&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the maximum allowable error (i.e., the paramter &lt;b&gt;Maximum error&lt;/b&gt;) is set to &lt;i&gt;1.0e+38f&lt;/i&gt; (default value). &lt;/ul&gt;             Other important parameters to adjust are the &lt;b&gt;Feature angle&lt;/b&gt; and &lt;b&gt;Split angle&lt;/b&gt; parameters, since these can impact the quality of the final mesh.</source>
        <translation type="obsolete">(à traduire en Français) This action reduce the number of triangles in a triangle mesh, forming a good approximation to the original geometry. &lt;br/&gt;&lt;br /&gt; The algorithm proceeds as follows. Each vertex in the mesh is classified and inserted into a priority queue. The priority is based on the error to delete the vertex and retriangulate the hole. Vertices that cannot be deleted or triangulated (at this point in the algorithm) are skipped. Then, each vertex in the priority queue is processed (i.e., deleted followed by hole triangulation using edge collapse). This continues until the priority queue is empty. Next, all remaining vertices are processed, and the mesh is split into separate pieces along sharp edges or at non-manifold attachment points and reinserted into the priority queue. Again, the priority queue is processed until empty. If the desired reduction is still not achieved, the remaining vertices are split as necessary (in a recursive fashion) so that it is possible to eliminate every triangle as necessary. &lt;br/&gt;&lt;br /&gt;To use this object, at a minimum you need to specify the parameter &lt;b&gt;Decimation percentage&lt;/b&gt;. The algorithm is guaranteed to generate a reduced mesh at this level as long as the following four conditions are met:&lt;ul&gt;                    &lt;li&gt;topology modification is allowed (i.e., the parameter &lt;b&gt;Preserve topology&lt;/b&gt; is false); &lt;/li&gt;                   &lt;li&gt;mesh splitting is enabled (i.e., the parameter &lt;b&gt;Splitting&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the algorithm is allowed to modify the boundaries of the mesh (i.e., the parameter &lt;b&gt;Boundary vertex deletion?&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the maximum allowable error (i.e., the paramter &lt;b&gt;Maximum error&lt;/b&gt;) is set to &lt;i&gt;1.0e+38f&lt;/i&gt; (default value). &lt;/ul&gt;             Other important parameters to adjust are the &lt;b&gt;Feature angle&lt;/b&gt; and &lt;b&gt;Split angle&lt;/b&gt; parameters, since these can impact the quality of the final mesh.</translation>
    </message>
    <message>
        <source>This is an ENGLISH text</source>
        <translation type="obsolete">Ceci est un texte en Français</translation>
    </message>
</context>
<context>
    <name>ExportAsMDL</name>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="48"/>
        <source>Export As MDL</source>
        <translation>Exporter comme MDL</translation>
    </message>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="67"/>
        <source>Save As MDL...</source>
        <translation>Sauver comme MDL...</translation>
    </message>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="67"/>
        <source>MDL format(*.mdl)</source>
        <translation>format MDL (*.mdl)</translation>
    </message>
</context>
<context>
    <name>ExtractEdges</name>
    <message>
        <location filename="../../ExtractEdges.cpp" line="42"/>
        <source>Extract edges from a mesh</source>
        <translation>Extraire les bordures d&apos;un maillage</translation>
    </message>
    <message>
        <location filename="../../ExtractEdges.cpp" line="45"/>
        <source>ExtractEdges</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtractEdges.cpp" line="54"/>
        <source>Extracting edges...</source>
        <translation>Extraction des bordures...</translation>
    </message>
</context>
<context>
    <name>ExtractSurface</name>
    <message>
        <location filename="../../ExtractSurface.cpp" line="41"/>
        <source>Extract Surface from Volumetric Mesh</source>
        <translation>Extraire la Surface d&apos;un Maillage Volumique</translation>
    </message>
    <message>
        <location filename="../../ExtractSurface.cpp" line="44"/>
        <source>ExtractSurface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtractSurface.cpp" line="53"/>
        <source>Extracting Surface...</source>
        <translation>Extraction de la Surface...</translation>
    </message>
</context>
<context>
    <name>FillWithPoints</name>
    <message>
        <location filename="../../FillWithPoints.cpp" line="48"/>
        <source>Fill a surfacic mesh with regularly spaced nodes by creating new nodes inside the mesh.</source>
        <translation>Remplir un maillage surfacique avec des noeuds espacés régulièrement en créant des nouveaux noeuds à l&apos;intérieur du maillage.</translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="52"/>
        <source>Add Nodes</source>
        <translation>Ajouter des Noeuds</translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="54"/>
        <source>Points per bucket</source>
        <translation>Points par contenu</translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="54"/>
        <source>The number of points per bucket. &lt;br /&gt;A bucket represent a group of points in the input mesh.&lt;br /&gt; The less this number is, the higher the output mesh points density will be.</source>
        <translation>Le nombre de point par contenu.. &lt;br/&gt; Un cointenu represente un groupe de points dans le maillage en entrée.. &lt;br/&gt; Plus ce nombre est petit, plus la densité  de points du maillage de sortie sera grande.</translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="59"/>
        <source>Randomize?</source>
        <translation>Randomiser?</translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="59"/>
        <source>Randomize the position of the added points by +/- 0.5</source>
        <translation>Randomiser la position des points ajoutés par +/- 0.5</translation>
    </message>
</context>
<context>
    <name>ICPRegistration</name>
    <message>
        <location filename="../../ICPRegistration.cpp" line="43"/>
        <source>Iterative Closest Point algorithm bewteen two mesh.&lt;br/&gt;</source>
        <translation>Algorithme iteratif du point le plus proche entre deux maillages .&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="44"/>
        <source>At least two mesh components must be selected :</source>
        <translation>Au moins deux components doivent être sélectionnés :</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="46"/>
        <source>&lt;li&gt; The first one is the source mesh (the one to be registered) &lt;/li&gt;</source>
        <translation>&lt;li&gt; Le premier est le maillage source ( le seul a être déclaré) &lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="47"/>
        <source>&lt;li&gt; The second one is the target mesh &lt;/li&gt;</source>
        <translation>&lt;li&gt; Le second est le maillage cible &lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="51"/>
        <source>registration</source>
        <translation>Déclaration</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="54"/>
        <source>Number of iterations</source>
        <translation>Nombre d&apos;itération</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="54"/>
        <source>The number of iteration of the ICP algorithm.</source>
        <translation>Le nombre d&apos;itération de l&apos;algorithme ICP.</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="57"/>
        <source>Distance mesure type</source>
        <translation>Type de messure de distance</translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="57"/>
        <source>The distance mesure type use by the ICP algorithm.</source>
        <translation>Le Type de messure de distance utilisé par l&apos;algorithme ICP.</translation>
    </message>
</context>
<context>
    <name>InvertMesh</name>
    <message>
        <location filename="../../InvertMesh.cpp" line="44"/>
        <source>Invert the mesh faces</source>
        <translation>Inverser les faces de maillage</translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="49"/>
        <source>Inside Out</source>
        <translation>A l&apos;envers</translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="50"/>
        <source>Normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="51"/>
        <source>Flip</source>
        <translation>Retourner</translation>
    </message>
</context>
<context>
    <name>LoadTextureFromBMP</name>
    <message>
        <location filename="../../LoadTextureFromBMP.cpp" line="50"/>
        <source>LoadTextureFromBMP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LoadTextureFromBMP.cpp" line="69"/>
        <source>Open BMP File</source>
        <translation>Ouvrir le fichier BMP</translation>
    </message>
</context>
<context>
    <name>LoadTransformation</name>
    <message>
        <location filename="../../LoadTransformation.cpp" line="54"/>
        <source>LoadTransformation</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MergeMeshs</name>
    <message>
        <location filename="../../MergeMeshs.cpp" line="53"/>
        <source>&lt;br&gt;Create a new mesh from two input meshs and merge them. &lt;ul&gt;                    &lt;li&gt; If points are exactly at the same coordinates, they will be merged. &lt;/li&gt;                    &lt;li&gt; If cells (tetras, triangles) are overlapped, just one is kept (Points must be exactly at the same coordinates).&lt;/li&gt;                    &lt;li&gt; If a triangle is enclosed in a volume during the merging action, it will be removed.&lt;/li&gt;&lt;/ul&gt;                    The parameters below help you to print a file which contains an indice value for each element of each mesh after they are merged. If the parameter &lt;b&gt;File path&lt;/b&gt; is empty, the file is not printed.                    &lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING:&lt;/b&gt;At this stage, this action works only with triangular surfacic mesh or tetrahedral and/or triangular volume mesh.</source>
        <translation>&lt;br&gt;Créer un nouveau maillage à partir de deux maillages en entrée en les fusionnant &lt;ul&gt;                    &lt;li&gt; Si les points sont exactement aux mêmes coordonnées, ils seront fusionnés &lt;/li&gt;                    &lt;li&gt; Si les cellules (tetras, triangles) se chevauchent, un seul est conservé (Les points doivent être exactement aux mêmes coordonnées.&lt;/li&gt;                    &lt;li&gt; Si un triangle est englobé dans un volume durant la fusion, il sera supprimé.&lt;/li&gt;&lt;/ul&gt;                    Les paramêtres ci-dessous vous aide à imprimer un fichier qui contient un valeur indice pour chaque élement de chaque maillage aprés leur fusion. Si le paramêtre &lt;b&gt;Répertoire du fichier&lt;/b&gt; est vide, le fichier n&apos;est pas imprimé.                    &lt;br&gt;&lt;br&gt;&lt;b&gt;ATTENTION:&lt;/b&gt;A ce stade, cette action fonctionne seulement avec un maillage à surface triangulaire ou tetrahedrique et/ou maillage de volume a base de triangle.</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="64"/>
        <source>Merge Meshes</source>
        <translation>Fusionner les Maillages</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="66"/>
        <source>Value for mesh 1</source>
        <translation>Valeur du maillage 1</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="66"/>
        <location filename="../../MergeMeshs.cpp" line="71"/>
        <source>In the file, each triangle will be saved with its (indice, value)</source>
        <translation>Dans le fichier, chaque triangle sera sauvé avec son (indice,valeur)</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="71"/>
        <source>Value for mesh 2</source>
        <translation>Valeur du maillage 2</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="76"/>
        <source>File path</source>
        <translation>Repertoire du fichier</translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="76"/>
        <source>The place where to save the file</source>
        <translation>L&apos;endroit  où sauver le fichier</translation>
    </message>
</context>
<context>
    <name>MeshClipping</name>
    <message>
        <location filename="../../MeshClipping.cpp" line="56"/>
        <source>Interactive Mesh Clipping in the 3D Viewer</source>
        <translation>Découpe des Maillages Interactifs dans l&apos;afficheur 3D</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="60"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="61"/>
        <source>Clipping</source>
        <translation>Découpe</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="62"/>
        <source>3D Interaction</source>
        <translation>Interaction 3D</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="364"/>
        <source>Raw clipping</source>
        <translation>Découpe Brute</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="366"/>
        <source>Smooth clipping</source>
        <translation>Découpe régulière</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="379"/>
        <source>Clip Selected Components only</source>
        <translation>Découper seulement les Components Sélectionnés</translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="381"/>
        <source>Clip All Components</source>
        <translation>Découper tous les Components</translation>
    </message>
</context>
<context>
    <name>SaveDisplacementFromTransformation</name>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="52"/>
        <source>Save the displacement from the last translation in a text file. Alternatively, load a transformation from                    a text file and apply it on the selected Mesh component.</source>
        <translation>Sauver le déplacement depuis la dernière translation dans un fichier texte. Sinon, charger une transformation a partir                    d&apos;un fichier texte et l&apos;appliquer sur le component Maillage sélectionné.                   </translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="72"/>
        <source>Save As Ansys...</source>
        <translation>Sauver comme Ansys...</translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="166"/>
        <source>Load Transformation</source>
        <translation>Charger la transformation</translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="167"/>
        <source>Output file</source>
        <translation>Fichier en sortie</translation>
    </message>
</context>
<context>
    <name>SmoothFilter</name>
    <message>
        <location filename="../../SmoothFilter.cpp" line="47"/>
        <source>This filter adjusts point positions using Laplacian smoothing. &lt;br/&gt;It makes the cells better shaped and the vertices more evenly distributed. &lt;br /&gt; The effect is to &quot;relax&quot; the mesh, making the cells better shaped and the vertices more evenly distributed. Note that this filter operates on the lines, polygons, and triangle strips composing an instance of vtkPolyData. Vertex or poly-vertex cells are never modified. &lt;br /&gt;&lt;br /&gt;                      The algorithm proceeds as follows. For each vertex v, a topological and geometric analysis is performed to determine which vertices are connected to v, and which cells are connected to v. Then, a connectivity array is constructed for each vertex. (The connectivity array is a list of lists of vertices that directly attach to each vertex.) Next, an iteration phase begins over all vertices. For each vertex v, the coordinates of v are modified according to an average of the connected vertices. (A relaxation factor is available to control the amount of displacement of v). The process repeats for each vertex. This pass over the list of vertices is a single iteration. Many iterations (generally around 20 or so) are repeated until the desired result is obtained. &lt;br/&gt;&lt;br/&gt;                       There are some special parameters used to control the execution of this filter. (These parameters basically control what vertices can be smoothed, and the creation of the connectivity array.) The &lt;b&gt;Boundary smoothing&lt;/b&gt; paramerter enables/disables the smoothing operation on vertices that are on the &quot;boundary&quot; of the mesh. A boundary vertex is one that is surrounded by a semi-cycle of polygons (or used by a single line). &lt;br/&gt;&lt;br/&gt;                      Another important parameter is &lt;b&gt;Feature edge smoothing&lt;/b&gt;. If this ivar is enabled, then interior vertices are classified as either &quot;simple&quot;, &quot;interior edge&quot;, or &quot;fixed&quot;, and smoothed differently. (Interior vertices are manifold vertices surrounded by a cycle of polygons; or used by two line cells.) The classification is based on the number of feature edges attached to v. A feature edge occurs when the angle between the two surface normals of a polygon sharing an edge is greater than the FeatureAngle ivar. Then, vertices used by no feature edges are classified &quot;simple&quot;, vertices used by exactly two feature edges are classified &quot;interior edge&quot;, and all others are &quot;fixed&quot; vertices.&lt;br/&gt;&lt;br/&gt;                       Once the classification is known, the vertices are smoothed differently. Corner (i.e., fixed) vertices are not smoothed at all. Simple vertices are smoothed as before (i.e., average of connected vertex coordinates). Interior edge vertices are smoothed only along their two connected edges, and only if the angle between the edges is less than the EdgeAngle ivar.&lt;br/&gt;&lt;br/&gt;                      The total smoothing can be controlled by the &lt;b&gt;The Number of iterations&lt;/b&gt; which is a cap on the maximum number of smoothing passes.&lt;br/&gt;&lt;br/&gt;                      Note that this action does not create a new component, but modify the selected one(s).</source>
        <translation>(à traduire en Français) This filter adjusts point positions using Laplacian smoothing. &lt;br/&gt;It makes the cells better shaped and the vertices more evenly distributed. &lt;br /&gt; The effect is to &quot;relax&quot; the mesh, making the cells better shaped and the vertices more evenly distributed. Note that this filter operates on the lines, polygons, and triangle strips composing an instance of vtkPolyData. Vertex or poly-vertex cells are never modified. &lt;br /&gt;&lt;br /&gt;                      The algorithm proceeds as follows. For each vertex v, a topological and geometric analysis is performed to determine which vertices are connected to v, and which cells are connected to v. Then, a connectivity array is constructed for each vertex. (The connectivity array is a list of lists of vertices that directly attach to each vertex.) Next, an iteration phase begins over all vertices. For each vertex v, the coordinates of v are modified according to an average of the connected vertices. (A relaxation factor is available to control the amount of displacement of v). The process repeats for each vertex. This pass over the list of vertices is a single iteration. Many iterations (generally around 20 or so) are repeated until the desired result is obtained. &lt;br/&gt;&lt;br/&gt;                       There are some special parameters used to control the execution of this filter. (These parameters basically control what vertices can be smoothed, and the creation of the connectivity array.) The &lt;b&gt;Boundary smoothing&lt;/b&gt; paramerter enables/disables the smoothing operation on vertices that are on the &quot;boundary&quot; of the mesh. A boundary vertex is one that is surrounded by a semi-cycle of polygons (or used by a single line). &lt;br/&gt;&lt;br/&gt;                      Another important parameter is &lt;b&gt;Feature edge smoothing&lt;/b&gt;. If this ivar is enabled, then interior vertices are classified as either &quot;simple&quot;, &quot;interior edge&quot;, or &quot;fixed&quot;, and smoothed differently. (Interior vertices are manifold vertices surrounded by a cycle of polygons; or used by two line cells.) The classification is based on the number of feature edges attached to v. A feature edge occurs when the angle between the two surface normals of a polygon sharing an edge is greater than the FeatureAngle ivar. Then, vertices used by no feature edges are classified &quot;simple&quot;, vertices used by exactly two feature edges are classified &quot;interior edge&quot;, and all others are &quot;fixed&quot; vertices.&lt;br/&gt;&lt;br/&gt;                       Once the classification is known, the vertices are smoothed differently. Corner (i.e., fixed) vertices are not smoothed at all. Simple vertices are smoothed as before (i.e., average of connected vertex coordinates). Interior edge vertices are smoothed only along their two connected edges, and only if the angle between the edges is less than the EdgeAngle ivar.&lt;br/&gt;&lt;br/&gt;                      The total smoothing can be controlled by the &lt;b&gt;The Number of iterations&lt;/b&gt; which is a cap on the maximum number of smoothing passes.&lt;br/&gt;&lt;br/&gt;                      Note that this action does not create a new component, but modify the selected one(s).</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="60"/>
        <source>SMO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="63"/>
        <source>Boundary smoothing</source>
        <translation>Lissage des frontières</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="63"/>
        <source>Turn on/off the smoothing of vertices on the boundary of the mesh.</source>
        <translation>Mettre ON/OFF le lissage des sommets sur la forntière du maillage.</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="66"/>
        <source>Feature edge smoothing</source>
        <translation>Caratéristique du lissage des extrémités</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="66"/>
        <source>Turn on/off smoothing along sharp interior edges.</source>
        <translation>Mettre ON/OFF le lissage le long des bords pointus intérieurs.</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="69"/>
        <source>Number of iterations</source>
        <translation>Nombre d&apos;itérations</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="69"/>
        <source>Specify the number of iterations for Laplacian smoothing.</source>
        <translation>Spécifier le nombre d&apos;itérations pour un lissage Laplacienb.</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="74"/>
        <source>Edge angle</source>
        <translation>Angle extrême</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="74"/>
        <source>Specify the edge angle to control smoothing along edges (either interior or boundary).</source>
        <translation>Spécifier l&apos;angle extrême pour contrôler le lissage le long des bords (soit intérieur , soit en frontière). </translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="80"/>
        <source>Feature angle</source>
        <translation>Caractéristique de l&apos;angle</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="80"/>
        <source>Specify the feature angle for sharp edge identification.</source>
        <translation>Spécifier la caractéristique de l&apos;angle pour l&apos;identification des bords pointus.</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="86"/>
        <source>Relaxation factor</source>
        <translation>Facteur de relaxation</translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="86"/>
        <source>Specify the relaxation factor for Laplacian smoothing. &lt;br/&gt; As in all iterative methods, the stability of the process is sensitive to this parameter. In general, small relaxation factors and large numbers of iterations are more stable than larger relaxation factors and smaller numbers of iterations.</source>
        <translation>Spécifier du facteur de relaxation utilisé pour le lissage Laplacien. &lt;br/&gt;  comme dans une méthode itérative, la stabilité du processus est sensible à ce paramêtre. En général, des facteurs de relaxation faible associés à un grand nombre d&apos;itérations est plus stable que des facteurs de relaxation elevés associé à un  faible nombre d&apos;itérations.</translation>
    </message>
</context>
<context>
    <name>WarpOut</name>
    <message>
        <location filename="../../WarpOut.cpp" line="58"/>
        <source>Move the outside points along the normal in order to thicken a volumic mesh (works only with closed- centered- mesh).</source>
        <translation>Déplacer les points externes le long de la normal pour épaissir le maillage volumique (s&apos;applique seulement avec des maillages centrés fermés).</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="63"/>
        <source>Grow</source>
        <translation>Développer</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="64"/>
        <source>Thicken</source>
        <translation>Epaissir</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="66"/>
        <source>Displacement</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="66"/>
        <source>The length of the displacement of the mesh toward its normals.</source>
        <translation>La distance de déplacement du maillage jusqu&apos;à son ancien emplacement.  </translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>Warp Out</source>
        <translation>Déformer</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>Invalid mesh: the selected component (&quot;</source>
        <translation>Maillage non valide : le component sélectionné (&quot;</translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>&quot;) is not an unstructured grid nor a polydata.</source>
        <translation>&quot;) n&apos;est pas une grille non structurée ni une polydat.</translation>
    </message>
</context>
</TS>
