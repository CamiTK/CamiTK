<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ChangeColor</name>
    <message>
        <location filename="../../ChangeColor.cpp" line="38"/>
        <source>Change the surface, wireframe or points colors of objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ChangeColor.cpp" line="42"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputeNormals</name>
    <message>
        <location filename="../../ComputeNormals.cpp" line="19"/>
        <source>Compute normals of surface mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="22"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="23"/>
        <source>surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="33"/>
        <source>Compute Curvatures...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractSelection</name>
    <message>
        <location filename="../../ExtractSelection.cpp" line="43"/>
        <source>Extract the current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSelection.cpp" line="46"/>
        <source>selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSelection.cpp" line="53"/>
        <source>Extracting Surface...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeshQuality</name>
    <message>
        <location filename="../../MeshQuality.cpp" line="140"/>
        <source>Triangles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="145"/>
        <source>Tetrahedra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="150"/>
        <source>Hexahedra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="155"/>
        <source>Quads</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeshToImageStencil</name>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="54"/>
        <source>This action generates a new volume image and converts the mesh into a volume representation (vtkImageData) where the foreground voxels are colored and the background voxels are black.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="59"/>
        <source>STE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="65"/>
        <source>Dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="65"/>
        <source>The dimension of the output volumic image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="68"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="68"/>
        <source>The origin frame position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="71"/>
        <source>Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="71"/>
        <source>The spacing between each voxel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="74"/>
        <source>Output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="74"/>
        <source>The output filename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenderingOption</name>
    <message>
        <location filename="../../RenderingOption.cpp" line="54"/>
        <source>Surface representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="54"/>
        <source>Allow the selected mesh(es) to be representated as a surface?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="57"/>
        <source>Wireframe representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="57"/>
        <source>Allow the selected mesh(es) to be representated as wireframes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="60"/>
        <source>Points representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="60"/>
        <source>Allow the selected mesh(es) to be representated as with points?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="63"/>
        <source>Label representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="63"/>
        <source>Allow the selected mesh(es) to be representated with its label?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="66"/>
        <source>Glyph representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="66"/>
        <source>Allow the selected mesh(es) to have glyphes representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="69"/>
        <source>Normals representation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="69"/>
        <source>Allow the selected mesh(es) to be representated with its normals?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RigidTransformDialog</name>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="23"/>
        <source>Rigid Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="26"/>
        <source>TransformDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="39"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="57"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="75"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="97"/>
        <source>Automatically Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="107"/>
        <source>Concatenate Transformations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="114"/>
        <source>Uniform Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="123"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="149"/>
        <source>&amp;Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="162"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="185"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="195"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="208"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
