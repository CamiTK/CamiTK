/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ClearSelectionAction.h"

// CamiTK
#include <Application.h>
#include <Component.h>

using namespace camitk;

// --------------- constructor -------------------
ClearSelectionAction::ClearSelectionAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Clear Selection");
    setEmbedded(false);
    setDescription(tr("Clear the list of selected item"));
    setComponentClassName("");

    // Setting classification family and tags
    setFamily("Edit");
    addTag(tr("Clear Selection"));

    getQAction()->setShortcut(Qt::Key_Escape);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- getWidget --------------
QWidget* ClearSelectionAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus ClearSelectionAction::apply() {
    Application::clearSelectedComponents();
    Application::refresh();
    return SUCCESS;
}

