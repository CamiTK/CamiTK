/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef APPLICATION_EDIT_ACTION_EXTENSION_H
#define APPLICATION_EDIT_ACTION_EXTENSION_H

#include <ActionExtension.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * The ApplicationEditActionExtension class features all the application actions.
 *
 **/
class ApplicationEditActionExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.application.edit")

public:
    /// the constructor (needed to initialize the icon resources)
    ApplicationEditActionExtension() = default;

    /// the destructor
    virtual ~ApplicationEditActionExtension() = default;

    /// initialize all the actions
    virtual void init() override;

    /// Method that return the action extension name
    virtual QString getName() override {
        return "Edit Application Level Actions";
    };

    /// Method that return the action extension description
    virtual QString getDescription() override {
        return "This extension provides various actions to manipulate selection and paths (select/remove last instantiated, set path to test data, clear selection...)";
    };

};

#endif // APPLICATION_EDIT_ACTION_EXTENSION_H
