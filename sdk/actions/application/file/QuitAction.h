/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef QUIT_ACTION_H
#define QUIT_ACTION_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * When triggered, quit the current application.
 */
class QuitAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    QuitAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~QuitAction() = default;

    /// Returns NULL: no widget at all for this action
    virtual QWidget* getWidget();

public slots:
    /// apply the action: quit the application
    virtual camitk::Action::ApplyStatus apply();

};
#endif // QUIT_ACTION_H
