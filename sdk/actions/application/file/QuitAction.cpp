/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "QuitAction.h"

// CamiTK
#include <Application.h>
#include <MainWindow.h>
#include <Component.h>

using namespace camitk;

// --------------- constructor -------------------
QuitAction::QuitAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Quit");
    setEmbedded(false);
    setDescription(tr("Exit the application, prompting for additional information if needed"));
    setComponentClassName("");
    setIcon(QPixmap(":/fileQuit"));

    // Setting classification family and tags
    setFamily("File");
    addTag(tr("Quit Application"));
    addTag(tr("Exit Application"));

    // add a shortcut
    getQAction()->setShortcut(QKeySequence::Quit);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- getWidget --------------
QWidget* QuitAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus QuitAction::apply() {
    // close all components (and therefore ask the user to savet the modified ones)
    ApplyStatus closeAllStatus = Application::getAction("Close All")->apply();
    if (closeAllStatus == SUCCESS) {
        // Quit the app (will call Application::quitting() and therefore unload all action extensions and delete all actions)
        Application::quit();
        return SUCCESS;
    }
    else {
        // or abort the operation
        return ABORTED;
    }
}

