<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>CloseAction</name>
    <message>
        <location filename="../../CloseAction.cpp" line="36"/>
        <source>Close the currently selected components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CloseAction.cpp" line="42"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CloseAllAction</name>
    <message>
        <location filename="../../CloseAllAction.cpp" line="59"/>
        <source>Closing all the documents...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenAction</name>
    <message>
        <location filename="../../OpenAction.cpp" line="41"/>
        <source>Open data (component) from a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="48"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="68"/>
        <source>Opening file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="83"/>
        <source>Select One or More Files to Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="91"/>
        <source>All files loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="94"/>
        <source>Error loading files: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="98"/>
        <source>Open aborted.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenFile</name>
    <message>
        <location filename="../../OpenFile.cpp" line="45"/>
        <source>Open data (component) from a given file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="53"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>The name of the file to open.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="85"/>
        <source>File Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="89"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="100"/>
        <source>Open Selected File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="143"/>
        <source>Select One File to Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuitAction</name>
    <message>
        <location filename="../../QuitAction.cpp" line="39"/>
        <source>Exit the application, prompting for additional information if needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="45"/>
        <source>Quit Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="46"/>
        <source>Exit Application</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveAction</name>
    <message>
        <location filename="../../SaveAction.cpp" line="37"/>
        <source>Save all the top-level of the selected components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAction.cpp" line="43"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveAllAction</name>
    <message>
        <location filename="../../SaveAllAction.cpp" line="43"/>
        <source>Saves all the currently loaded data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="49"/>
        <source>Save All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="64"/>
        <source>Saving all data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="73"/>
        <source>Ready.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveAsAction</name>
    <message>
        <location filename="../../SaveAsAction.cpp" line="44"/>
        <source>Save the currently selected data under a different filename or format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="50"/>
        <source>Save As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="71"/>
        <source>Saving currently selected component under new filename or format...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="172"/>
        <location filename="../../SaveAsAction.cpp" line="177"/>
        <source>Save File As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="193"/>
        <source>Saving aborted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
