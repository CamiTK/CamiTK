/*****************************************************************************
 * 
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 ****************************************************************************/
#include "Restart.h"

using namespace camitk;


// -------------------- process --------------------
Action::ApplyStatus Restart::process() {
    Application::restart("Are you sure you want to restart the application?");
    return SUCCESS;
}

// -------------------- targetDefined --------------------
void Restart::targetDefined() {
    
}

// -------------------- parameterChanged --------------------
void Restart::parameterChanged(QString parameterName) {
}

// -------------------- init --------------------
void Restart::init() {
}