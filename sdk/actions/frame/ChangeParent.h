/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CHANGEPARENT_H
#define CHANGEPARENT_H

#include <Action.h>

/** This action allows you to modify the selected component's frame parent
 */
class ChangeParent : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    ChangeParent(camitk::ActionExtension*);

    /// Destructor
    virtual ~ChangeParent() = default;

    /// update the possible parent lists
    virtual QWidget* getWidget();

public slots:
    /**
     * Update the component's frame with the parent frame selected by the user
     */
    virtual camitk::Action::ApplyStatus apply();

private:
    // currently selected/active component
    camitk::Component* currentComponent;

    // used to map item id with component for the combobox
    QList<camitk::Component*> possibleParentList;

    // update the list of components
    void updateComboBox();
};

#endif // CHANGEPARENT_H
