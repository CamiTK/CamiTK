/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COLORPOINTWIDGET_H
#define COLORPOINTWIDGET_H

// Qt
#include <QMainWindow>
#include <QString>
// Include GUI  automatically generated from the file
// ColorPointWidget.ui (to be modified via QtDesigner).
#include "ui_ColorPointWidget.h"

/**
 * @ingroup group_sdk_actions_image_volumerendering
 *
 * @brief
 * This widget handle specifically the color of each voxel (R, G, B) in the output rendered image.
 *
 * \image html actions/volume_rendering_2.png "The color point widget appears in the bottom part of the widget." width=10cm
 *
 */
class ColorPointWidget : public QWidget  {
    Q_OBJECT

public:

    ColorPointWidget(QWidget* volumeRenderingWidget);
    ColorPointWidget(QWidget* volumeRenderingWidget, double grayLevel, QColor color);
    double getGrayLevel();
    QColor getColor();

public slots:
    virtual void remove();
    virtual void colorButtonClicked();

private:
    Ui_ColorPointWidget ui;
    QWidget* volumeRenderingWidget;
    QColor currentColor;

};
#endif // COLORPOINTWIDGET_H
