/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// - Qt stuff
#include <QColor>
#include <QColorDialog>

// - Local stuff
#include "ColorPointWidget.h"
#include "VolumeRenderingWidget.h"

ColorPointWidget::ColorPointWidget(QWidget* volumeRenderingWidget) : QWidget(volumeRenderingWidget) {
    ui.setupUi(this);
    this->volumeRenderingWidget = volumeRenderingWidget;
    this->currentColor = QColor(255, 255, 255, 255);
    QString s = tr("background-color: ");
    ui.colorPushButton->setStyleSheet(s + currentColor.name());

}

ColorPointWidget::ColorPointWidget(QWidget* volumeRenderingWidget, double grayLevel, QColor color) : QWidget(volumeRenderingWidget) {
    ui.setupUi(this);
    this->volumeRenderingWidget = volumeRenderingWidget;
    this->currentColor = QColor(255, 255, 255, 255);

    ui.doubleSpinBox->setValue(grayLevel);

    if (color.isValid()) {
        currentColor = color;
    }
    QString s = tr("background-color: ");
    ui.colorPushButton->setStyleSheet(s + currentColor.name());

}

void ColorPointWidget::remove() {
    auto* dadVR = dynamic_cast<VolumeRenderingWidget*>(this->volumeRenderingWidget);
    if (dadVR) {
        dadVR->removeColorPoint(this);
    }
}

void ColorPointWidget::colorButtonClicked() {
    QColor color = QColorDialog::getColor(currentColor, this, tr("Please choose a color"), QColorDialog::ShowAlphaChannel);

    if (color.isValid()) {
        currentColor = color;
        QString s = tr("background-color: ");
        ui.colorPushButton->setStyleSheet(s + currentColor.name());
    }

}

double ColorPointWidget::getGrayLevel() {
    return ui.doubleSpinBox->value();
}

QColor ColorPointWidget::getColor() {
    return currentColor;
}
