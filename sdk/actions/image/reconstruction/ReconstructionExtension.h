/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef RECONSTRUCTIONEXTENSION_H
#define RECONSTRUCTIONEXTENSION_H

#include <QObject>
#include <Action.h>
#include <ActionExtension.h>

/**
 * @ingroup group_sdk_actions_image_reconstruction
 *
 * @brief
 * This reconstruction action extension.
 *
 */
class ReconstructionExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.reconstruction")

public:
    /// the constructor
    ReconstructionExtension() : ActionExtension() {};

    /// the destructor
    virtual ~ReconstructionExtension() = default;

    /// initialize all the actions
    virtual void init() override;

    /// Method that return the action extension name
    virtual QString getName() override {
        return "Reconstruction";
    };

    /// Method that return the action extension description
    virtual QString getDescription() override {
        return "Extension that provides 3D reconstruction from an image component (e.g. marching cube)";
    };

};


#endif // RECONSTRUCTIONEXTENSION_H
