/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <ImageComponent.h>
#include <Application.h>
#include <Log.h>

#include "ImageLutAction.h"
#include "ImageLutWidget.h"


using namespace camitk;



// --------------- constructor -------------------
ImageLutAction::ImageLutAction(ActionExtension* extension) : Action(extension) {
    setName("Image Look Up Table");
    setDescription("Modify the LUT of an image components");
    setComponentClassName("ImageComponent");
    setFamily("View");
    addTag("LUT");
}

// --------------- getWidget -------------------
QWidget* ImageLutAction::getWidget() {

    // The currently selected ImageComponent should already contains a LUT.
    //  If this is not the case, then no interaction is possible.
    //  This is the case for instance for images that have more than one component, e.g. color images.
    // see ImageComponent::initLookupTable()
    ImageComponent* comp = dynamic_cast<ImageComponent*>(getTargets().last());
    if (comp->getLut() == nullptr) {
        CAMITK_WARNING(tr("Image Look Up Table is not supported on colored images or images with no LUT. Aborting."))
        return nullptr;
    }

    //-- create the widget if needed
    if (!actionWidget) {
        actionWidget = new ImageLutWidget();
    }

    Application::setOverrideCursor(QCursor(Qt::WaitCursor));

    //--  update the LUT values (the getTargets() might have changed since the last call)
    dynamic_cast<ImageLutWidget*>(actionWidget)->updateComponent(comp);

    Application::setOverrideCursor(QCursor(Qt::ArrowCursor));

    return actionWidget;
}

// --------------- getWidget -------------------
Action::ApplyStatus ImageLutAction::apply() {
    return Action::SUCCESS;
}

