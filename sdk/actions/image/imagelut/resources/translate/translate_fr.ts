<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ui_ImageLutWidget</name>
    <message>
        <location filename="../../ImageLutWidget.ui" line="20"/>
        <source>Image Volume</source>
        <translation>Volume de l&apos;Image</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="23"/>
        <source>Image Volume Property</source>
        <translation>Propriété du Volume de l&apos;Image</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="26"/>
        <source>You can change here the propreties value of the Image
Volume data component...</source>
        <translation>Vous pouvez changer les valeurs des propriétés de l&apos;Image
component Données Volume ...</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="33"/>
        <source>LookUp Table (LUT)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="65"/>
        <source>Window Level</source>
        <translation>Niveau de la fenêtre</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="142"/>
        <source>Window Width</source>
        <translation>Taille de la Fenêtre</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="206"/>
        <source>Transparency for min value</source>
        <translation>Transparence pour la Valeur min</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="232"/>
        <source>Transparency for max value</source>
        <translation>Transparence pour la Valeur max</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="327"/>
        <source>Invert</source>
        <translation>Inverser</translation>
    </message>
    <message>
        <location filename="../../ImageLutWidget.ui" line="337"/>
        <source>Reset</source>
        <translation></translation>
    </message>
</context>
</TS>
