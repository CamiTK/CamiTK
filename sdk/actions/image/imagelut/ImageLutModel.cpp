/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ImageLutModel.h"

#include <QTranslator>
#include <Log.h>

// ---------------------------- constructor ----------------------------
ImageLutModel::ImageLutModel(vtkSmartPointer<vtkImageData> image) {
    this->image = image;
    greyLevels = nullptr;

    // update min and max voxel values
    double* imgRange = image->GetScalarRange();
    minValue = imgRange[0];
    maxValue = imgRange[1];

    // default number of bin is 256
    nbHistoBins = std::min((unsigned) 256, (unsigned) abs(maxValue - minValue + 1));

    updateHistogram();
}

// ---------------------------- ImageLutModel ----------------------------
ImageLutModel::~ImageLutModel() {
    delete greyLevels;
    greyLevels = nullptr;
}

// ---------------------------- updateHistogram ----------------------------
void ImageLutModel::updateHistogram() {
    //-- update greyLevels array
    if (greyLevels) {
        delete greyLevels;
    }

    // allocate the memory needed for that component
    greyLevels = new double[nbHistoBins];

    //-- initialize histogram values to 0
    for (unsigned int i = 0; i < nbHistoBins; i++) {
        greyLevels[i] = 0.0;
    }

    //-- image dimensions
    int* dims = image->GetDimensions();

    //-- build histogram
    for (unsigned int i = 0; i < (unsigned) dims[0]; i++) {
        for (unsigned int j = 0; j < (unsigned) dims[1]; j++) {
            for (unsigned int k = 0; k < (unsigned) dims[2]; k++) {
                // using component 0 (== red in case of a color image)
                greyLevels[getBinIndex(image->GetScalarComponentAsDouble(i, j, k, 0))] += 1.0;
            }
        }
    }

    //-- compute max value
    highestGreyLevel = 0;
    for (unsigned int i = 0; i < nbHistoBins; i++) {
        if (greyLevels[i] > highestGreyLevel) {
            highestGreyLevel = greyLevels[i];
        }
    }
}

// ---------------------------- getBinValue ----------------------------
double ImageLutModel::getBinValue(int index) {
    if (index >= 0 && index < (int)nbHistoBins) {
        return greyLevels[index];
    }
    else {
        return 0.0;
    }
}

// ---------------------------- getMaxBinValue ----------------------------
double ImageLutModel::getMaxBinValue() {
    return highestGreyLevel;
}


// ---------------------------- getBinIndex ----------------------------
int ImageLutModel::getBinIndex(double value) {
    int index = getBinIndexAsDouble(value);
    if (index < 0) {
        index = 0;
    }
    else {
        if (index >= (int) nbHistoBins) {
            index = nbHistoBins - 1;
        }
    }
    return index;
}

// ---------------------------- getIndexAsDouble ----------------------------
double ImageLutModel::getBinIndexAsDouble(double value, bool checkBound) {
    if (checkBound && value < minValue) {
        return 0;
    }
    else {
        if (checkBound && value > maxValue) {
            return nbHistoBins - 1;
        }
        else {
            return ((value - minValue) / (maxValue - minValue)) * double(nbHistoBins) - 1;
        }
    }
}

// ---------------------------- getMaxValue ----------------------------
double ImageLutModel::getMaxValue() {
    return maxValue;
}

// ---------------------------- getMinValue ----------------------------
double ImageLutModel::getMinValue() {
    return minValue;
}

// ---------------------------- getNumberOfBins ----------------------------
int ImageLutModel::getNumberOfBins() {
    return nbHistoBins;
}

// ---------------------------- setNumberOfBins ----------------------------
void ImageLutModel::setNumberOfBins(int binCount) {
    if (binCount != (int) nbHistoBins) {
        nbHistoBins = std::min((unsigned) binCount, (unsigned) abs(maxValue - minValue + 1));
        updateHistogram();
    }
}

// ---------------------------- getLevelPercent ----------------------------
int ImageLutModel::getPercentFromLevel(double level) {
    return 100.0 * (level - minValue) / (maxValue - minValue);
}

// ---------------------------- getWindowPercent ----------------------------
int ImageLutModel::getPercentFromWindow(double window) {
    return 100.0 * window / (maxValue - minValue);
}

// ---------------------------- getLevelFromPercent ----------------------------
double ImageLutModel::getLevelFromPercent(int levelPercent) {
    return minValue + double(levelPercent) * (maxValue - minValue) / 100.0;
}

// ---------------------------- getWindowFromPercent ----------------------------
double ImageLutModel::getWindowFromPercent(int windowPercent) {
    return  double(windowPercent) * (maxValue - minValue) / 100.0;
}

