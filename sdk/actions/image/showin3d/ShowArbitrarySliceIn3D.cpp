/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShowArbitrarySliceIn3D.h"

#include <ImageComponent.h>
#include <ArbitrarySingleImageComponent.h>

using namespace camitk;

// --------------- constructor -------------------
ShowArbitrarySliceIn3D::ShowArbitrarySliceIn3D(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show Arbitrary Slice In 3D");
    setEmbedded(false);
    setDescription(tr("Toggle display of the arbitrary slice(s) in 3D"));
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    setFamily("View");
    addTag(tr("3D Viewer"));
    addTag(tr("Arbitrary Slice"));

}

// --------------- destructor -------------------
ShowArbitrarySliceIn3D::~ShowArbitrarySliceIn3D() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ShowArbitrarySliceIn3D::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus ShowArbitrarySliceIn3D::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* img = dynamic_cast<ImageComponent*>(comp);

        if (img != nullptr && img->getArbitrarySlices() != nullptr) {
            img->getArbitrarySlices()->setVisibility("3D Viewer", !img->getArbitrarySlices()->getVisibility("3D Viewer"));
            Application::refresh();
        }
    }

    return SUCCESS;
}

// -------------------- getQAction --------------------
QAction* ShowArbitrarySliceIn3D::getQAction(Component* target) {
    QAction* myQAction = Action::getQAction();
    if (target != nullptr) {
        ImageComponent* img = dynamic_cast<ImageComponent*>(target);

        if (img != nullptr && img->getArbitrarySlices() != nullptr) {
            // make sure the status is visible
            myQAction->setCheckable(true);
            // update visibility GUI
            myQAction->setChecked(img->getArbitrarySlices()->getVisibility("3D Viewer"));
        }
    }

    return Action::getQAction();
}
