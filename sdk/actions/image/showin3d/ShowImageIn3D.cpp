/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShowImageIn3D.h"

#include <ImageComponent.h>

using namespace camitk;

// --------------- constructor -------------------
ShowImageIn3D::ShowImageIn3D(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show Image In 3D");
    setEmbedded(false);
    setDescription(tr("Shows the Image Volume in 3D viewer"));
    setComponentClassName("ImageComponent");
    // TODO: set a small icon with Axial/Coronal/Sagittal views in 3D

    // Setting classification family and tags
    setFamily("View");
    addTag(tr("3D Viewer"));

    // add shortcut
    ShowImageIn3D::getQAction()->setCheckable(true);
    ShowImageIn3D::getQAction()->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_D));
    ShowImageIn3D::getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- destructor -------------------
ShowImageIn3D::~ShowImageIn3D() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ShowImageIn3D::getWidget() {
    ImageComponent* img = dynamic_cast<ImageComponent*>(getTargets().last());
    if (img != nullptr) {
        bool isDisplayedIn3D = img->property("Display Image in 3D Viewer").toBool();
        getQAction()->setChecked(isDisplayedIn3D);
    }
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus ShowImageIn3D::apply() {

    // fill the toplevel component set
    foreach (Component* comp, getTargets()) {
        ImageComponent* img = dynamic_cast<ImageComponent*>(comp);
        if (img) {
            bool isDisplayedIn3D = img->property("Display Image in 3D Viewer").toBool();
            // toggle
            isDisplayedIn3D = !isDisplayedIn3D;
            img->setProperty("Display Image in 3D Viewer", isDisplayedIn3D);
            getQAction()->setChecked(isDisplayedIn3D);
            Application::refresh();
        }
    }

    return SUCCESS;
}

// -------------------- getQAction --------------------
QAction* ShowImageIn3D::getQAction(Component* target) {
    QAction* myAction = Action::getQAction(target);

    if (target != nullptr) {
        ImageComponent* img = dynamic_cast<ImageComponent*>(target);
        if (img) {
            myAction->setChecked(img->property("Display Image in 3D Viewer").toBool());
        }
    }

    return myAction;
}
