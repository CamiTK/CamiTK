# Call CamiTK CMake Macro to define the action
camitk_extension(ACTION_EXTENSION
                 DEFINES COMPILE_PIXELCOLORCHANGER_ACTION_API
                 CEP_NAME SDK
                 NEEDS_VIEWER_EXTENSION interactivesliceviewer
                 DESCRIPTION "Allows changing the color of the image pixels"
                 ENABLE_AUTO_TEST
                 TEST_FILES mixed_3D_beam.msh brain.mha sinus.mhd
)
