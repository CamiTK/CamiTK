<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ResampleAction</name>
    <message>
        <location filename="../../ResampleAction.cpp" line="60"/>
        <source>New image X dimension</source>
        <translation>Dimension Nouvelle Image X</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="60"/>
        <source>The new image width (in voxels).</source>
        <translation>Nouvelle Largeur d&apos;image (en voxels).</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="65"/>
        <source>New image Y dimension</source>
        <translation>Dimension Nouvelle Image Y</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="65"/>
        <source>The new image height (in voxels).</source>
        <translation>Nouvelle Hauteur d&apos;image (en voxels).</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="70"/>
        <source>New image Z dimension</source>
        <translation>Dimension Nouvelle Image Z</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="70"/>
        <source>The new image depth (in voxels).</source>
        <translation>Nouvelle Profondeur d&apos;image (en voxels).</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="75"/>
        <source>New image scalar type</source>
        <translation>Type Nouvelle Image Scalaire</translation>
    </message>
    <message>
        <location filename="../../ResampleAction.cpp" line="75"/>
        <source>The new image voxels scalar type</source>
        <translation>Nouveau Type de scalaire voxel de l&apos;image</translation>
    </message>
</context>
</TS>
