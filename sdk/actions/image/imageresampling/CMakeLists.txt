# Call CamiTK CMake Macro to define the action
camitk_extension(ACTION_EXTENSION
                 CEP_NAME SDK
                 DESCRIPTION "Re-sample image volumes"
                 ENABLE_AUTO_TEST
                 TEST_FILES liver-smooth.obj brain.mha sinus.mhd
)
