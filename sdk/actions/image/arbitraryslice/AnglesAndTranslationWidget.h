/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ANGLESANDTRANSLATIONWIDGET_H
#define ANGLESANDTRANSLATIONWIDGET_H

#include "ui_AnglesAndTranslationWidget.h"

#include <QWidget>
class AnglesAndTranslationAction;

/**
 * @ingroup group_sdk_actions_image_arbitraryslice
 *
 * @brief
 * This class describes the QWidget that allows user to change the X,Y ans Z angle of the arbitrary slice
 * of a 2D image. Moreover it also allows one to change the slice number using a slider.
 * \note
 *  For initialization purpose, widget must be used for only one component (arbitrary).
 *  If several component are selected, do not build / display it.
 *
 *  Use corresponding .ui file created with Qt Designer.
 */
class AnglesAndTranslationWidget : public QWidget {
    Q_OBJECT

public:
    /// Default construtor
    AnglesAndTranslationWidget(AnglesAndTranslationAction*, QWidget* parent = 0);

    /// Destructor
    virtual ~AnglesAndTranslationWidget();

    /// update the UI depending on the action
    void updateGUI();

private slots:

    /// Update the translation value
    void translationSpinBoxChanged(double);
    void translationSliderChanged(int);

    /// Method that update the angle dialog slider (text + value)
    void xAngleDialValueChanged(int);
    void yAngleDialValueChanged(int);
    void zAngleDialValueChanged(int);

    /// switch to a specific viewer
    void showArbitraryViewer(bool);
    void show3DViewer(bool);
    void showAllViewer(bool);

    void resetTransform();

private:
    /// update the angle dialog slider label using the current dial value
    void updateAngleSliderLabel(QDial*);

    // the GUI class itself
    Ui::AnglesAndTranslationWidget ui;

    /// the action to refer to
    AnglesAndTranslationAction* myAction;
};

#endif // ANGLESANDTRANSLATIONWIDGET_H
