#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_ELASTIX_REGISTRATION_API
#define ELASTIX_REGISTRATION_API __declspec(dllexport)
#else
#define ELASTIX_REGISTRATION_API __declspec(dllimport)
#endif // COMPILE_MY_COMPONENT_API
#else // for all other platforms ELASTIX_REGISTRATION_API is defined to be "nothing"
#define ELASTIX_REGISTRATION_API
#endif // MSVC and mingw