/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ExtensionTabPresenter.h"

#include "QtPropertyFieldEditor.h"
#include "TextFieldEditor.h"
#include "ExpandableFieldEditor.h"

// -------------------- constructor --------------------
ExtensionTabPresenter::ExtensionTabPresenter(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, QWidget* parent) : QScrollArea(parent) {
    QWidget* container = new QWidget(parent);

    setWidget(container);
    setWidgetResizable(true);
    setStyleSheet("QScrollArea { background-color: palette(button); }");

    QGridLayout* gridLayout = new QGridLayout(container);
    gridLayout->setMargin(0);

    QtPropertyFieldEditor* qtPropertyFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["name"], "Name", "Name of the extension (cannot be modified interactively).");
    qtPropertyFieldEditor->setReadOnly(true); // name is read only as consequences of modifying names on the fly are hard to manage
    qtPropertyFieldEditor->appendRowTo(gridLayout);

    QStringList generationTypeEnum = {"HotPlug", "Standard"};
    QtPropertyFieldEditor* generationTypeFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["generationType"], "Type", R"(<p>There are two types of extension:</p>
        <ul>
            <li>
                <b>HotPlug</b> extensions are more versatile, faster and easier to use as they are generated on the fly from
                their CamiTK extension file. The source code you need to write is kept to bare minimum. This is the only option if you are developing in Python. Even in C++, HotPlug extension gives you the possibility to avoid application restart most of the time.
                <ul>
                    <li>
                        <b>Advantages:</b> when you modify the action source code, <b>modifications are taken into account
                        immediately</b> (no need to restart) and <b>redistributing your extension is easy</b> as you just
                        need to pack the CamiTK extension file and the action source codes (even in C++, unless you have
                        developed your own libraries). In C++ you still need to rebuild your code after a modification, but
                        that's about it, no need to restart the application (although restart is still recommended after a
                        clean+rebuild or clean+reconfigure).
                    </li>
                    <li>
                        <b>Disadvantages:</b> Actions are instantiated when the application is started, which might impact the launch time and leads to <b>slower application start</b> (especially visible in C++).
                    </li>
                </ul>
            <li>
                <b>Standard</b> extensions have to be loaded classically (i.e., using the standard and only available style up to CamiTK version 5.2). It uses classic ActionExtension plugin. Only C++ extension can be made available for this type.
                <ul>
                    <li>
                        <b>Advantages: </b>startup time is faster</b> and <b>you don't need to distribute your source code</b> as everything is built and contained into a dynamic library
                    </li>
                    <li>
                        <b>Disadvantage</b> When you modify a source code, you need to go through the usual C++ development
                        steps and restart the application for your modification to be taken into account.
                    </li>
                </ul>
            </li>
        </ul>
        <p>If you are a C++ developer, the CamiTK team recommends that you start with the <i>HotPlug</i> extension type, and
            then, when your code is more stable, you convert it to <i>Standard</i>. The conversion from <i>HotPlug</i> to
            <i>Standard</i> can be done automatically using the extension generator (beware that conversion back to <i>HotPlug</i> is possible but it will have to be done manually).</p>)", "HotPlug");
    generationTypeFieldEditor->setEnumValues(generationTypeEnum);
    generationTypeFieldEditor->appendRowTo(gridLayout);

    TextFieldEditor* textFieldEditor = new TextFieldEditor(presenter, dataModel["description"], "Description", "Description of the extension.");
    textFieldEditor->appendRowTo(gridLayout);

    QStringList licenseEnum = {"LGPL-3 CamiTK", "LGPL-3", "GPL-3", "BSD"};
    qtPropertyFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["license"], "License", "Source code license. You can choose one of the known licenses or edit the last combo box item to add your own.", "LGPL-3 CamiTK");
    qtPropertyFieldEditor->setEnumValues(licenseEnum);
    qtPropertyFieldEditor->setPlaceHolderText("Edit to specify another license");
    qtPropertyFieldEditor->setEditableEnum(true); // if the user wants to enter other license
    qtPropertyFieldEditor->appendRowTo(gridLayout);

    VariantDataModel& contactInfo = dataModel["contact"];
    if (!contactInfo.isValid()) {
        // if contactInfo does not exists, creates an empty map so that adding name and email are valid actions
        dataModel["contact"] = VariantDataModel(QVariantMap());
    }
    // Check if "contact" exists
    qtPropertyFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["contact"]["author"], "Author", "Name of the extension author", "");
    qtPropertyFieldEditor->appendRowTo(gridLayout);

    qtPropertyFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["contact"]["email"], "Email", "Email of the extension author", "");
    qtPropertyFieldEditor->setRegExp(R"(^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$)");
    qtPropertyFieldEditor->appendRowTo(gridLayout);

    //-- properties
    QWidget* expandableInnerWidget = new QWidget();
    QGridLayout* expandableInnerGridLayout = new QGridLayout(expandableInnerWidget);
    expandableInnerGridLayout->setMargin(0);

    QtPropertyFieldEditor* alwaysRebuildFieldEditor = new QtPropertyFieldEditor(presenter, dataModel["alwaysRebuild"], "Always Rebuild", "Always rebuild the user action libraries when the extension is loaded (e.g. each time camitk-imp is started)", true);
    alwaysRebuildFieldEditor->appendRowTo(expandableInnerGridLayout);

    connect(generationTypeFieldEditor, &FieldEditor::valueChanged, this, [ = ](VariantDataModel & generationType) {
        alwaysRebuildFieldEditor->setEnabled(generationType.toString() == "HotPlug");
    });

    textFieldEditor = new TextFieldEditor(presenter, dataModel["extensionDependencies"], "Dependencies", QString(R"(<p>List of the extension dependencies expressed as camitk extension macro options. Possible values are: </p>
        <ul>
            <li><tt>NEEDS_ITK</tt></li>
            <li><tt>NEEDS_LIBXML2</tt></li>
            <li><tt>NEEDS_OPENCV</tt></li>
            <li><tt>NEEDS_IGSTK</tt></li>
            <li><tt>NEEDS_XSD</tt></li>
            <li><tt>NEEDS_GDCM</tt></li>
            <li><tt>NEEDS_ACTION_EXTENSION action1 action2 ...</tt></li>
            <li><tt>NEEDS_VIEWER_EXTENSION viewer1 viewer2 ...</tt></li>
            <li><tt>NEEDS_COMPONENT_EXTENSION component1 component2 ...</tt></li>
            <li><tt>NEEDS_CEP_LIBRARIES CEPLib1 CEPLib2 ...</tt></li>
            <li><tt>INCLUDE_DIRECTORIES dir1 dir2 ...</tt></li>
            <li><tt>DEFINES flag1 flag2 ...</tt></li>
            <li><tt>CXX_FLAGS flag1 flag2 ...</tt></li>
            <li><tt>EXTERNAL_SOURCES file1 file2 ...</tt></li>
            <li><tt>EXTERNAL_LIBRARIES lib1 lib2 ...</tt></li>
            <li><tt>HEADERS_TO_INSTALL header1.h header2.h ...</tt></li>
            <li><tt>INSTALL_ALL_HEADERS</tt></li>
            <li><tt>TARGET_NAME non-default-targetname</tt></li>
        </ul>
        <p><b>Note:</b> you don't have to fill this now. The first run of the extension generator will generate a default <tt>CMakeLists.txt</tt>. You can edit it anytime after this first generation, depending on your requirements. Once generated the first time, <tt>CMakeLists.txt</tt> will never be overwritten by the extension generator.</p>)"));
    textFieldEditor->setLanguage("cmake");
    textFieldEditor->appendRowTo(expandableInnerGridLayout);
    ExpandableFieldEditor* advancedEditor = new ExpandableFieldEditor(presenter, expandableInnerWidget, dataModel, "Advanced", false, false);
    advancedEditor->appendRowTo(gridLayout);
}
