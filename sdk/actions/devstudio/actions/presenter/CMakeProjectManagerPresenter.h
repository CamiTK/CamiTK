/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __CMAKE_PROJECT_MANAGER_PRESENTER__
#define __CMAKE_PROJECT_MANAGER_PRESENTER__

#include <CMakeProjectManager.h>

#include <QListWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

/**
 * @brief Presenter for CMakeProjectManager
 * 
 * This widget will create a dialog where the user can see the different stages,
 * starts the process and examine the results of each stages.
 * 
 */
class CMakeProjectManagerPresenter : public QWidget {
    Q_OBJECT

public:
    /// constructor
    /// @param camitkFilePath the path to the file to be used by CMakeProjectManager
    /// @param stages the list of stages to perform
    /// @param parent (optional)
    CMakeProjectManagerPresenter(const QString& camitkFilePath, const QList<CMakeProjectManager::CMakeProjectManagerStage>& stages, QWidget *parent = nullptr);

    /// Destructor
    ~CMakeProjectManagerPresenter();

signals:
    /// sent when all the stages are finished with the resulting status
    void allStagesFinished(bool status);
    /// sent when the widget's window is closed
    void cMakeProjectManagerPresenterClosed();
    
private slots:
    /// called when a stage is selected in the GUI list
    void stageClicked(QListWidgetItem *item);

    /// called when the given stage is starting
    void stageStarted(const QString &stage);

    /// called when the given stage is finished with the given success status and output (stdout and stderr)
    void stageFinished(const QString &stage, bool success, const QString &output);

    /// called when the user clicks on the "Start" button
    void start();

    /// called when the user clicks on the "Close" button or closes this widget's window
    void closeClicked();

protected:
    /// called when the user closes the widget's window using the window manager button
    void closeEvent(QCloseEvent *event) override;

private:
    /// where the stage list is displayed
    QListWidget *stageListWidget;

    /// where the output of the selected stage will be displayed
    QTextEdit *outputTextEdit;

    /// the managed CMakeProjectManager
    CMakeProjectManager *cmakeProjectManager;

    /// A label that acts as an application message status bar
    QLabel* statusBarLabel;

    /// The button (initially labelled "Starts", and then labelled "Close" when the pipeline is finished)
    QPushButton* startCloseButton;

    /// Get the list item pointer that corresponds to the given stage
    QListWidgetItem* getListItem(QString stage);
};

#endif // __CMAKE_PROJECT_MANAGER_PRESENTER__
