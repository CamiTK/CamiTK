/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __CAMITK_PROPERTY_FIELD_EDITOR__
#define __CAMITK_PROPERTY_FIELD_EDITOR__


#include "FieldEditor.h"
#include "QtPropertyFieldEditor.h"

/**
 * @brief Field editor for CamiTK property
 * 
 * Presents the different attributes of a property: name, type, initial value, ...
 * 
 */
class CamiTKPropertyFieldEditor : public FieldEditor {
public:
    /// Constructor
    CamiTKPropertyFieldEditor(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, const QString& name = "", const QString& description = "");
    
    /// the editor widget
    virtual QWidget* getWidget();

    /// overriden in order to synchronize the row label with the name field, and add a line between rows
    virtual void appendRowTo(QGridLayout* layout, bool canBeDeleted) override;

protected:
    /// @name edited properties and fields
    /// Required to synchronize the CamiTK property label to the name,
    /// and the value editor to the current type, and the property attributes
    /// @{
    QtPropertyFieldEditor* nameFieldEditor;
    QtPropertyFieldEditor* typeFieldEditor;
    FieldEditor* defaultValueEditor;
    QWidget* minMaxWidget;
    QtPropertyFieldEditor* minimumValueEditor;
    QtPropertyFieldEditor* maximumValueEditor;
    QtPropertyFieldEditor* singleStepEditor;
    QtPropertyFieldEditor* decimalsEditor;
    QtPropertyFieldEditor* regExpEditor;
    ///@}

    /// the main grid layout
    QGridLayout* mainGridLayout;
    
    /// the advanced grid layout
    QGridLayout* expandableGridLayout;

    /// separator line inserted after the field editor (separator)
    QFrame* separatorLine;

    /// add a field editor to a specific grid cell (allows for stacking more than default label/field editor)
    void addToGrid(QGridLayout* gridLayout, FieldEditor* field, int row, int column, int columnSpan = 1);

    /// build all the editors that depends from the current type
    void buildTypeDependentEditors(VariantDataModel& dataModelType, bool overwriteDefault = true);

protected slots:

    /// Called when the delete button is called
    /// Do the same as FieldEditor, but also remove the separator line frame
    virtual void deleteButtonClicked();
};


#endif // __CAMITK_PROPERTY_FIELD_EDITOR__
