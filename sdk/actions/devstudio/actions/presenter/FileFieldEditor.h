/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __FILE_FIELD_EDITOR__
#define __FILE_FIELD_EDITOR__

#include "FieldEditor.h"

/**
 * @brief FieldEditor for file 
 * 
 * Provides an QLineEdit with a button that open a file selection dialog to select an existing file.
 * 
 * Note: the default filter for file selection is for all supported pixmap.
 * Call setFilter to give a list of filters.
 * 
 * Note: the selected file is left where it is, unless you call copyToPath to set a specific path.
 * When a path is set, the selected file will be automatically copied to the specified path.
 * (this is usefully to keep a local copy, i.e. for icons used in camitk extension files, but it
 * can be awkward as you might end up with residual files in the specified path if the user
 * modify the selected path!)
 * 
 */
class FileFieldEditor : public FieldEditor {
    Q_OBJECT
public:
    /// Constructor
    FileFieldEditor(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, const QString& name = "", const QString& description = "");
    
    /// set the file extension filter
    void setFilter(QString filter);

    /// set the path to copy the selected file to
    void copyToPath(QString path);

    /// Build an editable QListWidget (where items can be added an removed)
    virtual QWidget* getWidget();

private slots:
    void chooseFile();

private:
    // filter to use for the file dialog (default = images)
    QString filter;

    // path to copy to (default is null string, meaning do not copy)
    QString path;

    // the line edit showing the file name (read only)
    QLineEdit* lineEdit;

    // list of file format supported by QImageReader
    QString getDefaultImageFilter();
};


#endif // __FILE_FIELD_EDITOR__
