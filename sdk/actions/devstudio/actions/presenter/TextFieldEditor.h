/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __TEXT_FIELD_WIDGET__
#define __TEXT_FIELD_WIDGET__

#include "FieldEditor.h"

/**
 * @brief FieldEditor for long/multiline string  
 * 
 * Provides a widget to edit a string in a text area with (optional) language syntax highlighting
 * 
 * Currently supported languages are:
 * - JSON
 * - C++
 * - CMake
 */
class TextFieldEditor : public FieldEditor {
    Q_OBJECT
public:
    /// Constructor
    TextFieldEditor(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, const QString& name = "", const QString& description = "");

    /// set the language for the text field
    /// Supported languages are: `json`, `cpp` and `cmake`
    void setLanguage(const QString& language);

    /// set the displayed text when the field value is empty
    void setPlaceHolderText(const QString& placeHolderText);

    /// Build a QTextEdit and sets syntax highlighting according to the optionally given language (@see setLanguage)
    virtual QWidget* getWidget();

public slots:
    /// called when the text has changed (get the plain text string and inform the presenter of the change)
    void valueChangedInTextEdit();

private:
    /// can be empty if none is provided (default)
    QString language;

    /// place holder text for the widget, can be empty if none is provided
    QString placeHolderText;
};

#endif // __TEXT_FIELD_WIDGET__