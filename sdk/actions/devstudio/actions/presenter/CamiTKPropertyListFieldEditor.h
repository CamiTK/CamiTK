/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __CAMITK_PROPERTY_LIST_FIELD_EDITOR__
#define __CAMITK_PROPERTY_LIST_FIELD_EDITOR__

#include "ExpandableFieldEditor.h"

/**
 *  @brief Same as ExpandableFieldEditor but has a "+" button on the left that, when clicked, adds a new CamiTKPropertyFieldEditor at the end of the inner widget
 * 
 */
class CamiTKPropertyListFieldEditor : public ExpandableFieldEditor {
public:
    /// constructor
    CamiTKPropertyListFieldEditor(CamiTKExtensionModelPresenter* presenter, QWidget* innerWidget, VariantDataModel& dataModel, const QString& label = "Advanced Plus", bool compact = false, bool expandedAtInitialization = false);

    /// get the widget (adds a "+" at the end of the ExpandableFieldEditor title + connect the "+" button clicked signal to the corresponding slot)
    virtual QWidget* getWidget();

public slots:
    /// called when the "+" button is clicked
    void plusButtonClicked();
};

#endif // __CAMITK_PROPERTY_LIST_FIELD_EDITOR__