/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __CUSTOM_INPUT_DIALOG__
#define __CUSTOM_INPUT_DIALOG__

#include <QDialog>
#include <QLineEdit>

/**
 * @brief A Utility class to show an input dialog with a long description underneath the line edit
 * 
 * Usage
 * \code
 *  CustomInputDialog dialog("Enter Something", "Please enter something:", "<p>Please!</p>", "foo", parentWidget);
 *  if (dialog.exec() == QDialog::Accepted) {
 *       QString inputName = dialog.getInput();
 *       ...
 *  }
 * \endcode
 */
class CustomInputDialog : public QDialog {
    Q_OBJECT

public:
    /// Constructor
    /// @param title dialog titel
    /// @param topMessage is displayed on top the line edit
    /// @param bottomMessage is displayed under the line edit
    /// @param defaultValue is the default value of the line edit
    /// @param parent (optional)
    CustomInputDialog(const QString& title, const QString& topMessage, const QString& bottomMessage, const QString& defaultValue, QWidget* parent = nullptr);

    /// get the line edit value, once the dialog is closed
    QString getInput() const;

private:
    /// the line edit used to get the user input
    QLineEdit* inputLineEdit;
};

#endif // __CUSTOM_INPUT_DIALOG__