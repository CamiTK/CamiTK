/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ExpandableFieldEditor.h"

#include <QVBoxLayout>
#include <QStyle>

// -------------------- constructor --------------------
ExpandableFieldEditor::ExpandableFieldEditor(CamiTKExtensionModelPresenter* presenter, QWidget* innerWidget, VariantDataModel& dataModel, const QString& label, bool compact, bool expandedAtInitialization) : FieldEditor(presenter, dataModel) {
    this->innerWidget = innerWidget;
    this->label = label;
    this->compact = compact;
    this->expandedAtInitialization = expandedAtInitialization;
}

// -------------------- getWidget --------------------
QWidget* ExpandableFieldEditor::getWidget() {
    if (myWidget == nullptr) {
        myWidget = new QWidget();
        QVBoxLayout* advancedLayout = new QVBoxLayout(myWidget);
        // compact space
        if (compact) {
            myWidget->setContentsMargins(0, 0, 0, 0);
            advancedLayout->setMargin(0);
            advancedLayout->setSpacing(0);
            advancedLayout->setContentsMargins(0, 0, 0, 0);
        }

        // The first line (expand button ">" with its label and optionally (see CamitkPropertyListFieldEditor) the "+" button)
        expandableWidgetTitleLayout = new QHBoxLayout();
        expandButton = new QPushButton(myWidget->style()->standardIcon(QStyle::SP_ToolBarVerticalExtensionButton), label);
        expandButton->setFixedHeight(expandButton->fontMetrics().height());
        expandButton->setStyleSheet("background-color: palette(button); text-align: left;");

        expandableWidgetTitleLayout->addWidget(expandButton);
        advancedLayout->addLayout(expandableWidgetTitleLayout);

        // Initially hide advanced settings
        if (!expandedAtInitialization) {
            innerWidget->hide();
            expandButton->setIcon(myWidget->style()->standardIcon(QStyle::SP_ToolBarHorizontalExtensionButton));
        }
        advancedLayout->addWidget(innerWidget);

        connect(expandButton, &QPushButton::clicked, this, &ExpandableFieldEditor::expandButtonClicked);
    }
    return myWidget;
}

// -------------------- appendRowTo --------------------
void ExpandableFieldEditor::appendRowTo(QGridLayout* layout) {
    myGridLayout = layout;
    getWidget()->setWhatsThis(description);
    getWidget()->setToolTip(description);
    myGridLayout->addWidget(getWidget(), myGridLayout->rowCount(), 0, 1, 2);
}

// -------------------- expandButtonClicked --------------------
void ExpandableFieldEditor::expandButtonClicked(bool checked) {
    if (innerWidget->isVisible()) {
        innerWidget->hide();
        expandButton->setIcon(myWidget->style()->standardIcon(QStyle::SP_ToolBarHorizontalExtensionButton));
    }
    else {
        innerWidget->show();
        expandButton->setIcon(myWidget->style()->standardIcon(QStyle::SP_ToolBarVerticalExtensionButton));
    }
}
