/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ExtensionTypeTabPresenter.h"

#include <Application.h> // for getting action families and component class names
#include <Action.h>

#include "QtPropertyFieldEditor.h"
#include "TextFieldEditor.h"
#include "CamiTKPropertyFieldEditor.h"
#include "ExpandableFieldEditor.h"
#include "CamiTKPropertyListFieldEditor.h"
#include "ListFieldEditor.h"
#include "FileFieldEditor.h"

// -------------------- constructor --------------------
ExtensionTypeTabPresenter::ExtensionTypeTabPresenter(camitk::ExtensionManager::ExtensionType extensionType, CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, QWidget* parent) : extensionType(extensionType), presenter(presenter), dataModel(dataModel), QScrollArea(parent) {
}

// -------------------- buildEnum --------------------
QStringList ExtensionTypeTabPresenter::buildEnum(QStringList originalList) {
    // QMap to store the count of each string
    QMap<QString, int> countMap;
    for (const QString& str : originalList) {
        countMap[str]++;
    }

    // Create a QList of QPairs, where each pair is a string and its count
    QList<QPair<int, QString>> pairList;
    for (auto word : countMap.keys()) {
        pairList.append(qMakePair(countMap.value(word), word));
    }

    // Sort the QList in descending order of count
    std::sort(pairList.begin(), pairList.end(), std::greater<>());

    // Create a new QStringList with unique strings, sorted by popularity
    QStringList sortedList;
    for (const auto& pair : pairList) {
        sortedList.append(pair.second);
    }
    return sortedList;
}

// -------------------- getDataModel --------------------
VariantDataModel& ExtensionTypeTabPresenter::getDataModel() {
    return dataModel;
}

// -------------------- getExtensionType --------------------
const QString ExtensionTypeTabPresenter::getExtensionType() const {
    switch (extensionType) {
        case camitk::ExtensionManager::ACTION:
            return "Action";
            break;

        case camitk::ExtensionManager::COMPONENT:
            return "Component";
            break;

        case camitk::ExtensionManager::VIEWER:
            return "Viewer";
            break;

        default:
            return "unsupported";
            break;
    }
}