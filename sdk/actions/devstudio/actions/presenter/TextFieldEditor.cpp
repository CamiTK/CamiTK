/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "TextFieldEditor.h"

// syntax highlighters
#include "JsonSyntaxHighlighter.h"
#include "CppSyntaxHighlighter.h"
#include "CMakeSyntaxHighlighter.h"

// -------------------- Constructor --------------------
TextFieldEditor::TextFieldEditor(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, const QString& name, const QString& description) : FieldEditor(presenter, dataModel, name, description) {
}

// -------------------- setLanguage --------------------
void TextFieldEditor::setLanguage(const QString& language) {
    this->language = language;
}

// -------------------- setPlaceHolderText --------------------
void TextFieldEditor::setPlaceHolderText(const QString& placeHolderText) {
    this->placeHolderText = placeHolderText;
}

// -------------------- getWidget --------------------
QWidget* TextFieldEditor::getWidget() {
    if (myWidget == nullptr) {
        QTextEdit* textEdit = new QTextEdit();
        if (language == "json") {
            textEdit->setFont(QFont("Monospace"));
            new JsonSyntaxHighlighter(textEdit->document());
        }
        else if (language == "cpp") {
            textEdit->setFont(QFont("Monospace"));
            new CppSyntaxHighlighter(textEdit->document());
        }
        else if (language == "cmake") {
            textEdit->setFont(QFont("Monospace"));
            new CMakeSyntaxHighlighter(textEdit->document());
        }
        textEdit->setPlainText(dataModel);
        textEdit->setPlaceholderText(placeHolderText);
        connect(textEdit, &QTextEdit::textChanged, this, &TextFieldEditor::valueChangedInTextEdit);
        textEdit->setWhatsThis(description);
        textEdit->setToolTip(description);

        myWidget = textEdit;
    }
    return myWidget;
}

// -------------------- valueChangedInTextEdit --------------------
void TextFieldEditor::valueChangedInTextEdit() {
    QTextEdit* changedTextEdit = dynamic_cast<QTextEdit*>(myWidget);

    if (changedTextEdit) {
        dataModel = changedTextEdit->toPlainText();
        presenter->modelUpdated();
    }
}
