/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __LIST_FIELD_EDITOR__
#define __LIST_FIELD_EDITOR__

#include "FieldEditor.h"

class QListWidget;

/**
 * @brief FieldEditor for list 
 * 
 * Provides an editable list widget for string list data model.
 * 
 */
class ListFieldEditor : public FieldEditor {
    Q_OBJECT
public:
    /// Constructor
    ListFieldEditor(CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, const QString& name = "", const QString& description = "");

    /// Build an editable QListWidget (where items can be added an removed)
    virtual QWidget* getWidget();

protected:
    /// update the VariantDataModel according to the modification in the GUI (and informed the CamiTKExtensionModelPresenter)
    void updateModel();

private:
    /// the list widget
    QListWidget* listWidget;
};


#endif // __LIST_FIELD_EDITOR__
