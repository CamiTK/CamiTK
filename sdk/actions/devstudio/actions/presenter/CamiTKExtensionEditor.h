/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __CAMITK_EXTENSION_EDITOR__
#define __CAMITK_EXTENSION_EDITOR__

#include "CamiTKExtensionModelPresenter.h"
#include "CMakeProjectManagerPresenter.h"

#include <QWidget>
#include <QLabel>
#include <QTimer>
#include <QPushButton>
#include <QProcess>

/**
 * @brief Editor of CamiTK extension file
 * 
 * This class uses
 * - a CamiTKExtensionModelPresenter widget to let the user edit a CamiTK extension file
 * - a CMakeProjectManagerPresenter widget to check the code generation, configure and build phases
 * - a save button to let the user save the CamiTK extension file 
 * - a initialize/update button to generate, configure and build the source code from the CamiTK extension file (thanks to the CMakeProjectManagerPresenter)
 * - a register/unregister button to register a valid CamiTK extension file in the application (thanks to the corresponding DevStudio actions)
 * - a status bar to show various message similary to an application status bar
 * - all the logic to link things in the right order and make sure the user does save its changes before closing the widget or generating/configuring/building the source code.
 * 
 */
class CamiTKExtensionEditor : public QWidget {
    Q_OBJECT

public:
    /// Constructor
    /// @param camitkFilePath path to the CamiTK extension file to edit (can be empty for a new extension)
    /// @param newExtensionName the extension name ("action", "component", or "viewer"), needed for creating of a new extension
    CamiTKExtensionEditor(const QString& camitkFilePath, const QString& newExtensionName = QString(), QWidget *parent = nullptr);

    /// Destructor
    virtual ~CamiTKExtensionEditor();

signals:
    /// sent when the window is being closed
    void extensionGeneratorPresenterClosed();

public slots:
    /// called when the user clicks on "Save"
    void saveClicked();

    /// called when the user clicks on "Close" or use the close button in the window manager title bar
    void closeClicked();

    /// called when the user clicks on "Initialize/Update"
    void initializeClicked();

    /// called when the user clicks on "Register/Unregister"
    void registerClicked();

    /// show a message for the given time (can be a warning that is shown in red)
    void showMessage(const QString &message, int durationMilliseconds = 2000, bool warning = false);

    /// called when the CamiTK extension model was updated in the GUI (shows a "*" in the title bar)
    void dataModelUpdated();

protected:
    /// called when the widget is being closed
    void closeEvent(QCloseEvent *event) override;

private:
    /// required UI elements
    ///@{}
    QLabel *statusBarLabel;
    QTimer *statusBarTimer;
    QPushButton* registerButton;
    QPushButton* initializeButton;
    bool hasUnsavedChanges;
    ///@}

    // current edited file
    QString camitkFilePath;

    // the extension editor
    CamiTKExtensionModelPresenter* extensionPresenter;

    // the cmake project manager presenter (can be nullptr)
    CMakeProjectManagerPresenter* cmakeProjectManagerPresenter;

    /// update button states and displayed strings (Initialize/Update and Register/Unregister)
    void updateButtonStatus(bool initializationWasSuccessful = false);
};

#endif // __CAMITK_EXTENSION_EDITOR__
