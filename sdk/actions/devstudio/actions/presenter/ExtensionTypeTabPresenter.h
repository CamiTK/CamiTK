/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef __EXTENSION_TYPE_TAB_PRESENTER__
#define __EXTENSION_TYPE_TAB_PRESENTER__

// camitk
#include <ExtensionManager.h>
#include <VariantDataModel.h>

// Qt
#include <QScrollArea>

// presenter
#include "CamiTKExtensionModelPresenter.h"

/**
 * @brief Base class for all extension type tab presenter (ActionTabPresenter, ComponentTabPresenter and ViewerTabPresenter)
 * 
 */
class ExtensionTypeTabPresenter : public QScrollArea {
    Q_OBJECT
public:
    /// constructor
    ExtensionTypeTabPresenter(camitk::ExtensionManager::ExtensionType extensionType, CamiTKExtensionModelPresenter* presenter, VariantDataModel& dataModel, QWidget* parent = nullptr);

    /// get the managed data model
    VariantDataModel& getDataModel();

    /// get the extension type as a string
    const QString getExtensionType() const;

protected:

    /// Use originalList (unsorted strings with potential duplicates) to generate
    /// a new QStringList that has only unique strings, sorted by popularity
    QStringList buildEnum(QStringList originalList);

    /// the currently managed data model
    VariantDataModel& dataModel;

    /// the current top level presenter
    CamiTKExtensionModelPresenter* presenter;

    /// The extension type 
    camitk::ExtensionManager::ExtensionType extensionType;
};


#endif // __EXTENSION_TYPE_TAB_PRESENTER__