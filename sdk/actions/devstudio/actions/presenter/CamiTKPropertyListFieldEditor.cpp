/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "CamiTKPropertyListFieldEditor.h"
#include "CamiTKPropertyFieldEditor.h"

// -------------------- constructor --------------------
CamiTKPropertyListFieldEditor::CamiTKPropertyListFieldEditor(CamiTKExtensionModelPresenter* presenter, QWidget* innerWidget, VariantDataModel& dataModel, const QString& label, bool compact, bool expandedAtInitialization) : ExpandableFieldEditor(presenter, innerWidget, dataModel, label, compact, expandedAtInitialization) {
    // nothing to do
}

// -------------------- getWidget --------------------
QWidget* CamiTKPropertyListFieldEditor::getWidget() {
    if (myWidget == nullptr) {
        myWidget = ExpandableFieldEditor::getWidget();

        // A plus button enables to add more items using the + button
        QPushButton* addButton = new QPushButton("+");
        addButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        addButton->setFixedSize(addButton->fontMetrics().height(), addButton->fontMetrics().height());
        QString itemLabel = label.toLower();
        // tool tip uses the label but without the "s"
        if (itemLabel.endsWith('s')) {
            itemLabel.chop(1);
        }
        addButton->setToolTip("Add a new " + itemLabel);
        expandableWidgetTitleLayout->addWidget(addButton);
        connect(addButton, &QPushButton::clicked, this, &CamiTKPropertyListFieldEditor::plusButtonClicked);
    }
    return myWidget;
}

// -------------------- plusButtonClicked --------------------
void CamiTKPropertyListFieldEditor::plusButtonClicked() {
    // show advanced
    if (!innerWidget->isVisible()) {
        expandButtonClicked(true);
    }

    VariantDataModel newParameter = VariantDataModel(QVariantMap());
    dataModel.append(newParameter);

    // In addFieldWidget add a parameter to say that the label has to change (not the title) when this parameter name is changed
    CamiTKPropertyFieldEditor* paramEditor = new CamiTKPropertyFieldEditor(presenter, dataModel.last(), "New Parameter");
    QGridLayout* innerGridLayout = dynamic_cast<QGridLayout*>(innerWidget->layout());
    if (innerGridLayout != nullptr) {
        paramEditor->appendRowTo(innerGridLayout, true);
    }

    presenter->modelUpdated();
}