/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __CAMITK_EXTENSION_MODEL_PRESENTER__
#define __CAMITK_EXTENSION_MODEL_PRESENTER__

#include <CamiTKExtensionModel.h>

// Qt
#include <QTabWidget>
#include <QtVariantProperty>
#include <QHBoxLayout>
#include <QTextEdit>

/** Presenter for a CamiTK extension file model.
 * 
 * \note The only implemented extension model is action extension
 *  
 * This creates 
 * - a tab to edit the action extension,
 * - one tab per action
 * - one tab with the resulting (updated) CamiTK extension JSON
 */
class CamiTKExtensionModelPresenter : public QWidget {
    Q_OBJECT

public:
    /// constructor
    /// If the camitkFilePath does not have a valid model, this will create a new one
    /// @param camitkFilePath path to the CamiTK extension file to edit
    /// @param newExtensionName the extension name ("action", "component", or "viewer"), needed for creating of a new extension
    /// @param parent (optional) 
    /// 
    CamiTKExtensionModelPresenter(const QString& camitkFilePath, const QString& newExtensionName, QWidget* parent = nullptr);

    /// destructor
    ~CamiTKExtensionModelPresenter();

    bool isModified();

    /// save the data model
    void saveExtensionFile(const QFile& file);

    /// make the presenter emit the show message signal
    void emitShowMessage(const QString &message, int durationMilliseconds = 2000, bool warning = false);

signals:
    /// emitted when a message has to be displayed to the user
    void showMessage(const QString &message, int durationMilliseconds = 2000, bool warning = false);

    /// emitted when the data model was updated by the user
    void dataModelUpdated();

public slots:
    /// called when a tab is closed (only action tabs are closable)
    void tabCloseRequest(int index);

    /// called when the user clicks on the "+" button to add an action tab
    void plusButtonClicked();

    /// called when the model has changed, the GUI/JSON is to be updated
    void modelUpdated();

private:
    /// currently managed extension file
    CamiTKExtensionModel camitkExtensionModel;

    /// current inner tab widget (the View of the MVC)
    QTabWidget* tabWidget;

    /// the text edit in the last tab that shows the json source (also part of the view)
    QTextEdit* jsonTextEdit;
};

#endif // __CAMITK_EXTENSION_MODEL_PRESENTER__