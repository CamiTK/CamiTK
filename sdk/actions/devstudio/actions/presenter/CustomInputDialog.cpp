/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "CustomInputDialog.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>

// -------------------- constructor ------------------
CustomInputDialog::CustomInputDialog(const QString& title, const QString& topMessage, const QString& bottomMessage, const QString& defaultValue, QWidget* parent) : QDialog(parent) {
    QVBoxLayout* layout = new QVBoxLayout(this);

    QLabel* topLabel = new QLabel(topMessage, this);
    layout->addWidget(topLabel);

    inputLineEdit = new QLineEdit(this);
    inputLineEdit->setText(defaultValue);
    layout->addWidget(inputLineEdit);

    QLabel* bottomLabel = new QLabel(bottomMessage, this);
    bottomLabel->setWordWrap(true);
    layout->addWidget(bottomLabel);

    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addWidget(buttonBox);

    setLayout(layout);
    setWindowTitle(title);
    setFixedSize(sizeHint().width() + 20, sizeHint().height() + 20); // Adjust size based on content
}

// -------------------- getInput ------------------
QString CustomInputDialog::getInput() const {
    return inputLineEdit->text();
}
