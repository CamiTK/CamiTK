/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "CMakeSyntaxHighlighter.h"

// -------------------- constructor --------------------
CMakeSyntaxHighlighter::CMakeSyntaxHighlighter(QTextDocument* parent) : camitk::SyntaxHighlighter(parent) {
    // Format for camitk_extension arguments
    addRule(QRegularExpression(QStringLiteral("\\b(NEEDS_ITK|NEEDS_LIBXML2|NEEDS_OPENCV|NEEDS_IGSTK|NEEDS_XSD|NEEDS_GDCM|NEEDS_ACTION_EXTENSION|NEEDS_VIEWER_EXTENSION|NEEDS_COMPONENT_EXTENSION|NEEDS_CEP_LIBRARIES|INCLUDE_DIRECTORIES|DEFINES|CXX_FLAGS|EXTERNAL_SOURCES|EXTERNAL_LIBRARIES|HEADERS_TO_INSTALL|INSTALL_ALL_HEADERS|TARGET_NAME)\\b")), QColor("#a626a4"), QFont::Bold);

    // Format for comments
    addRule(QRegularExpression(QStringLiteral("#[^\n]*")), Qt::darkGray);

    // Format for variables
    addRule(QRegularExpression(QStringLiteral("\\${[^}]+}")), QColor("#e45649"));
}