/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "CppSyntaxHighlighter.h"

// -------------------- constructor --------------------
CppSyntaxHighlighter::CppSyntaxHighlighter(QTextDocument* parent): camitk::SyntaxHighlighter(parent) {
    // keyword
    addRule(QRegularExpression(QStringLiteral(R"(\b(char|const|enum|double|inline|int|long|operator|private|protected|public|slots|static|template|typedef|typename|unsigned|virtual|void|bool)\b)")), QColor("#a626a4"), QFont::Bold);

    // class names (only works for Qt classes, vtk, itk classes and few CamiTK classes
    addRule(QRegularExpression(QStringLiteral(R"(\b(Q[A-Za-z]+|vtk[A-Za-z]+|itk[A-Za-z]+|ImageComponent|MeshComponent|Component)\b)")), QColor("#e45649"), QFont::Bold);

    // one line comment
    addRule(QRegularExpression(QStringLiteral("//[^\n]*")), Qt::darkGray);

    // quotes
    addRule(QRegularExpression(QStringLiteral("\".*\"")), QColor("#0366d6"));

    // functions/methods
    addRule(QRegularExpression(QStringLiteral("\\b[A-Za-z0-9_]+(?=\\()")), QColor("#50a14f"), QFont::Normal, true);
}