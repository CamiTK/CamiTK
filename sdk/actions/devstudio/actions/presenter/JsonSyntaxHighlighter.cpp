/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "JsonSyntaxHighlighter.h"

// -------------------- constructor --------------------
JsonSyntaxHighlighter::JsonSyntaxHighlighter(QTextDocument* parent) : camitk::SyntaxHighlighter(parent) {
    // keys
    addRule(QRegularExpression("\"[^\"]*\"(?=\\s*:)"), QColor("#a31515")); // #a71d5d #c18401 #e45649

    // strings
    addRule(QRegularExpression(R"re(("([^"\\]*(\\.[^"\\]*)*)")\s*[,\]\}\b])re", QRegularExpression::MultilineOption), QColor("#50a14f"));

    // number
    addRule(QRegularExpression(R"re(\b-?(0[xX][0-9a-fA-F]+|\d*\.?\d+([eE][-+]?\d+)?)\b)re"), QColor("#098658")); // #50a14f

    // boolean
    addRule(QRegularExpression(QStringLiteral(R"(\b(true|false)\b)")), QColor("#016d9b")); // #0366d6 #183691

    // null
    addRule(QRegularExpression(QStringLiteral(R"(\bnull\b)")), QColor("#808080"));

    // braces
    addRule(QRegularExpression(QStringLiteral(R"([{}])")), QColor("#000000"));

    // brackets
    addRule(QRegularExpression(QStringLiteral(R"([\[\]])")), QColor("#000000"));

    // colon
    addRule(QRegularExpression(QStringLiteral(R"(:)")), QColor("#000000"));

    // comma
    addRule(QRegularExpression(QStringLiteral(R"(,)")), QColor("#000000"));
}
