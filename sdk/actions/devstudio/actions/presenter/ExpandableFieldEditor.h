/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef __EXPANDABLE_FIELD_EDITOR__
#define __EXPANDABLE_FIELD_EDITOR__

#include "FieldEditor.h"

#include <QPushButton>

/**
 * @brief An expandable field editor is encapsulating a widget that can be initially hidden and can be shown by clicking on an expand button.
 * 
 * Its role is to simplify the appearance of the GUI to present a relatively smaller number of fields to edit.
 * 
 */
class ExpandableFieldEditor : public FieldEditor {
public:
    ExpandableFieldEditor(CamiTKExtensionModelPresenter* presenter, QWidget* innerWidget, VariantDataModel& dataModel, const QString& label = "Advanced", bool compact = false, bool expandedAtInitialization = false);

    /// Build the advanced widget that encapsulate the innerWidget
    virtual QWidget* getWidget();

    /// Append this widget to the given grid layout
    /// As ExpandableFieldEditor don't have any label, the widget is spanned over 2 columns
    virtual void appendRowTo(QGridLayout* layout);

public slots:
    /// called when the ">" button is clicked (shows/hides the inner widget)
    void expandButtonClicked(bool checked);

protected:
    /// the advanced/hidden inner widget
    QWidget* innerWidget;
    /// the expand button (">")
    QPushButton* expandButton;
    /// The title label (left to the ">" button)
    QString label;
    /// The layout that has the expand button and label. In subclass AdvandedPlusFieldEditor a "+" is added at the left
    QHBoxLayout* expandableWidgetTitleLayout;
    // if true, the the widget is compacted to minimal space
    bool compact;
    // if true the hidden widget starts in an expanded state
    bool expandedAtInitialization;
};

#endif // __EXPANDABLE_FIELD_EDITOR__