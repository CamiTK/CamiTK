/*****************************************************************************
 *
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ****************************************************************************/
#include "NewExtensionFile.h"

// CamiTK includes
#include <Property.h>
#include <Log.h>

#include "CustomInputDialog.h"

using namespace camitk;

// -------------------- init --------------------
void NewExtensionFile::init() {
    extensionEditor = nullptr;
    setIcon(QPixmap(":/fileNew"));
}

// -------------------- process --------------------
Action::ApplyStatus NewExtensionFile::process() {
    if (extensionEditor == nullptr) {
        // ask for a name
        CustomInputDialog dialog("CamiTK Extension Name", "Please enter the camitk extension name:", "<p>Please choose wisely. This name will be used to generate the extension source code.</p><p>While renaming an extension is always possible, it should be done very carefully as there are many consequences. Therefore once entered here the extension name will be presented as a <i>read only field</i>. If a rename is needed in the future, changing the extension name can be done by directly modifying the .CamiTK (JSON) file.</p>", "Sample Extension", nullptr);
        if (dialog.exec() == QDialog::Accepted) {
            QString inputName = dialog.getInput();
            if (!inputName.isEmpty()) {
                extensionEditor = new CamiTKExtensionEditor(QString(), inputName);
                connect(extensionEditor, &CamiTKExtensionEditor::extensionGeneratorPresenterClosed, this, [ = ]() {
                    extensionEditor = nullptr; // no need to delete as this will be done when the windows is closed
                    actionWidget = nullptr;
                });
            }
            else {
                return Action::ABORTED;
            }
        }
        else {
            return Action::ABORTED;
        }
    }

    extensionEditor->show();
    extensionEditor->raise();

    return Action::SUCCESS;
}

// -------------------- targetDefined --------------------
void NewExtensionFile::targetDefined() {

}

// -------------------- parameterChanged --------------------
void NewExtensionFile::parameterChanged(QString parameterName) {
}