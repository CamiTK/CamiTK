/*****************************************************************************
 *
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ****************************************************************************/

#include "OpenExtensionFile.h"


#include <QFileDialog>
#include <QMessageBox>

using namespace camitk;

// -------------------- init --------------------
void OpenExtensionFile::init() {
    extensionEditor = nullptr;
    setIcon(QPixmap(":/fileOpen"));
}

// -------------------- process --------------------
Action::ApplyStatus OpenExtensionFile::process() {
    if (extensionEditor == nullptr) {
        QString camitkExtensionFile = getParameterValueAsString("CamiTK File");
        if (camitkExtensionFile.isEmpty()) {
            camitkExtensionFile = QFileDialog::getOpenFileName(nullptr, tr("Open CamiTK Extension File"),
                                  Application::getLastUsedDirectory().absolutePath(),
                                  tr("CamiTK Extension Files (*.camitk)"));
        }
        if (!camitkExtensionFile.isEmpty()) {
            extensionEditor = new CamiTKExtensionEditor(camitkExtensionFile);
            connect(extensionEditor, &CamiTKExtensionEditor::extensionGeneratorPresenterClosed, this, [ = ]() {
                extensionEditor = nullptr; // no need to delete as this will be done when the windows is closed
                actionWidget = nullptr;
                // reset parameter
                setParameterValue("CamiTK File", QString());
            });
        }
        else {
            return Action::ABORTED;
        }        
    }

    extensionEditor->show();
    extensionEditor->raise();

    return Action::SUCCESS;
}

// -------------------- targetDefined --------------------
void OpenExtensionFile::targetDefined() {
}

// -------------------- parameterChanged --------------------
void OpenExtensionFile::parameterChanged(QString parameterName) {
}
