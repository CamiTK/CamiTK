/*****************************************************************************
 * 
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 ****************************************************************************/
#include "RegisterExtension.h"

#include "HotPlugExtensionManager.h"
#include <QFileDialog>

using namespace camitk;

// -------------------- process --------------------
Action::ApplyStatus RegisterExtension::process() {
    QString camitkExtensionFile = getParameterValueAsString("CamiTK File");
    if (camitkExtensionFile.isEmpty()) {
        camitkExtensionFile = QFileDialog::getOpenFileName(nullptr, tr("Open CamiTK Extension File"),
                              Application::getLastUsedDirectory().absolutePath(),
                              tr("CamiTK Extension Files (*.camitk)"));

    }
    if (!camitkExtensionFile.isEmpty()) {
        HotPlugExtensionManager::registerExtension(camitkExtensionFile);
        Application::refresh(); // to refresh dev menus
        return SUCCESS;
    }
    else {
        return ABORTED;
    }
    return SUCCESS;
}

// -------------------- targetDefined --------------------
void RegisterExtension::targetDefined() {
}

// -------------------- parameterChanged --------------------
void RegisterExtension::parameterChanged(QString parameterName) {
}

// -------------------- init --------------------
void RegisterExtension::init() {
}
