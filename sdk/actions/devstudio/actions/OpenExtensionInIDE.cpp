/*****************************************************************************
 * 
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 ****************************************************************************/
#include "OpenExtensionInIDE.h"

#include <Log.h>


using namespace camitk;

// -------------------- init --------------------
void OpenExtensionInIDE::init() {
}

// -------------------- findExecutable --------------------
QString OpenExtensionInIDE::findExecutable(const QString& executableName) {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QStringList paths = env.value("PATH").split(QDir::listSeparator());

    foreach (const QString& path, paths) {
        QFileInfo fileInfo(QDir(path).filePath(executableName));
        if (fileInfo.exists() && fileInfo.isExecutable()) {
            return fileInfo.absoluteFilePath();
        }
    }
    return QString();
}

// -------------------- process --------------------
Action::ApplyStatus OpenExtensionInIDE::process() {
    QString camitkExtensionFile = getParameterValueAsString("CamiTK File");
    if (camitkExtensionFile.isEmpty() || !QFileInfo(camitkExtensionFile).exists()) {
        return ABORTED;
    }

    QString vscodePath = findExecutable("code");
    if (vscodePath.isEmpty()) {
        vscodePath = findExecutable("codium");
    }

    if (vscodePath.isEmpty()) {
        CAMITK_WARNING("Neither Visual Studio Code (code) nor VSCodium (codium) was found in the system PATH.");
        return ABORTED;
    }

    startIDECommand.setProgram(vscodePath);
    QStringList arguments;
    arguments << "--new-window"
              << QFileInfo(camitkExtensionFile).absolutePath();
    startIDECommand.setArguments(arguments);
    startIDECommand.setProcessChannelMode(QProcess::MergedChannels);
    startIDECommand.start();
    if (!startIDECommand.waitForStarted()) {
        CAMITK_WARNING(tr("Failed to start the code editor. Ensure it is installed and added to the PATH. Output:\n%1").arg(QString(startIDECommand.readAll())));
        return ABORTED;
    }
    startIDECommand.waitForFinished();
    return SUCCESS;
}

// -------------------- targetDefined --------------------
void OpenExtensionInIDE::targetDefined() {
}

// -------------------- parameterChanged --------------------
void OpenExtensionInIDE::parameterChanged(QString parameterName) {
}

