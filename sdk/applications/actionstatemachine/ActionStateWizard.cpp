/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// -- Qt dependencies
#include <QFileDialog>

// -- CamiTK Application Local stuff
#include "ActionStateWizard.h"

// ---------------------- constructor ----------------------------
ActionStateWizard::ActionStateWizard(QString inputFileName, QString outputDir) {
    // init GUI
    ui = new Ui::ActionStateWizard();
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Start CamiTK Action State Machine");

    connect(ui->browseSCXML, SIGNAL(clicked()), this, SLOT(selectSCXMLFilename()));
    connect(ui->browseLogDir, SIGNAL(clicked()), this, SLOT(selectLogDirectory()));

    SCXMLFileName = inputFileName;
    logDirectory = outputDir;

    initLogDirLabel();
    initSCXMLLabel();
}

// ---------------------- getSCXMLFilename ----------------------------
QString ActionStateWizard::getSCXMLFilename() {
    return SCXMLFileName;
}

// ---------------------- getLogDirectory ----------------------------
QString ActionStateWizard::getLogDirectory() {
    return logDirectory;
}

// ---------------------- selectLogDirectory ----------------------
void ActionStateWizard::selectLogDirectory() {
    logDirectory = QFileDialog::getExistingDirectory(this, tr("Please select a directory where to save log and component files"));
    initLogDirLabel();
}

// ---------------------- selectSCXMLFilename ----------------------
void ActionStateWizard::selectSCXMLFilename() {
    SCXMLFileName = QFileDialog::getOpenFileName(this, tr("Please select an xml file"));
    initSCXMLLabel();
}

// ---------------------- initSCXMLLabel ----------------------
void ActionStateWizard::initSCXMLLabel() {
    if (SCXMLFileName.isEmpty()) {
        ui->SCXMLLabel->setText("<font color=\"red\">No SCXML document: please select an XML file...</font>");
        ui->browseSCXMLLabel->setText("<font color=\"red\">Select SCXML document</font>");
    }
    else {
        ui->SCXMLLabel->setText(SCXMLFileName);
        ui->browseSCXMLLabel->setText("Modify SCXML document");
    }
    updateStartButtonState();
}

// ---------------------- initLogDirLabel ----------------------
void ActionStateWizard::initLogDirLabel() {
    if (logDirectory.isEmpty()) {
        ui->LogDirLabel->setText("<font color=\"red\">No output directory: please select a directory</font>");
        ui->browseLogDirLabel->setText("<font color=\"red\">Secect output directory</font>");
    }
    else {
        ui->LogDirLabel->setText(logDirectory);
        ui->browseLogDirLabel->setText("Modify output directory");
    }
    updateStartButtonState();
}

// ---------------------- updateStartButtonState ----------------------
void ActionStateWizard::updateStartButtonState() {
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!SCXMLFileName.isEmpty() && !logDirectory.isEmpty());
}