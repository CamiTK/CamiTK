/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ExtensionSummaryState.h"
#include "ExtensionSummaryWidget.h"

#include "WizardMainWindow.h"

// includes from coreschema
#include <ActionExtension.hxx>
#include <Actions.hxx>
#include <Action.hxx>
#include <Dependencies.hxx>
#include <ComponentExtension.hxx>
#include <Components.hxx>
#include <Component.hxx>
#include <ViewerExtension.hxx>
#include <Viewer.hxx>

ExtensionSummaryState::ExtensionSummaryState(QString name, ExtensionSummaryWidget* widget,
        QString type, WizardMainWindow* mainWindow, QState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->type = type;
    this->domActionExtension = nullptr;
    this->domComponentExtension = nullptr;
    this->domViewerExtension = nullptr;
}

void ExtensionSummaryState::setActionExtension(cepcoreschema::ActionExtension* domActionExtension) {
    if (this->type == "Action") {
        this->domActionExtension = domActionExtension;
        this->domComponentExtension = nullptr;
        this->domViewerExtension = nullptr;
    }
}

void ExtensionSummaryState::setComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension) {
    if (this->type == "Component") {
        this->domActionExtension = nullptr;
        this->domComponentExtension = domComponentExtension;
        this->domViewerExtension = nullptr;
    }

}

void ExtensionSummaryState::setViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension) {
    if (this->type == "Viewer") {
        this->domActionExtension = nullptr;
        this->domComponentExtension = nullptr;
        this->domViewerExtension = domViewerExtension;
    }
}

void ExtensionSummaryState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* extensionSummaryWidget = dynamic_cast<ExtensionSummaryWidget*>(widget);
    if (extensionSummaryWidget != nullptr) {
        QStringList elements;
        QStringList dependencies;

        if ((type == "Action") && (domActionExtension != nullptr)) {
            extensionSummaryWidget->setElement("Action");
            QString name = domActionExtension->name().c_str();
            QString description = domActionExtension->description().c_str();

            cepcoreschema::Actions::action_iterator act;
            for (act = domActionExtension->actions().action().begin(); act != domActionExtension->actions().action().end(); act++) {
                cepcoreschema::Action& theAction = (*act);
                QString actionName = theAction.name().c_str();
                elements << actionName;
            }

            if (domActionExtension->dependencies().present()) {
                cepcoreschema::Dependencies deps = domActionExtension->dependencies().get();
                cepcoreschema::Dependencies::dependency_iterator it;
                for (it = deps.dependency().begin(); it != deps.dependency().end(); it++) {
                    cepcoreschema::Dependency& dep = (*it);
                    QString dependency = dep.name().c_str();
                    dependencies << dependency;
                }
            }

            extensionSummaryWidget->setSummary(name, description, elements, dependencies);
        }

        else if ((type == "Component") && (domComponentExtension != nullptr)) {
            extensionSummaryWidget->setElement("Component");
            QString name = domComponentExtension->name().c_str();
            QString description = domComponentExtension->description().c_str();

            cepcoreschema::Components::component_iterator comp;
            for (comp = domComponentExtension->components().component().begin(); comp != domComponentExtension->components().component().end(); comp++) {
                cepcoreschema::Component& theComponent = (*comp);
                QString componentName = theComponent.name().c_str();
                elements << componentName;
            }

            if (domComponentExtension->dependencies().present()) {
                cepcoreschema::Dependencies deps = domComponentExtension->dependencies().get();
                cepcoreschema::Dependencies::dependency_iterator it;
                for (it = deps.dependency().begin(); it != deps.dependency().end(); it++) {
                    cepcoreschema::Dependency& dep = (*it);
                    QString dependency = dep.name().c_str();
                    dependencies << dependency;
                }
            }

            extensionSummaryWidget->setSummary(name, description, elements, dependencies);
        }

        else if ((type == "Viewer") && (domViewerExtension != nullptr)) {
            extensionSummaryWidget->setElement("Viewer");
            QString name = domViewerExtension->name().c_str();
            QString description = domViewerExtension->description().c_str();

            QString viewerName = domViewerExtension->viewer().name().c_str();
            elements << viewerName;

            if (domViewerExtension->dependencies().present()) {
                cepcoreschema::Dependencies deps = domViewerExtension->dependencies().get();
                cepcoreschema::Dependencies::dependency_iterator it;
                for (it = deps.dependency().begin(); it != deps.dependency().end(); it++) {
                    cepcoreschema::Dependency& dep = (*it);
                    QString dependency = dep.name().c_str();
                    dependencies << dependency;
                }
            }

            extensionSummaryWidget->setSummary(name, description, elements, dependencies);
        }

    }

}


void ExtensionSummaryState::onExit(QEvent* event) {
    WizardState::onExit(event);
}

