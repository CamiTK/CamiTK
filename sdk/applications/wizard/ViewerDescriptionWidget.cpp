/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

#include <ViewerType.hxx>

ViewerDescriptionWidget::ViewerDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault(defaultViewerExtensionName);
}

void ViewerDescriptionWidget::nextButtonClicked() {
    QString viewerName = ui.viewerNameItself->text();
    QString viewerDescription = ui.viewerGoalItself->toPlainText();
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (viewerName == viewerExtensionName) {
        ui.viewerNameStar->setStyleSheet(enhancedStyle);
        ui.viewerGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultViewerNameAndExtension);
    }
    else if (viewerName.toUtf8() != viewerName.toLatin1()) {
        ui.viewerNameStar->setStyleSheet(enhancedStyle);
        ui.viewerGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((viewerName.isEmpty()) || (viewerName == defaultViewerName)) {
        ui.viewerNameStar->setStyleSheet(enhancedStyle);
        ui.viewerGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealViewerName);
    }
    else if (viewerDescription.toUtf8() != viewerDescription.toLatin1()) {
        ui.viewerNameStar->setStyleSheet(normalStyle);
        ui.viewerGoalStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((viewerDescription.isEmpty()) || (viewerDescription == defaultViewerDescription)) {
        ui.viewerNameStar->setStyleSheet(normalStyle);
        ui.viewerGoalStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealViewerDescription);
    }
    else {
        emit next();
    }
#else
    emit next();
#endif
}

void ViewerDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}


QString ViewerDescriptionWidget::getViewerName() {
    return ui.viewerNameItself->text();
}

QString ViewerDescriptionWidget::getViewerDescription() {
    return ui.viewerGoalItself->toPlainText();
}

cepcoreschema::ViewerType ViewerDescriptionWidget::getType() {
    if (ui.radioButton_embedded->isChecked()) {
        return cepcoreschema::ViewerType::EMBEDDED;
    }
    else {
        return cepcoreschema::ViewerType::DOCKED;
    }
}

void ViewerDescriptionWidget::setToDefault(QString viewerExtensionName) {

    this->viewerExtensionName = viewerExtensionName;

    ui.viewerNameItself->setText(defaultViewerName);
    ui.viewerGoalItself->setPlainText(defaultViewerDescription);
    ui.viewerNameStar->setStyleSheet(normalStyle);
    ui.viewerGoalStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
}

void ViewerDescriptionWidget::setName(QString name) {
    ui.viewerNameItself->setText(name);
}

void ViewerDescriptionWidget::setDescription(QString description) {
    ui.viewerGoalItself->setPlainText(description);
}