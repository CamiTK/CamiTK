/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERDESCRIPTIONSTATE_H
#define VIEWERDESCRIPTIONSTATE_H

#include "WizardState.h"

class ViewerDescriptionWidget;
class ViewerCreationState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Viewer;
class ViewerExtension;
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to describe action
 *
 */
class ViewerDescriptionState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewerDescriptionState(QString name, ViewerDescriptionWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ViewerCreationState* parent);

    /**  Destructor */
    ~ViewerDescriptionState() override = default;

    void resetDomViewer(cepcoreschema::Viewer* domViewer, cepcoreschema::ViewerExtension* domViewerExtension);

protected:
    void onExit(QEvent* event) override;
    void onEntry(QEvent* event) override;

private:
    // Dom element that will be completed with the name
    cepcoreschema::Viewer* domViewer;
    // Just to check that the name of the Action is not the same of the Action Extension
    cepcoreschema::ViewerExtension* domViewerExtension;
    // To know which Components are available within the CEP (ot possibly apply the Action on).
    cepcoreschema::Cep* domCep;

};
#endif
