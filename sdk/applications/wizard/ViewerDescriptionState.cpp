/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerDescriptionState.h"
#include "ViewerDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "ViewerCreationState.h"

// includes from coreschema
#include <Viewer.hxx>
#include <ViewerExtension.hxx>
#include <ItkFilter.hxx>
#include <Cep.hxx>
#include <Components.hxx>

// includes from cepgenerator
#include <ClassNameHandler.h>

// temporary
#include <iostream>

ViewerDescriptionState::ViewerDescriptionState(QString name, ViewerDescriptionWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ViewerCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domViewer = nullptr;
    this->domViewerExtension = nullptr;
    this->domCep = domCep;
}

void ViewerDescriptionState::resetDomViewer(cepcoreschema::Viewer* domViewer, cepcoreschema::ViewerExtension* domViewerExtension) {
    this->domViewer = domViewer;
    this->domViewerExtension = domViewerExtension;
    auto* viewerDescriptionWidget = dynamic_cast<ViewerDescriptionWidget*>(widget);
    if (viewerDescriptionWidget) {
        QString viewerExtensionName = domViewerExtension->name().c_str();
        viewerDescriptionWidget->setToDefault(viewerExtensionName);
    }
}

void ViewerDescriptionState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* viewerDescriptionWidget = dynamic_cast<ViewerDescriptionWidget*>(widget);
    if (viewerDescriptionWidget) {
        QString ViewerExtensionName = domViewerExtension->name().c_str();
        viewerDescriptionWidget->setToDefault(ViewerExtensionName);
        if ((domViewer != NULL) && (QString("A Viewer") != QString(domViewer->name().c_str()))) {
            viewerDescriptionWidget->setName(QString(domViewer->name().c_str()));
            viewerDescriptionWidget->setDescription(QString(domViewer->description().c_str()));
        }

        // Check existing CEP viewers
        // CS: problem to get viewers name.
        // The loop was under viewer extension, the corresction is to check viewer for each viewerExtension.
        if (domCep->viewerExtensions().present()) {
            cepcoreschema::ViewerExtensions::viewerExtension_sequence cepViewerExtensions = domCep->viewerExtensions().get().viewerExtension();
            cepcoreschema::ViewerExtensions::viewerExtension_iterator itCompExt;
            // Loop on viewerExtensions
            for (itCompExt = cepViewerExtensions.begin(); itCompExt != cepViewerExtensions.end(); itCompExt++) {
                cepcoreschema::Viewer comp = itCompExt->viewer();
                QString viewerName = comp.name().c_str();
                viewerName = ClassNameHandler::getClassName(viewerName);
            }
        }
    }

}

void ViewerDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* viewerDescriptionWidget = dynamic_cast<ViewerDescriptionWidget*>(widget);

    if (viewerDescriptionWidget) {
        QString viewerName = viewerDescriptionWidget->getViewerName();
        QString viewerDescription = viewerDescriptionWidget->getViewerDescription();
        cepcoreschema::ViewerType type = viewerDescriptionWidget->getType();

        domViewer->name(viewerName.toStdString());
        domViewer->description(viewerDescription.toStdString());
        domViewer->type(type);
    }

    domViewerExtension->viewer(*domViewer);
}
