/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerExtensionDependenciesState.h"
#include "WizardMainWindow.h"
#include "ViewerExtensionCreationState.h"
#include "DependenciesWidget.h"

// includes from coreschema
#include <ViewerExtension.hxx>
#include <Viewer.hxx>

ViewerExtensionDependenciesState::ViewerExtensionDependenciesState(QString name, DependenciesWidget* widget, WizardMainWindow* mainWindow,  cepcoreschema::Cep* domCep, ViewerExtensionCreationState* parent)
    : DependenciesState(name, widget, mainWindow, domCep, parent) {
    this->domViewerExtension = nullptr;
    widget->setElement("Viewer Extension");
}

void ViewerExtensionDependenciesState::resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension) {
    this->domViewerExtension = domViewerExtension;
    auto* dependenciesWidget = dynamic_cast<DependenciesWidget*>(widget);
    if (dependenciesWidget != nullptr) {
        dependenciesWidget->setToDefault();
    }
}

void ViewerExtensionDependenciesState::onEntry(QEvent* event) {
    DependenciesState::onEntry(event);
}


void ViewerExtensionDependenciesState::onExit(QEvent* event) {
    DependenciesState::onExit(event);
    cepcoreschema::Dependencies* domDeps = getDependencies();
    if (domDeps != nullptr) {
        if (domDeps->dependency().size() > 0) {
            domViewerExtension->dependencies((*domDeps));
        }
    }
}
