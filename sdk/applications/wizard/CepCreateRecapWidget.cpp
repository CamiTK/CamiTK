/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepCreateRecapWidget.h"

static const QString defaultExistingActionExtensions = "\
<html>\
    <head/>\
    <body>\
        <p align='center'><span style=' font-weight:600; '>Created Action Extensions:</span></p>\
        <ul>\
        </ul>\
    </body>\
</html>";

static const QString defaultExistingViewerExtensions = "\
<html>\
    <head/>\
    <body>\
        <p align='center'><span style=' font-weight:600; '>Created Viewer Extensions:</span></p>\
        <ul>\
        </ul>\
    </body>\
</html>";

static const QString defaultExistingComponentExtensions = "\
<html>\
<head/>\
<body>\
<p align='center'><span style=' font-weight:600; '>Created Component Extensions:</span></p>\
<ul>\
</ul>\
</body>\
</html>";

static const QString defaultExistingLibraries = "\
<html>\
<head/>\
<body>\
<p align='center'><span style=' font-weight:600; '>Created Libraries:</span></p>\
<ul>\
</ul>\
</span>\
</body>\
</html>";


CepCreateRecapWidget::CepCreateRecapWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    createdLibrariesString = defaultExistingLibraries;
    createdActionsString = defaultExistingActionExtensions;
    createdViewersString = defaultExistingViewerExtensions;
    createdComponentsString = defaultExistingComponentExtensions;
}

void CepCreateRecapWidget::nextButtonClicked() {
    emit next();
}

void CepCreateRecapWidget::setNameItself(QString name) {
    ui.cepNameItself->setText(name);
}

void CepCreateRecapWidget::setDescriptionItself(QString description) {
    ui.cepDescriptionItself->setText(description);
}

void CepCreateRecapWidget::setContactItself(QString contact) {
    ui.cepContactItself->setText(contact);
}

void CepCreateRecapWidget::addActionExtensionClicked() {
    emit addActionExtension();
}

void CepCreateRecapWidget::addViewerExtensionClicked() {
    emit addViewerExtension();
}

void CepCreateRecapWidget::addComponentExtensionClicked() {
    emit addComponentExtension();
}

void CepCreateRecapWidget::addLibrariesClicked() {
    emit addLibrary();
}

void CepCreateRecapWidget::emptyExistingActionExtensions() {
    createdActionsString = defaultExistingActionExtensions;
    ui.existingActionExtensions->setHtml(createdActionsString);
}

void CepCreateRecapWidget::emptyExistingViewerExtensions() {
    createdViewersString = defaultExistingViewerExtensions;
    ui.existingViewerExtensions->setHtml(createdViewersString);
}

void CepCreateRecapWidget::emptyExistingComponentExtensions() {
    createdComponentsString = defaultExistingComponentExtensions;
    ui.existingComponentExtensions->setHtml(createdComponentsString);
}

void CepCreateRecapWidget::emptyExistingLibraries() {
    createdLibrariesString = defaultExistingLibraries;
    ui.existingLibrariesTextEdit->setHtml(createdLibrariesString);
}

void CepCreateRecapWidget::addActionExtension(QString actionExtensionName) {
    QString toBeInserted = "<li>" + actionExtensionName + "</li>\n";
    int index = createdActionsString.lastIndexOf("</ul>");

    createdActionsString.insert(index, toBeInserted);
    ui.existingActionExtensions->setHtml(createdActionsString);
}

void CepCreateRecapWidget::addViewerExtension(QString viewerExtensionName) {
    QString toBeInserted = "<li>" + viewerExtensionName + "</li>\n";
    int index = createdViewersString.lastIndexOf("</ul>");

    createdViewersString.insert(index, toBeInserted);
    ui.existingViewerExtensions->setHtml(createdViewersString);
}

void CepCreateRecapWidget::addComponentExtension(QString componentExtensionName) {
    QString toBeInserted = "<li>" + componentExtensionName + "</li>\n";
    int index = createdComponentsString.lastIndexOf("</ul>");

    createdComponentsString.insert(index, toBeInserted);
    ui.existingComponentExtensions->setHtml(createdComponentsString);
}

void CepCreateRecapWidget::addLibrary(QString libraryNamme) {
    QString toBeInserted = "<li>" + libraryNamme + "</li>\n";
    int index = createdLibrariesString.lastIndexOf("</ul>");
    createdLibrariesString.insert(index, toBeInserted);
    ui.existingLibrariesTextEdit->setHtml(createdLibrariesString);

}
