/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerSummaryWidget.h"

#include "ViewerType.hxx"

#include "DefaultGUIText.h"

ViewerSummaryWidget::ViewerSummaryWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ViewerSummaryWidget::setToDefault() {
    ui.summaryLabel->setText("");
}

void ViewerSummaryWidget::nextButtonClicked() {
    emit next();
}

void ViewerSummaryWidget::cancelButtonClicked() {
    emit cancel();
}

void ViewerSummaryWidget::previousButtonClicked() {
    emit previous();
}

void ViewerSummaryWidget::setSummary(QString name, QString description, cepcoreschema::ViewerType type) {
    QString text = defaultViewerSummary;

    text = text.replace(QRegExp("@NAME@"), name);
    text = text.replace(QRegExp("@DESCRIPTION@"), description);

    if (type == cepcoreschema::ViewerType::EMBEDDED) {
        text = text.replace(QRegExp("@TYPE@"), "EMBEDDED");
    }
    else {
        text = text.replace(QRegExp("@TYPE@"), "DOCKED");
    }

    ui.viewerSummaryLabel->setText(text);
}




