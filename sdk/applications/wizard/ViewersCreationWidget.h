/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERSCREATIONWIDGET_H
#define VIEWERSCREATIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_ViewersCreationWidget.h"

// includes from Qt
#include <QWidget>
#include <QStringList>

#include <ViewerExtension.hxx>
#include <Viewer.hxx>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to create actions
 *
 */
class ViewersCreationWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewersCreationWidget(QWidget* parent);

    void resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension);

    /**  Destructor */
    ~ViewersCreationWidget() override = default;

    void setToDefault();

    void setGroupBoxTitle(QString text);
    void emptyExistingViewers();
    void addViewerName(QString viewerName);

    bool isViewerRegistered(QString viewerName);

public slots:
    virtual void registerDefaultViewerClicked();
    virtual void registerNewViewerClicked();
    virtual void nextButtonClicked();
    virtual void previousButtonClicked();
    virtual void cancelButtonClicked();

signals:
    void next();
    void cancel();
    void previous();
    void newDefaultViewer();

private:
    QStringList createdViewers;
    QString createdViewersString;

    cepcoreschema::ViewerExtension* domViewerExtension;

    Ui::ViewersCreationWidget ui;
};
#endif
