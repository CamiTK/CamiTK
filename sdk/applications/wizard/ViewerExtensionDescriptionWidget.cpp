/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerExtensionDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>


ViewerExtensionDescriptionWidget::ViewerExtensionDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ViewerExtensionDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}

void ViewerExtensionDescriptionWidget::nextButtonClicked() {
    QString viewerExtensionName = ui.viewerExtensionNameItself->text();
    QString viewerExtensionDescription = ui.viewerExtensionDescriptionItself->toPlainText();

#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (viewerExtensionName.toUtf8() != viewerExtensionName.toLatin1()) {
        ui.viewerExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.viewerExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((viewerExtensionName.isEmpty()) || (viewerExtensionName == defaultViewerExtensionName)) {
        ui.viewerExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.viewerExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionName);
    }
    else if (viewerExtensionDescription.toUtf8() != viewerExtensionDescription.toLatin1()) {
        ui.viewerExtensionNameStar->setStyleSheet(normalStyle);
        ui.viewerExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((viewerExtensionDescription.isEmpty()) || (viewerExtensionDescription == defaultViewerExtensionDescription)) {
        ui.viewerExtensionNameStar->setStyleSheet(normalStyle);
        ui.viewerExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionDescription);
    }
    else {
        ui.viewerExtensionNameStar->setStyleSheet(normalStyle);
        ui.viewerExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif
}

void ViewerExtensionDescriptionWidget::setToDefault() {
    ui.explanationLabel->setText(defaultViewerExtensionExplanation);
    ui.viewerExtensionNameItself->setText(defaultViewerExtensionName);
    ui.viewerExtensionDescriptionItself->setPlainText(defaultViewerExtensionDescription);
    ui.viewerExtensionNameStar->setStyleSheet(normalStyle);
    ui.viewerExtensionDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
}

QString ViewerExtensionDescriptionWidget::getViewerExtensionName() {
    return ui.viewerExtensionNameItself->text();
}

QString ViewerExtensionDescriptionWidget::getCepDescription() {
    return ui.viewerExtensionDescriptionItself->toPlainText();
}


