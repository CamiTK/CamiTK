/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERSUMMARYWIDGET_H
#define VIEWERSUMMARYWIDGET_H

// Include GUI  automatically generated file
#include "ui_ViewerSummaryWidget.h"

// includes from Qt
#include <QWidget>
#include <QStringList>

namespace cepcoreschema {
class Viewer;
class ViewerExtension;
class ViewerType;
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to summarize the created action
 *
 */
class ViewerSummaryWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewerSummaryWidget(QWidget* parent);

    /**  Destructor */
    ~ViewerSummaryWidget() override = default;

    void setToDefault();
    void setSummary(QString name, QString description, cepcoreschema::ViewerType type);

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();
    virtual void previousButtonClicked();

signals:
    void next();
    void previous();
    void cancel();

private:
    Ui::ViewerSummaryWidget ui;

};
#endif
