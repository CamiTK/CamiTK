#--------------------------------------------
#
#         test actions 
#
#--------------------------------------------
camitk_application( NO_GUI
                    ADDITIONAL_SOURCES CommandLineOptions.cxx CommandLineOptions.hxx
                    CEP_NAME SDK
                    DESCRIPTION "Test application to test actions" 
)

# CDash subproject listing
set(CAMITK_TARGETS ${CAMITK_TARGETS} application-testactions CACHE INTERNAL "")
                  
#---------
# Testing
#----------
# Define the test basename
set(APPLICATION_TEST_ACTION ${APPLICATION_TARGET_NAME} CACHE INTERNAL "")#CACHE INTERNAL extends the scope of the variable outside the directory

#---------------------------------
# Testing command-line robustness
#---------------------------------
set(TEST_BASENAME ${APPLICATION_TARGET_NAME})
camitk_init_test(${TEST_BASENAME})
# should pass because invoking testactions without arguments or with help arg shows usage and exit success
camitk_add_test(PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")
camitk_add_test(EXECUTABLE_ARGS "--help" PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")
camitk_add_test(EXECUTABLE_ARGS "-h"     PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

# should pass because invoking testactions with a faulty arguments results in printing
# an "Argument error" message (and exit failure)
camitk_add_test(EXECUTABLE_ARGS "-badarg" 
                PASS_REGULAR_EXPRESSION "unknown option '-badarg'"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-a missingtestfile" 
                PASS_REGULAR_EXPRESSION "Argument error: please provide a component test file to work with."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-i missingactionextension" 
                PASS_REGULAR_EXPRESSION "Argument error: please provide an action dll/so file"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-a myactionextension -i anotherbadarg -badarg" 
                PASS_REGULAR_EXPRESSION "unknown option '-badarg'"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-a myactionextension -i badcomponentfile" 
                PASS_REGULAR_EXPRESSION "Argument error: component test file \"badcomponentfile\" does not exist."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-a badactionextension -i ${PROJECT_SOURCE_DIR}/sdk/components/vtkmesh/testdata/simple.vtk" 
                PASS_REGULAR_EXPRESSION "Argument error: action dll/so file \"badactionextension\" does not exist."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

