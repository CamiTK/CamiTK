# XSD really needed
find_package(XSD REQUIRED)

# Create lib files from xsd
set(monitorLib_XSD
    ${CMAKE_CURRENT_SOURCE_DIR}/MonitoringModel.xsd
    ${CMAKE_CURRENT_SOURCE_DIR}/MonitorIn.xsd
    ${CMAKE_CURRENT_SOURCE_DIR}/MonitorOut.xsd
)

# accumulate all headers
unset(monitoringLib_H)
unset(monitoringLib_SRCS)

# generates headers from scratch
foreach(xsdFile ${monitorLib_XSD})
    get_filename_component(xsdFileName ${xsdFile} NAME_WE)
    message(STATUS "Generating code from ${xsdFileName}")
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.hxx ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.cxx
                    COMMAND ${XSD_EXECUTABLE}
                    ARGS cxx-tree ${XSD_CXX_STD_FLAG} --generate-polymorphic --generate-serialization --namespace-map =mml --polymorphic-type Method --polymorphic-type Criteria ${xsdFile}
                    DEPENDS ${xsdFile})
    set(monitoringLib_H ${monitoringLib_H} ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.hxx)
    set(monitoringLib_SRCS ${monitoringLib_SRCS} ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.cxx)
endforeach()

# add lib as a camitk tool
# force to mmlschema (default should be schema as it is in the schema dir, but schema is too generic!)
camitk_library(STATIC
               SOURCES ${monitoringLib_SRCS}
               NEEDS_XSD
               HEADERS_TO_INSTALL ${monitoringLib_H}
               CEP_NAME CEP_MODELING
               DESCRIPTION "Data description of MML: Monitoring biomechanical simulation Markup Language"
)
