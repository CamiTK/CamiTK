project(LML)

# Is this tool is compiled inside or outside CamiTK (as a standalone)?
if (NOT CEP_NAME STREQUAL "modeling")
  # if this tool is compiled independently from CamiTK
  set(LML_STANDALONE ON CACHE BOOL "ON only if LML compiled outside CAMITK SDK")
  message(FATAL_ERROR "Compilation of LML outside CamiTK SDK not implemented yet. Please contact authors")
endif()

# Version (in CamiTK)
set(VERSION_STRING "lml - ${CAMITK_VERSION_STRING} (c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)")
configure_file(${LML_SOURCE_DIR}/LoadsVersion.h.in ${LML_BINARY_DIR}/LoadsVersion.h)

set( HEADERS 
      Direction.h 
      Force.h 
      ForceUnit.h 
      Load.h 
      Loads.h 
      ${LML_BINARY_DIR}/LoadsVersion.h
      Pressure.h 
      PressureUnit.h 
      Rotation.h 
      RotationUnit.h 
      TargetList.h 
      Translation.h 
      TranslationUnit.h
      Acceleration.h
      AccelerationUnit.h
      Unit.h 
      ValueEvent.h
    )

set( SRCS
      Force.cpp 
      ForceUnit.cpp 
      Load.cpp 
      Loads.cpp 
      PressureUnit.cpp 
      RotationUnit.cpp 
      TargetList.cpp 
      Translation.cpp 
      TranslationUnit.cpp
      AccelerationUnit.cpp
      ValueEvent.cpp
      ${HEADERS}
    )

camitk_library(STATIC
               SOURCES ${SRCS}
               NEEDS_XSD
               NEEDS_CEP_LIBRARIES lmlschema
               HEADERS_TO_INSTALL ${HEADERS}
               CEP_NAME CEP_MODELING
               DESCRIPTION "Tools to describe loads and boundary condition of biomechanical simulation."
)

#-------------------
# api documentation
#-------------------

option(APIDOC_GENERATE_FOR_LML "Build API documentation for LML" OFF)

if(APIDOC_GENERATE_FOR_LML)
  find_package(Doxygen REQUIRED)
  if(DOXYGEN)
      add_subdirectory(api-doc)
  else()
      MESSAGE(STATUS "WARNING: Doxygen not found - LML API documentation and reference manual will not be created")
  endif()
endif()


