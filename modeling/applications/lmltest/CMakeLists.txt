camitk_application(NO_GUI
				   CEP_NAME CEP_MODELING
                   NEEDS_XSD
                   NEEDS_XERCESC
                   NEEDS_CEP_LIBRARIES lml lmlschema
                   DESCRIPTION "Provides a command line integration test tool for the lml library"
)


#----------
# Testing
#----------
# TODO compare output csv with a static output csv (that it should produce)
camitk_init_test(${APPLICATION_TARGET_NAME})
# should pass because lmltest run with no arguments shows usage and exit success
camitk_add_test(PROJECT_NAME ${APPLICATION_TARGET_NAME} TEST_SUFFIX "-")

# should pass because invoking lmltest with a faulty arguments results in printing
# an "Argument error" message (and exit failure)
camitk_add_test(EXECUTABLE_ARGS "-error" 
                PASS_REGULAR_EXPRESSION "Argument errors: "
                PROJECT_NAME ${APPLICATION_TARGET_NAME} TEST_SUFFIX "-")

# should pass because reading test.lml should not modify it (input == output)
if(NOT WIN32) #this test fails on windows7 VM due to missing tweek for -debug
    camitk_add_test(EXECUTABLE_ARGS "-i ${CMAKE_CURRENT_SOURCE_DIR}/test.lml"
                    PASS_FILE ${CMAKE_CURRENT_SOURCE_DIR}/test.lml 
                    PROJECT_NAME ${APPLICATION_TARGET_NAME} TEST_SUFFIX "-")
endif()
