camitk_application(NO_GUI)

#---------------------------------
# Testing nogui application
#---------------------------------
set(TEST_BASENAME ${APPLICATION_TARGET_NAME})
camitk_init_test(${TEST_BASENAME})
# should pass because invoking cepgenerator without arguments or with help arg shows usage and exit success
camitk_add_test(PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-"
                PASS_REGULAR_EXPRESSION "Resampled image succesfully saved to .*-output.mha")
