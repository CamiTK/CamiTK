/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Application Simple stuff
#include "SimpleMainWindow.h"

// -- Core stuff
#include <Application.h>
#include <InteractiveViewer.h>
#include <Log.h>

// -- QT stuff
#include <QToolBar>
#include <QKeyEvent>

// ------------- constructor -----------------
SimpleMainWindow::SimpleMainWindow() : MainWindow("Simple Demo") {

    // only add a main viewers
    Viewer* medicalImageViewer = Application::getViewer("Medical Image Viewer");

    if (medicalImageViewer != nullptr) {
        setCentralViewer(medicalImageViewer);
    }
    else {
        CAMITK_ERROR(tr("Cannot find \"Medical Image Viewer\". This viewer is mandatory for running camitk-simple."))
    }

    // all black like if they were old-school OpenGL viewers
    InteractiveViewer* default3DViewer = dynamic_cast<InteractiveViewer*>(Application::getViewer("3D Viewer"));

    if (default3DViewer != nullptr) {
        default3DViewer->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_A, Qt::NoModifier)); // simulate "a" stroke
        default3DViewer->setBackgroundColor(Qt::black);
        default3DViewer->setGradientBackground(false);
    }
    else {
        CAMITK_ERROR(tr("Cannot find \"3D Viewer\". This viewer is mandatory for running camitk-simple."))
    }

    // hide slide bar and modify the color and background of default slice viewers
    QStringList viewerNames;
    viewerNames << "Axial Viewer" << "Sagittal Viewer" << "Coronal Viewer";
    for (auto& viewerName : qAsConst(viewerNames)) {
        InteractiveViewer* sliceViewer = dynamic_cast<InteractiveViewer*>(Application::getViewer(viewerName));

        if (sliceViewer != nullptr) {
            sliceViewer->setBackgroundColor(Qt::black);
            sliceViewer->setGradientBackground(false);
        }
        else {
            CAMITK_ERROR(tr("Cannot find \"%1\". This viewer is mandatory for running camitk-fancy.").arg(viewerName))
        }
    }
}

// ------------- aboutToShow -----------------
void SimpleMainWindow::aboutToShow() {
    // remove all but the main viewer
    Application::getViewer("3D Viewer")->getToolBar()->hide();
    camitk::MainWindow::aboutToShow();
}



