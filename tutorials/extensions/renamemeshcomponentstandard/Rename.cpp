/*****************************************************************************
 * 
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 ****************************************************************************/
#include "Rename.h"

// CamiTK includes
#include <Property.h>
#include <Log.h>
#include <MeshComponent.h>

using namespace camitk;

// -------------------- init --------------------
void Rename::init() {
    // nothing special to do here
}

// -------------------- process --------------------
Action::ApplyStatus Rename::process() {
    for(Component* selected: getTargets()) {
        MeshComponent* input = dynamic_cast <MeshComponent*>(selected);
        input->setName(getParameterValueAsString("New Name"));
    }

    Application::refresh();
    return SUCCESS;
}

// -------------------- targetDefined --------------------
void Rename::targetDefined() {
    for(Component* selected: getTargets()) {
        MeshComponent* input = dynamic_cast <MeshComponent*>(selected);
        setParameterValue("New Name", input->getName());
    }
}

// -------------------- parameterChanged --------------------
void Rename::parameterChanged(QString parameterName) {
}

// -------------------- getUI --------------------
QWidget* Rename::getUI() {
    // If you want don't want to use the default
    // action widget and create your own action's widget,
    // then fill up this method
    //
    // Note: this method will only be called once,
    // use signal/slot to update your widget.
    //
    return nullptr;
}