/*****************************************************************************
 *
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ****************************************************************************/

//
// Generated source for action "Rename Mesh"
// 2024-05-24 - 08:05:25
//
// CamiTK includes
#include <Action.h>

// This action process MeshComponent, include is required
#include <MeshComponent.h>

using namespace camitk;

CamiTKActionImplementation(RenameMesh) {

    // -------------------- process --------------------
    Action::ApplyStatus process(Action * self) {
        MeshComponent* input = dynamic_cast <MeshComponent*>(self->getTargets().last());
        input->setName(self->getParameterValueAsString("New Name"));

        Application::refresh();
        return Action::SUCCESS;
    }

    // -------------------- targetDefined --------------------
    void targetDefined(Action * self) {
        MeshComponent* input = dynamic_cast <MeshComponent*>(self->getTargets().last());
        self->setParameterValue("New Name", "" + input->getName());
    }

}