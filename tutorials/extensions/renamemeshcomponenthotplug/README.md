# Test hot plug / dev action

## Cleanup / Start from scratch

- in this directory: remove `.vscode`, `.gitignore`, `build`, `FindCamiTK.cmake`, `CMakeLists.txt` and `actions/CMakeLists.txt`

```bash
rm -rf .vscode .gitignore build FindCamiTK.cmake CMakeLists.txt actions/CMakeLists.txt
```

## Test 1: CamiTK file visualization, initial file generation and registration

- Run `camitk-imp`
- Click on menu `Dev` → `Open Extension File`
- Open [`rename-mesh-component-modern.camitk`](rename-mesh-component-modern.camitk)
- In the new `ExtensionGeneratorPresenter` that should be opened, check that
  - Type is `HotPlug`
  - There is one action `Rename Mesh` that has component set to `MeshComponent` and one parameter name "New Name" of type QString and default value `""`
- Click on `Initialize`
- In the CMake Project Manager dialog:
  - Check that there are two stages: `Check System` and `Generate Source Files`
  - Click on Start
    - Both stages should perform without error (a tick mark should be displayed in from of the stage names)
    - Click on `Check System`: the last line should be `[OK] System checked successfully.` Check that `CamiTK file` and `CamiTK bin directory` are correct (the later should be set to the same directory as `camitk-imp` and `camitk-extensionmanager` applications)
    - Click on `Generate Source File`, it should show that:
      - `FindCamiTK.cmake`, `.gitignore`, `.vscode/launch.json`, top-level `CMakeLists.txt`, `actions CMakeLists.txt` are generated
      - User source code for action "Rename Mesh" (RenameMesh.cpp) was **not** overwritten
      - `Standard Error:` should be empty
  - Check that the `build` directory was created in the same directory as `rename-mesh-component-modern.camitk` but is empty
  - Click on Close
- Back to the `ExtensionGeneratorPresenter` dialog, click on `Register`
  - A warning dialog should pop up asking if you want to rebuild the extension
  > Action extension "Rename Action Extension (HotPlug)" is registered, but an error occurred while loading the defined action (Rename Mesh).
  > HotPlug action implementation libraries could not be loaded.
  > Do you want to rebuild the extension now?
  - Click on "Yes"
  - Back to the `ExtensionGeneratorPresenter` dialog, the second button from the left at the bottom of the dialog should now be enabled and show `Unregister`
  - Click on `Close`
- In the `Dev` menu, a new menu the `Rename Action Extension (HotPlug)` should appear with a tick mark.

## Test 2: unregister

- Click on menu `Dev` → `Rename Action Extension (HotPlug)` → `Unregister`, this should restart the application after asking you to confirm restart
- In the `Dev`, the `Rename Action Extension (HotPlug)` should have disappeared

## Test 3: Registration from the menu and menu tests

- Click on menu `Dev` → `Register Extension`
- Select [`rename-mesh-component-modern.camitk`](rename-mesh-component-modern.camitk)
- In the `Dev`, a new menu the `Rename Action Extension (HotPlug)` should have reappeared, also with a tick mark.
- Click on menu `Dev` → `Rename Action Extension (HotPlug)` → `Edit Extension`, the `ExtensionGeneratorPresenter` dialog should reopen with the extension information. The first button on the left at the bottom of the dialog should now display `Update`
- Click on `Update`, the second button from the left at the bottom of the dialog should now be enabled and show `Unregister`
- Click on `Close`
- Click on menu `Dev` → `Rename Action Extension (HotPlug)` → `Open In IDE`, a new _Visual Studio Code_ application should open

## Test 3: Code modification (and hot plug magic revealed)

!!! note
    If your default CMake Generator is defined globally as "Ninja" generator, you need to put it back to the system default value:
    - either
      -  In _Visual Studio Code_  use `Ctrl+Shift+P` and run the `Open Workspace Settings` command,
      - search for "cmake generator" and change the empty value to "Unix Makefiles", 
    - or just create a `.vscode/settings.json` with the following configuration
    ```.json
    {
    "cmake.generator": "Unix Makefiles"
    }
    ```
    Needed as the default CMake Project Manager use the default CMake Generator, which is set to "Unix Makefiles" on Linux.

- In `camitk-imp`
  - Open a mesh component
  - Run the `Rename Mesh` action (in the `Edit` family)
  - Enter a new name in the corresponding parameter, click on Apply
  - The component should be renamed accordingly

- In `actions/RenameMesh.cpp`, modify line 43 so that it shows:
```cpp
input->setName(self->getParameterValueAsString("New Name"));
```
and save the file.
- Click on menu `Dev` → `Rename Action Extension (HotPlug)` → `Rebuild and Reload Extension`
  - A new dialog should open, asking
  > Do you want to rebuild before reloading?
  - Click on "Yes"
  - The extension is rebuilt, and another dialog should open, asking
  > Extension reload requires restart
  - Click on "Yes"
- Open a mesh component and try the `Edit` → `Rename Mesh` action, the 🙂 should be added to the given new name
- In _Visual Studio Code_, undo the modification and revert back to the original source code

```cpp
input->setName(self->getParameterValueAsString("New Name"));
```

- In _Visual Studio Code_, run `CMake: Build` (or type on F7)
- In `camitk-imp`, just click on "Apply" of the `Rename Mesh` action (without restarting, reopening the mesh component or even switching to another action)
- magic: the C++ source code modification was taken into account!

## Cleanup

- when/if everything goes according to plans, just clean up the generated 
- in this directory: remove `.vscode`, `.gitignore`, `build`, `FindCamiTK.cmake`, `CMakeLists.txt` and `actions/CMakeLists.txt`

```bash
rm -rf .vscode .gitignore build FindCamiTK.cmake CMakeLists.txt actions/CMakeLists.txt
```