/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MIXED_VIEWER_H
#define MIXED_VIEWER_H

#include "MixedViewerAPI.h"

// -- Core stuff
#include <Viewer.h>

// -- QT stuff
#include <QVBoxLayout>

/**
  *    A dual panel viewer...
  *    It just send reused the axial and 3D viewer to display two views of the same thing.
  *
  *    All the heavy work is already done by the MedicalImageViewer:
  *    - view synchronization
  *    - update and refresh of the viewer
  *    - management of viewed components
  *
  *    This viewer is therefore very simple. In the end it is just building a specific widget
  *    that presents the axial and 3D viewer side by side using a grid layout.
  */
class MIXED_VIEWER_API MixedViewer : public camitk::Viewer {
    Q_OBJECT

public:

    /** @name Constructors/Destructors
      */
    /// @{
    /** construtor. */
    Q_INVOKABLE MixedViewer(QString name);

    /** destructor */
    virtual ~MixedViewer();
    /// @}

    /** @name Viewer inherited
      */
    /// @{
    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the viewer widget.
    virtual QWidget* getWidget() override;
    /// @}

private:

    /// The viewer's widget
    QWidget* myWidget;

    /// The two panels inside the main layout
    QVBoxLayout* leftLayout;
    QVBoxLayout* rightLayout;

};

#endif // MIXED_VIEWER_H

