# a component that cannot be instantiate (only use this for test purpose)
camitk_extension(COMPONENT_EXTENSION
                 ENABLE_AUTO_TEST
                 # save is not implemented (as opened cannot be performed!), just do level 1 test
                 AUTO_TEST_LEVEL 1
)

# --------------------------------
# --- Specific test management ---
# --------------------------------
# this is a guaranted test failure. Therefore test fail is correct.
set_tests_properties(component-abort-level1-1 PROPERTIES WILL_FAIL true)
