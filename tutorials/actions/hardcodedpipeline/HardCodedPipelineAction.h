/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONSINACTIONACTION_H
#define ACTIONSINACTIONACTION_H

#include <Action.h>

#include <ImageComponent.h>

class HardCodedPipelineAction : public camitk::Action {
    Q_OBJECT
public:

    /// Default Constructor
    HardCodedPipelineAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~HardCodedPipelineAction();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ComputeNeighborValueAction (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply();

private:
    /// many things can happen during the execution of this action
    enum PipelineActionStatus {
        RESAMPLE_NOT_FOUND,
        OTSU_NOT_FOUND,
        RESAMPLE_APPLY_ERROR,
        OTSU_APPLY_ERROR,
        OK,
    };

    /// Get the last toplevel output component of a given action
    /// This is required in some rare specific pipeline execution environment
    ///
    /// Knowing that:
    /// - Action::getOutputComponents() method returns all instantiated components during the apply() method
    /// **as well** as all the unmodified components (unaware of if this modified components were modified
    /// prior to the apply() method).
    ///   Note that the last instantiated component is at the end of the list.
    /// - Action::getOutputComponent() method returns the first instantiated or modified component during the
    /// apply().
    ///
    /// There is a specific issue that arise when there are existing components marked as modified
    /// **before** the start of the pipeline (e.g., when the action pipeline was run previously
    /// and the user left the instantiated component untouched/unsaved or when the user modified
    /// a component manually, outside the pipeline).
    ///
    /// The best way (for now) to deal with this issue is to simply get the last top-level component
    /// in the ouput list. This will be the last instantiated top-level component during the apply() method.
    /// Any components that were modified _before_ the apply() will therefore be ignore
    ///
    /// This is an issue to consider in the Action class.
    /// We probably should list in Action::preprocessInPipeline() all the component that were marked modified
    /// and ignore them in the output component list (that will not take into account the rare case were a
    /// already modified component was modified **as well** during the apply(), but that seems a better
    /// behaviour anyway.
    ///
    camitk::Component* getLastTopLevelOutputComponents(const QString& actionName);

    /// apply the resample action (modifying the parameter)
    PipelineActionStatus applyResample(camitk::Component* input);

    /// apply the Otsu action
    PipelineActionStatus applyOtsu(camitk::Component* input);
};

#endif // ACTIONSINACTIONACTION_H

