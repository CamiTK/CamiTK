#!/bin/bash

# Path needs to use forward slashes
# This is ok on Linux but since gitlab-runner 11.7 on windows all path variables use backward slash instead of forward slash
# → Replace all backslash to forward slash 
export PROJECT_SOURCE_DIR=${PROJECT_SOURCE_DIR//\\//}

echo "Job $CI_JOB_NAME"
echo "PROJECT_SOURCE_DIR=$PROJECT_SOURCE_DIR"

if [ "$TRIGGER_STAGE_CHECK" == "false" ]; then 
    echo "Job skipped as /check flag not in commit message and CAMITK_CI_STAGE < $STAGE_CHECK"; 
    exit 1; 
fi

pwd
printenv
ls -la $PROJECT_SOURCE_DIR
