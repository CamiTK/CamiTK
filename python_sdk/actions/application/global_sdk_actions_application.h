// local header files
#include <ApplicationActionExtension.h>
#include <AboutAction.h>
#include <ClearSelectionAction.h>
#include <CloseAction.h>
#include <CloseAllAction.h>
#include <OpenAction.h>
#include <OpenFile.h>
#include <QuitAction.h>
#include <RemoveLastInstantiatedAction.h>
#include <SaveAction.h>
#include <SaveAllAction.h>
#include <SaveAsAction.h>
#include <SelectLastInstantiatedAction.h>
#include <SetPathToTestData.h>
#include <Show3DViewer.h>
#include <ShowAllViewers.h>
#include <ShowArbitraryViewer.h>
#include <ShowAxialViewer.h>
#include <ShowConsoleAction.h>
#include <ShowCoronalViewer.h>
#include <ShowSagittalViewer.h>

